﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using InfosysiSafe;

namespace InfosysiSafe.Tests
{
    [TestClass]
    public class EncryptionTest
    {
        [TestMethod]
        public void Encryption_ShouldEncryptDecrypt_From_0_To_100()
        {
            for (int i = 0; i <= 100; i++)
            {
                string encrypted = EncryptDecrypt.Encrypt(i);
                Assert.AreEqual(EncryptDecrypt.Decrypt(encrypted), i);
            }
        }
    }
}
