﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfosysiSafe;

namespace InfosysiSafe.Tests
{
    public class MockLogger : ILogger
    {
        public void SetContext(string context) { }

        public void Debug(object message) { }
        public void Debug(object message, Exception ex) { }

        public void Info(object message) { }
        public void Info(object message, Exception ex) { }

        public void Warn(object message) { }
        public void Warn(object message, Exception ex) { }

        public void Error(object message) { }
        public void Error(object message, Exception ex) { }

        public void Fatal(object message) { }
        public void Fatal(object message, Exception ex) { }
    }
}
