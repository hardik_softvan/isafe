﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfosysiSafe.Tests
{
    public class ErrorAuthenticator : IAuthenticator
    {
        string ErrorMessage { get; }
        public ErrorAuthenticator(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
        public bool IsValid(string loginName, string password)
        {
            throw new Exception(ErrorMessage);
        }
        public Dictionary<string, object> GetUserDetails(string loginName, string password)
        {
            throw new InvalidOperationException();
        }
    }
}
