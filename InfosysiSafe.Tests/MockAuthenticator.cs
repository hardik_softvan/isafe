﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfosysiSafe.Tests
{
    public class MockAuthenticator : IAuthenticator
    {
        public string ValidLoginName { get; set; }
        public string ValidPassword { get; set; }
        Dictionary<string, object> UserDetails { get; }
        public MockAuthenticator(string validLoginName, string validPassword, Dictionary<string, object> userDetails)
        {
            ValidLoginName = validLoginName;
            ValidPassword = validPassword;
            UserDetails = userDetails;
        }
        public bool IsValid(string loginName, string password)
        {
            return loginName == ValidLoginName && password == ValidPassword;
        }
        public Dictionary<string, object> GetUserDetails(string loginName, string password)
        {
            return UserDetails;
        }
    }

}
