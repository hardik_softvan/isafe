﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InfosysiSafe;

namespace InfosysiSafe.Tests
{
    public class MockStore : IPersistantStore
    {
        private List<IPersistantObject> Objects = new List<IPersistantObject>();
        private int NewId = 0;
        public void Create(string className, IPersistantObject newObject)
        {
            Objects.Add(newObject);
            newObject.intId = ++NewId;
        }
        public IPersistantObject Get(string className, string propertyName, object value)
        {
            string prop = propertyName[0].ToString().ToUpper() + propertyName.Substring(1);
            foreach (IPersistantObject obj in Objects)
            {
                if (obj.GetType().Name.Equals(className) && obj.GetType().GetProperty(prop).GetValue(obj).Equals(value))
                {
                    return obj;
                }
            }
            return null;
        }
        public void Update(IPersistantObject updatedObject)
        {
            List<IPersistantObject> updatedObjects = new List<IPersistantObject>();
            foreach (IPersistantObject obj in Objects)
            {
                if (obj.intId == updatedObject.intId)
                {
                    updatedObjects.Add(updatedObject);
                } else
                {
                    updatedObjects.Add(obj);
                }
            }
            Objects = updatedObjects;
        }
        public void DumpObjects(string tag)
        {
            StringBuilder sb = new StringBuilder();
            foreach (IPersistantObject obj in Objects)
            {
                sb.Append('\n');
                sb.Append("\t" + obj.ToString());
            }
            Console.WriteLine(tag + sb.ToString());
        }
        // Note: returns all objects of the specified class
        public List<IPersistantObject> Find(string className, Dictionary<string, object> filter)
        {
            List<IPersistantObject> output = new List<IPersistantObject>();
            foreach (IPersistantObject obj in Objects)
            {
                if (Matches(className, obj, filter)) output.Add(obj);
            }
            return output;
        }
        private bool Matches(string className, IPersistantObject obj, Dictionary<string, object> filter)
        {
            return obj.GetType().Name.Equals(className);
        }
        public long Count(string className, Dictionary<string, object> filter)
        {
            long count = 0L;
            foreach (IPersistantObject obj in Objects)
            {
                if (Matches(className, obj, filter)) count++;
            }
            return count;
        }
    }
}
