﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfosysiSafe
{
    /// <summary>
    /// All loggers need to implement this interface
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Sets the context (a string that is used to filter log entries)
        /// </summary>
        /// <param name="context"></param>
        void SetContext(string context);

        void Debug(object message);
        void Debug(object message, Exception ex);

        void Info(object message);
        void Info(object message, Exception ex);

        void Warn(object message);
        void Warn(object message, Exception ex);

        void Error(object message);
        void Error(object message, Exception ex);

        void Fatal(object message);
        void Fatal(object message, Exception ex);
    }
}
