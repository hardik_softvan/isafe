﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.Services;

namespace InfosysiSafe.html
{
    public partial class showcertificate : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Certificate();
            string sessionId = Session["userId"].ToString()+".png";
            string userId = Session["userId"].ToString();
            StringBuilder sb1 = new StringBuilder();
            //sb1.Append("<iframe src=\"../images/certificates/"+ sessionId + " \"/> </ iframe > ");
            sb1.Append("<img src=\"../images/certificates/" + sessionId + "\" alt =\"\"> ");
            ltrCertificates.Text = sb1.ToString();
        }
        protected void DownloadFile(object sender, EventArgs e)
        {
            string userId = Session["userId"].ToString();
            if (File.Exists(Server.MapPath("../images/Certificates/" + userId + ".png")))
            {
                Response.ContentType = "image/png";
                Response.AppendHeader("Content-Disposition", "attachment; filename=certificate.png");
                Response.WriteFile(Server.MapPath("../images/Certificates/" + userId + ".png"));
                Response.End();
              
            }
            else
            {

            }
        }

        public void Certificate()
        {
            Dictionary<string, object> filter2 = new Dictionary<string, object>();
            filter2.Add("Name", "ByPledge");
            filter2.Add("UserId", Convert.ToInt32(Session["userId"]));
            List<IPersistantObject> Pledge = Global.Store.Find("iSafeTrnPledge", filter2);
            if (Pledge.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "showPledge()", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "hidePledge()", true);
            }
        }


    }
}