﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="showcertificate.aspx.cs" Inherits="InfosysiSafe.html.showcertificate" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
<script type="text/javascript">
	function printContent(el) {
		var divToPrint = document.getElementById(el);
		var popupWin = window.open('', '_blank', 'width=900,height=600,location=no,left=200px');
		popupWin.document.open();
		popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
		popupWin.document.close();
	}
	showPledge();
</script>
</head>
<body class="loaded-content congrats-bg">
 <div class="d-none" id="availableLanguagesContainerBody1"><span>Certificate</span> </div>
    <div class="d-none" id="availableLanguagesContainerBody"><span>video</span> </div>
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container text-center">
    <div class="container-fluid">
      <div class="cert-controls">
        <asp:Button ID="ButtonDownload" CssClass="btn infs-btn-secondary" ClientIDMode="Static" OnClick="DownloadFile" runat="server" Text="Download Certificate" BorderStyle="None" UseSubmitBehavior="False" />
        <asp:Button ID="btnPrint" CssClass="btn infs-btn-secondary" ClientIDMode="Static" runat="server" OnClientClick="printContent('printwin')" Text="Print Certificate" BorderStyle="None" UseSubmitBehavior="False" />
      </div>
      <!-- Generated Certificate -->
      <div class="print-certificate" id="printwin">
            <asp:Literal ID="ltrCertificates" runat="server"></asp:Literal>
      </div>
      <!-- Generated Certificate Iframe -->
      <div class="print-certificate" style="display:none;"> <img src="../images/certificates/73.png" alt="">
        <style>
			iframe{width:100%;height:500px;border:0;margin:0 auto;}
		</style>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../User/js/CMS/pledge.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
