﻿using InfosysiSafe.API;
using InfosysiSafe.Domain;
using InfosysiSafe.User;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.html
{
    public partial class Module02_3_2 : System.Web.UI.Page
    {
       static string  CorrectAnswer1, CorrectAnswer2, CorrectAnswer3, CorrectAnswer4, CorrectAnswer5;
       static string UserAnswer1, UserAnswer2, UserAnswer3, UserAnswer4, UserAnswer5;
       static  int Totalresult = 0;
        static DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
       static  Dictionary<string, object> input = new Dictionary<string, object>();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
#if DEBUG
                // Session["Module1AssesmentID"] = "1";
#endif
                ViewState["userid"] = Session["Module1AssesmentID"].ToString();
                BindData();

            }
        }

        private void BindData()
        {
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            UserLogin loginAPI = new UserLogin(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> filter = new Dictionary<string, object>();
            int module = 9;
            int questionumber = 1;
            int option = 1;
            filter.Add("Name", "Random5");
            filter.Add("intMstModuleId", module);
            StringBuilder sb1 = new StringBuilder();
            List<IPersistantObject> questions = Global.Store.Find("iSafeMstQuestion", filter);
            List<iSafeMstQuestion> questionlist = new List<iSafeMstQuestion>();
            
            for (int i = 0; i < questions.Count; i++)
            {
                
                var jdata = questions[i];
                string jsondata = Convert.ToString(jdata).Replace("\"", "\'");
                int quetionid = 0;
                int optioncount = 1;
                JArray parsedArray = JArray.Parse(jsondata);
                foreach (JObject parsedObject in parsedArray.Children<JObject>())
                {
                    if (questionumber == 1)
                    {
                        sb1.Append("<div id='q" + questionumber + "' class=''>");
                    }
                    else
                    {
                        sb1.Append("<div id='q" + questionumber + "' class='d-none'>");
                    }
                    foreach (JProperty parsedProperty in parsedObject.Properties())
                    {
                        string propertyName = parsedProperty.Name;

                        if (propertyName.Equals("id"))
                        {
                            quetionid = (int)parsedProperty.Value;
                        }
                        if (propertyName.Equals("txtQuestion"))
                        {
                            string propertyValue = (string)parsedProperty.Value;
                            ViewState["" + questionumber + ""] = quetionid;
                            sb1.Append(" <div class=\"container\"><div class=\"module-title\"><h3 class='infs-heading'>Question " + questionumber + " of 5</h3></div> <div class=\"row rapidfire-progress\"><div class=\"col-6\"><div class=\"rapidfire-counter timer\"><span class='timer' id=\"points"+ questionumber + "\"></span> POINTS</div><progress value=\"0\" max=\"20\" id=\"Bar\"></progress></div><div class=\"col-6 text-right\"><div class=\"rapidfire-counter timer\"><span class='timer' id=\"time" + questionumber + "\" >10</span> SECONDS</div><progress value=\"0\" max=\"10\" id=\"progressBar" + questionumber + "\"></progress></div></div> <h4  class=\"questions\">" + propertyValue + "</h4></div> <div class=\"rapidfire-quiz\">");
                        }

                        if (propertyName.Contains("intCorrectOptNo"))
                        {
  
                            string propertyValue = (string)parsedProperty.Value;
                            ViewState["ans" + questionumber + ""] = propertyValue;
                            if (questionumber==1)
                            {
                                CorrectAnswer1 = propertyValue;
                                //hdnRightAnswer1.Value = propertyValue;
                            }
                            if (questionumber == 2)
                            {
                                CorrectAnswer2 = propertyValue;
                               // hdnRightAnswer2.Value = propertyValue;
                            }
                            if (questionumber == 3)
                            {
                                 CorrectAnswer3 = propertyValue;
                               // hdnRightAnswer3.Value = propertyValue;
                            }
                            if (questionumber == 4)
                            {
                                CorrectAnswer4 =  propertyValue;
                               // hdnRightAnswer4.Value = propertyValue;
                            }
                            if (questionumber == 5)
                            {
                                CorrectAnswer5 = propertyValue;
                                //hdnRightAnswer5.Value = propertyValue;
                            }

                        }
                     

                        if (propertyName.Contains("txtOpt"))
                        {
                            string propertyValue = (string)parsedProperty.Value;
                        
                                if (optioncount == 1 || optioncount == 5)
                                {
                                if (propertyValue != "")
                                {
                                    sb1.Append("<div class=\"kbc-quiz\"> <div class=\"container\"><div class=\"row\">");
                                }
                                else
                                {
                                    sb1.Append("<div class=\"\"> <div class=\"container\"><div class=\"row\">");
                                }
                                }


                            // string propertyValue = (string)parsedProperty.Value;
                            if (propertyValue != "")
                            {

                                //string questlist = questionumber.ToString();
                                //if (questionumber == 5)
                                //{
                                //     questlist = "";
                                //}
 

                                    sb1.Append("<div class=\"col-sm-6\"><div class='answers'><input type='radio' onclick='$(\"#hdnAnswer" + questionumber + "\").val(\"" + option + "\"); document.getElementById(\"btn" + questionumber + "\").click();' id='qname" + quetionid + "_" + optioncount + "' name='qname" + quetionid + "' value=" + questionumber + " runat=\"server\">  <label for=\"qname" + quetionid + "_" + optioncount + "\"><span>" + propertyValue + "</span></label><span class=\"answeroption\">" + option + "</span></div></div>");
                                //if (questionumber == 5)
                                //{
                                //    sb1.Append("<div class=\"col-sm-6\"><div class='answers'><input type='radio' onclick='$(\"#hdnAnswer" + questionumber + "\").val(\"" + option + "\"); document.getElementById(\"btnStart\").click();' id='qname" + quetionid + "_" + optioncount + "' name='qname" + quetionid + "' value=" + questionumber + " runat=\"server\">  <label for=\"qname" + quetionid + "_" + optioncount + "\"><span>" + propertyValue + "</span></label><span class=\"answeroption\">" + option + "</span></div></div>");
                                //}

                            }
                            option++;
                            optioncount++;
                            int val = optioncount++;

                            if (val == 4 || val == 7)
                            {
                                if (propertyValue != "")
                                {
                                    sb1.Append("</div> </div> ");
                                }
                                
                            }
                        }
                       
                    }
                    option = 1;
                    sb1.Append("</div></div> </div>");
 
                     int questionumberLink = questionumber + 1;
                    if (questionumber == 1)
                    {
                        string answer = "qname" + quetionid;
                        sb1.Append("</div><div class=\"text-center mt-4\"><a  id='btn" + questionumber + "' href='javascript:$(\"#q" + questionumber + "\").addClass(\"d-none\");$(\"#q" + questionumberLink + "\").removeClass(\"d-none\");timer("+ questionumber + ");' class=\"infs-btn-secondary d-none\" > Next </a></div>");
                    }
                    else if (questionumber == 4)
                    {
                        sb1.Append("</div><div class=\"text-center mt-4\"><a  id='btn" + questionumber + "' href='javascript:onclick=$(\"#q" + questionumber + "\").addClass(\"d-none\");$(\"#q" + questionumberLink + "\").removeClass(\"d-none\");$(\"#btnsbmit\").removeClass(\"d-none\");timer("+ questionumber + ");' class=\"infs-btn-secondary d-none\" > Next </a></div>");
                    }
                    else if (questionumber == 5)
                    {
                        sb1.Append("</div></div>");
                    }
                    else
                    {
                        Panel1.Visible = false;
                        sb1.Append("</div>" +
                            "<div class=\"text-center mt-4\"><a  id='btn" + questionumber + "' href='javascript:$(\"#q" + questionumber + "\").addClass(\"d-none\");$(\"#q" + questionumberLink + "\").removeClass(\"d-none\");timer("+ questionumber + ");' class=\"infs-btn-secondary d-none\" > Next </a></div>");
                    }
                    sb1.Append("</div>");
                    sb1.Append("</div>");
                    questionumber++;
                }

              
                iSafeMstQuestion question = (iSafeMstQuestion)questions[i];
                questionlist.Add(question);
            }

            var abc = questionlist.Count;



            ltrAssessment.Text = sb1.ToString();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string Question1 = ViewState["1"].ToString();
            string Question2 = ViewState["2"].ToString();
            string Question3 = ViewState["3"].ToString();
            string Question4 = ViewState["4"].ToString();
            string Question5 = ViewState["5"].ToString();

            CorrectAnswer1 = ViewState["ans1"].ToString();
            CorrectAnswer2 = ViewState["ans2"].ToString();
            CorrectAnswer3 = ViewState["ans3"].ToString();
            CorrectAnswer4 = ViewState["ans4"].ToString();
            CorrectAnswer5 = ViewState["ans5"].ToString();

            UserAnswer1 = hdnAnswer1.Value.ToString();
            UserAnswer2 = hdnAnswer2.Value.ToString();
            UserAnswer3 = hdnAnswer3.Value.ToString();
            UserAnswer4 = hdnAnswer4.Value.ToString();
            UserAnswer5 = hdnAnswer5.Value.ToString();

            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            AssessmentQuestion AssessmentQuestionAPI = new AssessmentQuestion(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> input = new Dictionary<string, object>();

            input.Add("intMstModuleId", ViewState["userid"].ToString());
            input.Add("Question1", Question1.ToString());
            input.Add("Answer1", UserAnswer1);
            input.Add("Question2", Question2.ToString());
            input.Add("Answer2", UserAnswer2);
            input.Add("Question3", Question3.ToString());
            input.Add("Answer3", UserAnswer3);
            input.Add("Question4", Question4.ToString());
            input.Add("Answer4", UserAnswer4);
            input.Add("Question5", Question5.ToString());
            input.Add("Answer5", UserAnswer5);

            Dictionary<string, object> output = AssessmentQuestionAPI.Process(input);
            if (output.ContainsKey("error"))
            {
                //lblMessage.Text = "Incorrect user id or password. Please try again.";
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Button_Alert", "alert('Submited Sucessfully');", true);
            }
            result();
        }

        public static void result()
        {
            if (CorrectAnswer1 == UserAnswer1.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 1;
            }
            if (CorrectAnswer2 == UserAnswer2.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 1;
            }
            if (CorrectAnswer3 == UserAnswer3.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 1;
            }
            if (CorrectAnswer4 == UserAnswer4.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 1;
            }
            if (CorrectAnswer5 == UserAnswer5.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 1;
            }
            int score = Totalresult;
            HttpContext.Current.Session["TopicValue"] = "Privileged Access - Scenario";
            if (Totalresult > 70)
            {
                iSafeTrnAssessment iSafeTrnAssessmentDomain = new iSafeTrnAssessment();
                iSafeTrnAssessmentDomain.intId = Convert.ToUInt32(HttpContext.Current.Session["userid"].ToString());
                iSafeTrnAssessmentDomain.intScore = Totalresult;
                iSafeTrnAssessmentDomain.intMstModuleId = Convert.ToInt32(HttpContext.Current.Session["hdnModel"]);
                iSafeTrnAssessmentDomain.flgResult = false;
                iSafeTrnAssessmentDomain.dtAssessmentStartAt = DateTime.Now;
                iSafeTrnAssessmentDomain.dtAssessmentCompletedAt = DateTime.Now;
                Global.Store.Update(iSafeTrnAssessmentDomain);
                NextPage();

            }
            else
            {

                iSafeTrnAssessment iSafeTrnAssessmentDomain = new iSafeTrnAssessment();
                iSafeTrnAssessmentDomain.intId = Convert.ToUInt32(HttpContext.Current.Session["userid"].ToString());
                iSafeTrnAssessmentDomain.intScore = Totalresult;
                iSafeTrnAssessmentDomain.intMstModuleId = Convert.ToInt32(HttpContext.Current.Session["hdnModel"]);
                iSafeTrnAssessmentDomain.flgResult = false;
                iSafeTrnAssessmentDomain.dtAssessmentStartAt = DateTime.Now;
                iSafeTrnAssessmentDomain.dtAssessmentCompletedAt = DateTime.Now;
                Global.Store.Update(iSafeTrnAssessmentDomain);
                NextPage();
            }

        }
        
        [System.Web.Services.WebMethod]
        public static string btnActivity(string hdnAnswer1, string hdnAnswer2, string hdnAnswer3, string hdnAnswer4, string hdnAnswer5, string QuestionNo)
        {
             int ActivityTotalScore = 0;
             int Activity = 0;
            if (CorrectAnswer1== hdnAnswer1)
            {
                Activity = ActivityTotalScore + 1;
            }
            if (CorrectAnswer2 == hdnAnswer2)
            {
                Activity = Activity + 1;
            }
            if (CorrectAnswer3 == hdnAnswer3)
            {
                Activity = Activity + 1;
            }
            if (CorrectAnswer4 == hdnAnswer4)
            {
                Activity = Activity + 1;
            }
            if (CorrectAnswer5 == hdnAnswer5)
            {
                Activity = Activity + 1;
            }


            return JsonConvert.SerializeObject(Activity);
        }

        protected static void NextPage()
        {
            Dictionary<string, object> input1 = new Dictionary<string, object>();
            Index IndexAPI = new Index(dbAuthenticator, Global.Store, Global.Logger);
            input1.Add("intMstUserid", HttpContext.Current.Session["userId"].ToString());
            input1.Add("Page", "1");
            input1.Add("Pageid", Convert.ToString(Convert.ToInt32(HttpContext.Current.Session["hdnPageSequence"]) + 1));
            input1.Add("CurrentPageid", Convert.ToString(Convert.ToInt32(HttpContext.Current.Session["hdnPageSequence"])));
            input1.Add("Model", HttpContext.Current.Session["hdnModel"].ToString());
            Dictionary<string, object> output = IndexAPI.Process(input1);
            int PageSequence = Convert.ToInt32(output["SequenceNo"]);
            int lstpage = Convert.ToInt32(HttpContext.Current.Session["hdnPageSeq"]);
            if (lstpage <= PageSequence)
            {
                HttpContext.Current.Session["hdnPageSessionType"] = "nextPage";
                HttpContext.Current.Session["hdnPageSeq"] = output["SequenceNo"].ToString();
            }
            else
            {
                HttpContext.Current.Session["hdnPageSessionType"] = "prevPage";
            }
            int currentpage = Convert.ToInt32(HttpContext.Current.Session["hdnPageSequence"]) + 1;
            decimal ModelCnt = Convert.ToDecimal(output["ModelCount"]);
            HttpContext.Current.Session["Pledge"] = output["Pledge"];
            IframePages(PageSequence, Convert.ToInt32(output["Model"]), output["PageName"].ToString(), Convert.ToInt32(HttpContext.Current.Session["userId"]), ModelCnt, output["Topic"].ToString(), Convert.ToInt32(output["Pageid"]));
        }
        public static void IframePages(int PageSequence, int Model, string PageName, int intMstUserid, decimal ModelCnt, string Topic, int Pageid)
        {
            int PageNextValue = Convert.ToInt32(PageSequence) + 1;
            int PagePrevValue = Convert.ToInt32(PageSequence) - 1;
            HttpContext.Current.Session["Pageid"] = Pageid;
            HttpContext.Current.Session["PageSequence"] = PageSequence;
            HttpContext.Current.Session["hdnPageSequence"] = PageSequence.ToString();
            HttpContext.Current.Session["hdnModel"] = Model.ToString();
            HttpContext.Current.Session["hdnNextiFrameId"] = PageNextValue.ToString();
            HttpContext.Current.Session["hdnEmpId"] = HttpContext.Current.Session["userId"].ToString();
            HttpContext.Current.Session["hdnNextiFrameId"] = PageName;
            HttpContext.Current.Session["hdnPreviFrameId"] = PagePrevValue.ToString();
            HttpContext.Current.Session["TopicValue"] = Topic;
            
            HttpContext.Current.Response.Redirect(PageName);
        }

    }
}