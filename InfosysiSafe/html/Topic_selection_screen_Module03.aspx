﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Topic_selection_screen_Module03.aspx.cs" Inherits="InfosysiSafe.html.video.Topic_selection_screen_Module03" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" /> 
 <link rel="stylesheet" href="../css/highlight.css">
     <script src="js/jquery.js"></script> 
 <script>
        var topicid = "";
        function topicSelection(PageSequence, Model,id) {
            //    topicid = id;
                var hdnPageSequence = PageSequence;
                var hdnPreviFrameId = PageSequence - 1;
                var hdnNextiFrameId = PageSequence + 1;
               var hdnModel = Model;
              $('#Antopic'+id).removeClass('flash-button');
                $.ajax({
                    url: "../User/index.aspx/Topic",
                    data: JSON.stringify({ "hdnPageSequence": hdnPageSequence, "hdnPreviFrameId": hdnPreviFrameId, "hdnNextiFrameId": hdnNextiFrameId, "hdnModel": hdnModel }),
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        // alert(data.d);
                         document.getElementById("btnStart").click();
                    }
                });
        }
    </script>
        <style>
        #Antopic1:hover, #Antopic1:focus{border: 4px solid #0077bb;}
        #Antopic2:hover, #Antopic2:focus{border: 4px solid #0077bb;}
        #Antopic3:hover, #Antopic3:focus{border: 4px solid #0077bb;}
        #Antopic4:hover, #Antopic4:focus{border: 4px solid #0077bb;}
        .disabled {pointer-events: none;opacity:0.6;}
        .check1{position: absolute;color: #30c0a4;font-size: 91px;opacity: 0.7;left: 37%;top: 21%;}
    </style>
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container-fluid">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Topic Selection Screen</h2>
      </div>
      <!-- Select Topic -->
      <div class="container">
        <div class="row text-center">
              <asp:Literal ID="Topic" runat="server"></asp:Literal>
        </div>
        <div class="text-center mt-4">
              <asp:Button ID="btnStart" class="infs-btn-secondary d-none" runat="server" Text="Select Topic" OnClick="btnStart_Click" />
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>

</form>
</body>
</html>
