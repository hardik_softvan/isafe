﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module04-2-3.aspx.cs" Inherits="InfosysiSafe.html.Module04_2_3" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
<style>
.worldMap svg, .worldMap img {
	max-width: 634px;
	height: auto;
}
.Display
{
    display:block;
}
.Dhide
{
display:none;
}
</style>
</head>
<body class="loaded-content congrats-bg" onload="hide()">
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Applicable Information Security Regulations </h2>
      </div>
      <!-- Regulations Content -->
      <div class="regulations-map mt-4">
        <p class="text-center">Some of the key information security regulations in countries around the world where Infosys has its presence are :</p>
        <div class="world-map" id="worldMap">
          <div class="row">
			<!-- Locations Tab List -->
            <div class="col-lg-7">
              <div class="map text-center nav" style="display:block;">
                <figure class="worldMap" style="height: 370px;">
                  <svg viewBox="0 0 634 370" xmlns="http://www.w3.org/2000/svg" version="1.1">
                    <image  width="634" height="370" xlink:href="../images/regulations-world-map.png"></image>
                    <a href="#usa" tabindex="0" role="button" data-toggle="tab"  >
                    <rect id="pic" x="64" y="94" fill="#fff" opacity="0" width="20" height="30" onclick="btnClick('pic', 'showimg')"></rect>
					<image id="showimg" x="52" y="88"  width="42" height="39" xlink:href="../images/selected-location.png"></image>
					</a> 
					<a href="#usa2" class="a" tabindex="0" role="button" data-toggle="tab" onclick="show(pic1)"  >
                    <rect x="97" id="pic1"  y="130" fill="#fff" opacity="0" onclick="btnClick('pic1', 'showimg1')" width="20" height="30" ></rect>
					<image id="showimg1" x="85" y="123"   width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a> 
					<a href="#usa3" tabindex="0" role="button" data-toggle="tab" id="pic">
                    <rect x="116" y="136" fill="#fff" opacity="0" width="20" height="30" id="pic2" onclick="btnClick('pic2', 'showimg2')"></rect>
					<image id="showimg2" x="104" y="131"   width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a> 
					
					<a href="#location1" tabindex="0" role="button" data-toggle="tab" >
                    <rect x="292" y="85" fill="#fff" opacity="0" width="20" height="30" id="pic3" onclick="btnClick('pic3', 'showimg3')"></rect>
					<image id="showimg3" x="292" y="85"   width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a>


					<a href="#location2" tabindex="0" role="button" data-toggle="tab" >
                    <rect x="382" y="262" fill="#fff" opacity="0" width="20" height="30" id="pic4" onclick="btnClick('pic4', 'showimg4')"></rect>
					<image id="showimg4" x="371" y="257"   width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a> 
					
					<a href="#india" tabindex="0" role="button" data-toggle="tab" >
                    <rect x="427" y="168" fill="#fff" opacity="0" width="20" height="30" id="pic5" onclick="btnClick('pic5', 'showimg5')"></rect>
					<image id="showimg5" x="416" y="165"   width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a> 
					
					<a href="#china" tabindex="0" role="button" data-toggle="tab" >
                    <rect x="468" y="128" fill="#fff" opacity="0" width="20" height="30" id="pic6" onclick="btnClick('pic6', 'showimg6')"></rect>
					<image id="showimg6" x="457" y="123"   width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a>

					<a href="#location3" tabindex="0" role="button" data-toggle="tab" >
                    <rect x="513" y="169" fill="#fff" opacity="0" width="20" height="30" id="pic7" onclick="btnClick('pic7', 'showimg7')"></rect>
					<image id="showimg7" x="502" y="163"   width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a>

					<a href="#location4" tabindex="0" role="button" data-toggle="tab" >
                    <rect x="499" y="205" fill="#fff" opacity="0" width="20" height="30" id="pic8" onclick="btnClick('pic8', 'showimg8')"></rect>
					<image id="showimg8" x="488" y="198"   width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a> 
					<a href="#australia" tabindex="0" role="button" data-toggle="tab" >
                    <rect x="533" y="278" fill="#fff" opacity="0" width="20" height="30" id="pic9" onclick="btnClick('pic9', 'showimg9')"></rect>
					<image id="showimg9" x="522" y="270"   width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a>
					<a href="#location5" tabindex="0" role="button" data-toggle="tab" >
                    <rect x="607" y="316" fill="#fff" opacity="0" width="20" height="30" id="pic10" onclick="btnClick('pic10', 'showimg10')"></rect>
					<image id="showimg10" x="596" y="309" width="42" height="39" xlink:href="../images/selected-location.png"></image>
                    </a>
					</svg>
                </figure>
              </div>
            </div>
			<!-- Locations Tab Content -->
            <div class="col-lg-5">
              <div class="regulations-tab-content tab-content"> 
                <!-- USA -->
                <div class="regulations-list tab-pane fade show active" id="usa">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"> <img src="../images/selected-location.png" alt="">USA Federal</h5>
                  </div>
                  <ul class="infolist">
                    <li> Federal Cybersecurity Enhancement Act of 2015</li>
                    <li>Cyber Intelligence Sharing and Protection Act</li>
                    <li>CAN-SPAM Act of 2003 (Controlling the Assault of Non-Solicited Pornography And Marketing Act of 2003)</li>
                  </ul>
                </div>
                <!-- USA2 -->
                <div class="regulations-list tab-pane fade" id="usa2">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">US Arkansas</h5>
                  </div>
                  <ul class="infolist">
                    <li>An Act to Protect Consumers From the Improper Useof Computer Spyware; and for Other Purposes.</li>
                    
                  </ul>
                </div>
                <!-- USA3 -->
                <div class="regulations-list tab-pane fade" id="usa3">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">US - Georgia</h5>
                  </div>
                  <ul class="infolist">
                    <li>Georgia Computer System Protection Act: HB 1630</li>
                  
                  </ul>
                </div>
                <!-- Location 1 -->
                <div class="regulations-list tab-pane fade" id="location1">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">Poland</h5>
                  </div>
                  <ul class="infolist">
                    <li>Law of Electronic Signatures</li>
                   
                  </ul>
                </div>
                <!-- Location 2 -->
                <div class="regulations-list tab-pane fade" id="location2">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">Mauritius</h5>
                  </div>
                  <ul class="infolist">
                    <li>The Electronic Transactions Act 2000</li>
                  </ul>
                </div>
                <!-- India -->
                <div class="regulations-list tab-pane fade" id="india">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">India</h5>
                  </div>
                  <ul class="infolist">
                    <li>IT (Amendment) Act 2008</li>
                   
                  </ul>
                </div>
                <!-- China -->
                <div class="regulations-list tab-pane fade" id="china">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">China</h5>
                  </div>
                  <ul class="infolist">
                    <li>Regulation on the Protection of the Right to Network Dissemination of Information</li>
                    <li>Regulation for safety protection of computer information systems</li>
                    <li>Measures for the Prevention and Management of Computer Viruses</li>
                  </ul>
                </div>
				<!-- Location 3 -->
                <div class="regulations-list tab-pane fade" id="location3">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">Philippines</h5>
                  </div>
                  <ul class="infolist">
                    <li>Electronic Act of 200 RA 8792</li>
                  </ul>
                </div>
				<!-- Location 4 -->
                <div class="regulations-list tab-pane fade" id="location4">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">Malaysia</h5>
                  </div>
                  <ul class="infolist">
                    <li>Malaysia Digital Signature Act 1997[Act 562]</li>
                  </ul>
                </div>
                <!-- Australia -->
                <div class="regulations-list tab-pane fade" id="australia">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">Australia</h5>
                  </div>
                  <ul class="infolist">
                    <li>Australia Spam Act 2003</li>
                   
                  </ul>
                </div>
                <!-- Location 5 -->
                <div class="regulations-list tab-pane fade" id="location5">
                  <div class="regulations-heading">
                    <h5 class="margin-0 bold"><img src="../images/selected-location.png" alt="">New Zealand</h5>
                  </div>
                  <ul class="infolist">
                    <li>New Zealand Unsolicited Electronic Messages Act 2007</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-idea.png" alt=""></div>
    <p>Click on the location pins to know more</p>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script> 
  <script>
	
	$(".worldMap a").click(function(e) {  
		
      $(this).removeClass('active');
       // hide();
        $("#" + fill2 + "").removeClass('Dhide');
	$("#"+fill2+"").addClass('Display');
});
	

	
	
  </script>
  <script>
   hide();
		function hide()
		{
    $('#showimg').addClass('Dhide');
	$('#showimg1').addClass('Dhide');	
	$('#showimg2').addClass('Dhide');	
	$('#showimg3').addClass('Dhide');	
	$('#showimg4').addClass('Dhide');
	$('#showimg5').addClass('Dhide');	
	$('#showimg6').addClass('Dhide');
	$('#showimg7').addClass('Dhide');	
	$('#showimg8').addClass('Dhide');
	$('#showimg9').addClass('Dhide');	
    $('#showimg10').addClass('Dhide');
		}
		var fill1 ="";
		var fill2 ="";
function btnClick(img1,img2){
		//hide();
	fill1 = img1;
	fill2 = img2;

}

  </script>
  <script>
      $(".infs-instruction").click(function () {
          $(".infs-instruction").toggleClass("infs-instruction1");
      });
  </script>
 

</form>
</body>
</html>
