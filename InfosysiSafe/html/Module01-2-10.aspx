﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module01-2-10.aspx.cs" Inherits="InfosysiSafe.html.Assessment_Instructions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="row mt-5 mb-5">
        <div class="col-md-3 text-center mb-4"> <img src="../images/assesment-character.png" alt=""> </div>
        <div class="col-md-9">
          <h3 class="infs-heading animation-left">Assessment Instructions</h3>
          <div class="reinforcement-box">
            <ul class="infolist">
              <li class="animation-left">Welcome to the Assessment section of the Password Policy and Privileged Access module</li>
              <li class="animation-left">You need to attempt 5 questions and score 80% to pass the assessment</li>
              <%--<li class="animation-left">You must score a minimum of 4 to clear this module</li>--%>
			  <li class="animation-left">After you start the assessment, you must complete it before you exit</li>
              <li class="animation-left">Selected answers are denoted by this colour<a href="javascript:void(0)"><img src="../images/selected-quiz.png" width="90" alt=""></a></li>
              <li class="animation-left">Submit your answers by clicking the submit button<a href="javascript:void(0)"><img src="../images/submit-btn.png" width="90" alt=""></a></li>
            </ul>
          </div>
          <div class="mt-4 start-assessment">
            <asp:Button ID="btnStart" CssClass="btn infs-btn-secondary" runat="server" Text="Start Assessment" ClientIDMode="Static" OnClick="btnStart_Click" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
