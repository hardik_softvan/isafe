﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module03-4-1A.aspx.cs" Inherits="InfosysiSafe.html.Module03_4_1A" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
     <link href="../css/no-scroll.css" rel="stylesheet" />  
    <script src="js/jquery.js"></script>
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 text-center mb-4"> <img src="../images/mascot1.png" alt=""> </div>
        <div class="col-sm-9">
          <h2 class="infs-heading infs-re-heading animation-left">Module Reinforcement</h2>
          <div class="reinforcement-box">
            <ul class="infolist">
              <li class="animation-left"><span class="danger">DO NOT</span> upload any confidential Information on external sites</li>
              <li class="animation-left"><span class="danger">DO NOT</span> open email attachments from unknown or untrusted sources</li>
              <li class="animation-left">Visit ISG SSVP Portal (http://ssvp/) to know if a software can be installed on the machine and subsequently raise an exception request if not approved for use</li>
              <li class="animation-left">Approach the SAM team for software needs</li>
			  
			  <li class="animation-left">Beware of phishing emails and avoid clicking on suspicious links.</li>
			  
			  <li class="animation-left">Take prior approval before carrying Infosys owned devices outside the premises</li>
			  
			   <li class="animation-left">Strictly adhere to clear desk and clear screen policies</li>
			   
			    <li class="animation-left">Refrain from discussing confidential information in public places</li>
				
				<li class="animation-left">Backup all your files on designated servers</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
    <script src="../js/main.js"></script>
</form>
</body>
</html>
