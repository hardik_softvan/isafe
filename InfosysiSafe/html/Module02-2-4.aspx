﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module02-2-4.aspx.cs" Inherits="InfosysiSafe.html.Module02_2_4" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
     <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content classification-bg">
<form id="form1" runat="server">
  <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Classification of Information -->
  <div class="infs-body-container">
    <div class="container-fluid">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Classification of Information</h2>
      </div>
      <div class="classification-tabs mt-5"> 
        <!-- Classification Tabs -->
        <ul class="nav nav-pills text-center" id="pills-tab" role="tablist">
          <li class="col-3"> <a class="active" onclick="testalert(1)" data-toggle="pill" href="#hConfidential" role="tab" aria-controls="pills-home" aria-selected="true"><img src="../images/highlyconfidentialtab.png"></a> </li>
          <li class="col-3"> <a data-toggle="pill" onclick="testalert(4)"  href="#confidential" role="tab" aria-controls="pills-profile" aria-selected="false"><img src="../images/confidential.png"></a> </li>
          <li class="col-3"> <a data-toggle="pill" onclick="testalert(15)" href="#internal" role="tab" aria-controls="pills-contact" aria-selected="false"><img src="../images/internal.png"></a> </li>
          <li class="col-3"> <a data-toggle="pill" onclick="testalert(50)" href="#public" role="tab" aria-controls="pills-contact" aria-selected="false"><img src="../images/public.png"></a> </li>
        </ul>
        <!-- Classification Tab Content  -->
        <div class="tab-content" id="pills-tabContent">
          <div class="tab-pane fade show active" id="hConfidential" role="tabpanel" aria-labelledby="pills-home-tab">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
              <ul class="infolist">
                <li class="animation-left">Highly Confidential information is intended to be restricted to a very small group of identified people and is sensitive in nature. </li>
                <li class="animation-left">Unauthorized disclosure of Highly Confidential information may have severe impact on compliance to laws, regulations, brand image, business continuity and results in financial losses.</li>
                <li class="animation-left">Some examples of Highly Confidential information are source code, proposals, quarterly results before announcement, Intellectual Property, etc. </li>
               
              </ul>
            </div>
          </div>
          <div class="tab-pane fade" id="confidential" role="tabpanel" aria-labelledby="pills-profile-tab">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
              <ul class="infolist">
                <li class="animation-left">Confidential information is intended to be restricted to a small group of identified people and should not be shared with public at large or all employees of Infosys.</li>
                <li class="animation-left">Unauthorized disclosure may result in violation of laws and financial losses to Infosys. </li>
                <li class="animation-left">Some examples of Confidential information are personal information, Statement Of Work, Master Service Agreement, vendor agreement, etc. </li>
               
              </ul>
            </div>
          </div>
          <div class="tab-pane fade" id="internal" role="tabpanel" aria-labelledby="pills-contact-tab">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
              <ul class="infolist">
                <li class="animation-left">Internal information is intended to be used within Infosys only and can be shared with all employees of Infosys. </li>
                <li class="animation-left">Some examples of Internal  information are: Infosys policies, Infosys internal announcements, Infosys organizational chart, etc.</li>
               
              </ul>
            </div>
          </div>
          <div class="tab-pane fade" id="public" role="tabpanel" aria-labelledby="pills-contact-tab">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
              <ul class="infolist">
                <li class="animation-left">Public information is any information that may be freely disclosed with the public or information that is intended for public use.</li>
                <li class="animation-left">Examples of Public information are marketing brochures, Infosys website, press releases,etc. </li>
               
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
      <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-drag.png" alt=""></div>
    <p>View content under all tabs to proceed.</p>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
    <script>
      $(".infs-instruction").click(function(){
          $(".infs-instruction").toggleClass("infs-instruction1");
});
  </script>
</form>
</body>
</html>
