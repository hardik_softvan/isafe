﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Completion.aspx.cs" Inherits="InfosysiSafe.html.Completion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>.:: Infosys ::.</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Infosys">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />
</head>
<body class="loaded-content congrats-bg">
    <form id="form1" runat="server">
     <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <div class="container-fluid"> 
    <!-- Main Content -->
    <section>
      
      <div class="infs-body-container">
        <div class="maincontent congratswrap">
          <div class="congratscontent"> <img src="../images/congrats.png" alt="" title="" data-aos="zoom-in" /><br />
            <h3 data-aos="zoom-in">
              You have successfully completed iSAFE
                </h3>
            <div class="failbtn">
              <div class="submitbtn hidden">

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
    </form>
</body>
</html>
