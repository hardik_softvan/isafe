﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module02-2-2.aspx.cs" Inherits="InfosysiSafe.html.Module02_2_2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
     <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content lifecycle-bg">
<form id="form1" runat="server">
  <!-- Conveyor Belt Main Content -->
  <div class="pipe"> <img src="../images/pipe.png" alt=""> </div>
  <div class="infs-body-container paddingbottom-0">
    <div class="container-fluid"> </div>
    <div class="classication-pack"> </div>
    <div class="classication-label"> <img src="../images/disposal-info-board.png" alt=""> </div>
    <div class="conveyor-belt">
      <div class="rotator"></div>
      <div class="belt-rotate"> <img src="../images/information.png" alt=""> </div>
    </div>
    <div class="conveyor-belt-base"> </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>

</form>
</body>
</html>
