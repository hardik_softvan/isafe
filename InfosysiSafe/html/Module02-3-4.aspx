﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module02-3-4.aspx.cs" Inherits="InfosysiSafe.html.Module02_3_4" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
     <link href="../css/no-scroll.css" rel="stylesheet" />  
    <script src="js/jquery.js"></script>
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 text-center mb-4"> <img src="../images/mascot1.png" alt=""> </div>
        <div class="col-sm-9">
          <h2 class="infs-heading infs-re-heading animation-left">Module Reinforcement</h2>
          <div class="reinforcement-box">
            <ul class="infolist">
              <li class="animation-left"><span class="danger">DO NOT</span> share your password with anyone under any circumstances</li>
              <li class="animation-left">Apply CIA triad principle while handling information at Infosys</li>
              <li class="animation-left">Always ensure that you classify information as per ‘Infosys Information Asset Classification Process’</li>
              <li class="animation-left">Label classified information as per the classification chosen</li>
              <li class="animation-left">Share Highly Confidential or Confidential information only using approved transfer mediums to authorized entities</li>
              <li class="animation-left">Dispose electronic or printed information using appropriate disposal methods provided by Infosys</li>
              <li class="animation-left">Under no circumstances, should information be leaked from Infosys environment</li>
              <li class="animation-left">Be cautious while handling information since all your actions on Infosys network are monitored. Any violation could attract strict disciplinary action</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
    <script src="../js/main.js"></script>
</form>
</body>
</html>
