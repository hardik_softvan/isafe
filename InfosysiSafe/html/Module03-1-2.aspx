﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module03-1-2.aspx.cs" Inherits="InfosysiSafe.html.Module03_1_2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Internet Usage Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Internet Usage Guidelines</h2>
      </div>
      <!-- Does Donts Content -->
      <div class="does-donts mt-4">
        <div class="row"> 
          <!-- Does -->
          <div class="col-md-6 does-content">
            <h1 class="animation-left">DO’s</h1>
            <ul class="does-list">
              <li class="animation-left"> Use Infosys internet for business purpose only</li>
              <li class="animation-left">Avoid Installing software directly from the internet. Before installing any software on your computer, visit Software Security Validation Process (SSVP) portal (http://ssvp/) and follow the software exception process</li>
              <li class="animation-left">Upload or share confidential information on Infosys and client approved platforms only</li>
              <li class="animation-left">Be cautious while accessing Infosys network through shared or public machines</li>
			  <li class="animation-left">Avoid using the ‘save password’ option while accessing websites</li>
            </ul>
          </div>
          <!-- Donts -->
          <div class="col-md-6 donts-content">
            <h1 class="animation-left">DON'Ts</h1>
            <ul class="donts-list">
              <li class="animation-left"><span class="danger">Do not</span> download songs, videos, movies or any other non-business/objectionable content using Infosys Internet</li>
              <li class="animation-left"><span class="danger">Do not</span> visit websites containing gaming, auction, lottery or any other non-business/objectionable content</li>
              <li class="animation-left"><span class="danger">Do not</span> open suspicious links that redirect you to other websites</li>
              <li class="animation-left"><span class="danger">Do not</span> try to bypass Infosys internet access policy by using proxies or any other means</li>
              <li class="animation-left"><span class="danger">Do not</span> use Infosys's name or conduct Infosys business in any form on the Internet</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
