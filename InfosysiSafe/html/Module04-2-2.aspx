﻿       <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module04-2-2.aspx.cs" Inherits="InfosysiSafe.html.Module04_2_2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Information Security Incident Regulations</h2>
      </div>
      <!-- Regulations Content -->
      <div class="regulations-content mt-4">
        <div class="table-responsive">
          <table class="table table-striped table-bordered regulations-table mb-0">
            <thead>
              <tr>
                <th>Country</th>
                <th>Regulation / Act / Law</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="bold text-uppercase">India</td>
                <td><p>IT (Amendment) Act 2008</p></td>
              </tr>
              <tr>
                <td class="bold text-uppercase">Poland</td>
                <td><p>Law of Electronic Signatures</p></td>
              </tr>
              <tr>
                <td class="bold text-uppercase">China</td>
                <td><p>Measures for Security Protection Administration of the International Networking of Computer Information Networks</p><br>
				<p>Regulation on the Protection of the Right to Network Dissemination of Information</p><br>
				<p>Regulation for safety protection of computer information systems</p><br>
				<p>Measures for the Prevention and Management of Computer Viruses</p>
				</td>
				

              </tr>
              <tr>
                <td class="bold text-uppercase">Malaysia</td>
                <td><p>Malaysia Digital Signature Act 1997[Act 562]</p></td>
              </tr>
              <tr>
                <td class="bold text-uppercase">Philippines</td>
                <td><p>Electronic Act of 200 RA 8792</p></td>
              </tr>
			  
			  <tr>
                <td class="bold text-uppercase">Australia</td>
                <td><p>Australia Spam Act 2003</p></td>
              </tr>
			   <tr>
                <td class="bold text-uppercase">New Zealand</td>
                <td><p>New Zealand Unsolicited Electronic Messages Act 2007</p></td>
              </tr>
			  
			  <tr>
                <td class="bold text-uppercase">US - Federal</td>
                <td><p>Federal Cybersecurity Enhancement Act of 2015</p><br>
				<p>Cyber Intelligence Sharing and Protection Act</p><br>
				<p>CAN-SPAM Act of 2003 (Controlling the Assault of Non-Solicited Pornography And Marketing Act of 2003)</p></td>
              </tr>
			  
			   <tr>
                <td class="bold text-uppercase">US - Arkansas</td>
                <td><p>An Act to Protect Consumers From the Improper Use of Computer Spyware; and for Other Purposes.</p></td>
              </tr>
			  <tr>
                <td class="bold text-uppercase">US - Georgia</td>
                <td><p>Georgia Computer System Protection Act: HB 1630</p></td>
              </tr>
			  <tr>
                <td class="bold text-uppercase">Mauritius</td>
                <td><p>The Electronic Transactions Act 2000</p></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
