﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pledge.aspx.cs" Inherits="InfosysiSafe.html.pledge" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Pledge Content -->
  <div class="infs-body-container">
    <div class="container-fluid">
      <div class="row infs-pledge-content">
        <div class="col-md-4 col-sm-5"><img src="../images/mascot1.png" alt=""></div>
        <div class="col-md-4 col-sm-7">
          <div class="infs-pledge">
            <div class="pledge-header"> <img src="../images/infs-logo-pledge.png" alt=""> </div>
            <div class="pledge-text"> <img src="../images/pledge.png" alt="">
              <h4>I pledge to take personal responsibility to safeguard Infosys and client confidential information from unauthorized access, disclosure, loss or theft and abide by Infosys Information Security Policy.</h4>
              <asp:Button ID="BtniDO" runat="server" CssClass="btn infs-btn-secondary" Text="I Do" OnClick="btnIdo_Click" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
