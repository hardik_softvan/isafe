﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module01-Start.aspx.cs" Inherits="InfosysiSafe.html.Module01_Start" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
</head>
<body class="loaded-content start-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
  <div class="container">
    <div class="module-title">
      <h2 class="infs-heading animation-left">Password Policy & Privileged Access</h2>
    </div>
    <!-- Video Content -->
    <div class="start-screen text-center"> <img src="../images/module01-start.png" alt="">
      <div><a href="#" class="btn infs-btn-secondary">START</a></div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
