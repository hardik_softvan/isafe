﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.html
{
    public partial class Module02_2_2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnActivity_Click(object sender, EventArgs e)
        {
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            API.Activity loginAPI = new API.Activity(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("intMstUserid", Convert.ToString(Session["userId"]));
            input.Add("Pageid", Convert.ToString(Session["PageSequence"]));
            Dictionary<string, object> output = loginAPI.Process(input);

        }
    }
}