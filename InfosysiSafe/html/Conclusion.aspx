﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Conclusion.aspx.cs" Inherits="InfosysiSafe.html.Conclusion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
<link rel="stylesheet" href="../css/popup.css">
 <script src="js/jquery.js"></script> 
</head>
<body>
    <form id="form1" runat="server">
     <div class="d-none" id="availableLanguagesContainerBody1"><span>conclusion</span> </div>
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Conclusion</h2>
      </div>
      <!-- Video Content -->
      <div class="module-intro">
        <div class="video-cont text-center" data-aos="zoom-in">
          <video id="myVideo"   controls autoplay="autoplay" controlsList="nodownload" preload="none" poster="images/module4/conclusion.jpg">
            <source src="video/module4/Conclusion.mp4" type="video/mp4">
          </video>
        </div>
      </div>
    </div>
  </div>


  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
      <script src="../js/main.js"></script>
        <script>
            
var aud = document.getElementById("myVideo");
aud.onended = function() {
    //$("#myModal").modal('show');
    window.location = "Completion.aspx";
};
</script>
</form>
</body>
</html>
