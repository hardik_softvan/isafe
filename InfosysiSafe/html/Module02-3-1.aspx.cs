﻿using InfosysiSafe.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.html
{
    public partial class Module02_3_1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnStart_Click(object sender, EventArgs e)
        {
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            AssessmentApi objAssessmentApi = new AssessmentApi(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> input = new Dictionary<string, object>();

            input.Add("flgresult", "false");
            input.Add("intScore", 0);
            input.Add("userid", Session["userId"].ToString());
            Session["TopicValue"] = "Information Leakage - Rapid Fire Quiz";
            Dictionary<string, object> output = objAssessmentApi.Process(input);
            if (output.ContainsKey("error"))
            {
            }
            else
            {
                Session["Module1AssesmentID"] = output["userid"].ToString();
                Server.Transfer("Module02-3-2.aspx");
              
            }

        }
    }
}