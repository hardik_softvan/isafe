﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module02-3-4A.aspx.cs" Inherits="InfosysiSafe.html.Module02_3_4A" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
    <link href="../css/no-scroll.css" rel="stylesheet" />
 <script src="js/jquery.js"></script> 
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
   <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Main Content Information Leakage -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Secure Ways of Handling Information</h2>
      </div>
      <!-- Video Content -->
      <div class="module-intro">
        <div class="video-cont text-center aos-init aos-animate" data-aos="zoom-in">
          <video id="myVideo" controls autoplay="autoplay" controlsList="nodownload" preload="none" poster="images/module2/Secure_Ways_of_Handling_Information.jpg">
            <source src="video/module2/Secure_Ways_of_Handling_Information.mp4" type="video/mp4">
          </video>
        </div>
      </div>
    </div>
  </div>
               <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-drag.png" alt=""></div>
    <p>Click on the next button after viewing the content</p>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
    <script src="../js/main.js"></script>
    <script>
      $(".infs-instruction").click(function(){
          $(".infs-instruction").toggleClass("infs-instruction1");
        });
  </script>
</form>
</body>

</html>
