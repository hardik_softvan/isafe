﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module04-1-5.aspx.cs" Inherits="InfosysiSafe.html.Module04_1_5" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/swipe.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
<link rel="stylesheet" href="../css/animatecheckcross.css">
<link rel="stylesheet" href="../css/popup.css">
<script src="../js/popup111.min.js"></script>

</head>
<body class="loaded-content congrats-bg">
     <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container-fluid">
      <div class="module-title">
        <h2 class="infs-heading animation-left">IS THIS AN INFORMATION SECURITY INCIDENT?</h2>
      </div>
      <div class="row">
        <div class="activitywrap_incident" id="tinderslide">
                    <ul>
		  <!-- <li>
              <div class="buddy siutaions">
                <div class="avatar">Receiving an email from an unknown sender
                  <div class="dosnumber"><span>10</span>Question</div>
                </div>
              </div>
            </li>-->
		  <li>
              <div class="buddy siutaions">
                <div class="avatar-incident">Losing an official laptop
                  <div class="dosnumber"><span>9</span>Question</div>
                </div>
              </div>
            </li>
		  <li>
              <div class="buddy siutaions">
                <div class="avatar-incident">Sending your Salary Slip to your personal email ID	
                  <div class="dosnumber"><span>8</span>Question</div>
                </div>
              </div>
            </li>
		  <li>
              <div class="buddy siutaions">
                <div class="avatar-incident">Receiving an email from an unknown Infoscion
                  <div class="dosnumber"><span>7</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar-incident">Using personal laptop to connect to Infosys network from home
                  <div class="dosnumber"><span>6</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar-incident"> Sending project document to personal email ID for working on it over the weekend
                  <div class="dosnumber"><span>5</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar-incident">Sharing password with your colleague to raise a work from home request.  
                  <div class="dosnumber"><span>4</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar-incident"> Uploading client information on social media sites
                  <div class="dosnumber"><span>3</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar-incident">Installing an approved software from the CCD “Software House” 
                  <div class="dosnumber"><span>2</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar-incident"> Allowing your friend to enter inside a customer Offshore Development centre (ODC)
                  <div class="dosnumber"><span>1</span>Question</div>
                </div>
              </div>
            </li>
          </ul>

          <div class="actions"> <a href="#" class="dislike"><i></i></a> <a href="#" class="like"><i></i></a> </div>
           <div id="Result"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-drag.png" alt=""></div>
    <p>Drag left or right to make your choice</p>
  </div>
    
        
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content text-center">
              <div class="modal-body">
				<h5>You have answered <span id="score"></span> out of 9 correctly.</h5>
				<asp:Button ID="btnStart"  class="btn infs-btn-secondary infs-btn-secondary-popup" OnClick="btnStart_Click" runat="server" Text="CONTINUE" />
			 </div>
		</div>
    </div>
</div>



  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script> 
  <script type="text/javascript" src="../js/swipe.js"></script> 
  <script type="text/javascript" src="../js/jquery.transform2d.js"></script> 
  <script type="text/javascript" src="../js/securityincident.js"></script>
    <script>
      $(".infs-instruction").click(function(){
          $(".infs-instruction").toggleClass("infs-instruction1");
});
  </script>
</form>
    <audio id="audiocross">
  <source src="../audio/Wrong.wav" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
    <audio id="audiocheck">
  <source src="../audio/Right.wav" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
</body>
</html>
