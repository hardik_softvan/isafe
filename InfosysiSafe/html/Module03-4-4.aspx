﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module03-4-4.aspx.cs" Inherits="InfosysiSafe.html.Module03_4_4" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="questionwrap">
        <asp:HiddenField ID="hdnUserId" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdn" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer1" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer2" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer3" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer4" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer5" ClientIDMode="Static" runat="server" />
        <asp:Literal ID="ltrAssessment" runat="server"></asp:Literal>
        <asp:Panel ID = "Panel1" runat="server"></asp:Panel>
        <div class="text-center mt-4 d-none" id="btnsbmit"  runat="server">
          <asp:Button ID="btnSubmit" CssClass="btn infs-btn-secondary mb-4" runat="server" Text="Submit" ClientIDMode="Static" OnClick="btnSubmit_Click" />
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
