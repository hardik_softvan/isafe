﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.html
{
    public partial class Module01_2_3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnScenario1_Click(object sender, EventArgs e)
        {
            Session["Scenario"] = 1;
            Session["TopicValue"] = "Privileged Access Scenario - Response option 1";
            Response.Redirect("Module01-2-4.aspx");
        }

        protected void btnScenario2_Click(object sender, EventArgs e)
        {
            Session["Scenario"] = 2;
            Session["TopicValue"] = "Privileged Access Scenario - Response option 2";
            Response.Redirect("Module01-2-5.aspx");
        }

        protected void btnScenario3_Click(object sender, EventArgs e)
        {
            Session["Scenario"] = 3;
            Session["TopicValue"] = "Privileged Access Scenario - Response option 3";
            Response.Redirect("Module01-2-6.aspx");
        }

        public void Scenario(int Scenario, string PageName)
        {
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            API.Scenario loginAPI = new API.Scenario(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("intMstUserid", Convert.ToString(Session["userId"]));
            input.Add("Pageid", Convert.ToString(Session["PageSequence"]));
            input.Add("Scenario", Convert.ToString(Scenario));
            Dictionary<string, object> output = loginAPI.Process(input);
            Response.Redirect(PageName);
        }
    }
}