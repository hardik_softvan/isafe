﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.html
{
    public partial class Introduction_to_iSafe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StartPageLoad();
        }
        public void StartPageLoad()
        {
           
            if (Convert.ToInt32(Session["PrivacyStatus"]) != 0)
            {

                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Button_Alert", "", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Button_Alert", "myVideo.play();", true);

            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Button_Alert", "myVideo.pause();", true);
            }

        }

    }
}