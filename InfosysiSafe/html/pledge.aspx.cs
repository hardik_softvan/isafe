﻿using InfosysiSafe.API;
using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace InfosysiSafe.html
{
    public partial class pledge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }


        protected void btnIdo_Click(object sender, EventArgs e)
        {
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            PledgeApi objPledgeApi = new PledgeApi(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("intMstUserId", Session["userId"].ToString());
            input.Add("username", Session["username"].ToString());
            Dictionary<string, object> output = objPledgeApi.Process(input);
            if (output.ContainsKey("error"))
            {
            }
            else
            {
                DBAuthenticator dbAuthenticator2 = new DBAuthenticator(Global.Store);
                CertificateApi objCertificateApi = new CertificateApi(Global.Store,  Global.Logger, System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/CertificateTemplate.png"), System.Web.Hosting.HostingEnvironment.MapPath("~/images/Certificates/"));
                Dictionary<string, object> input2 = new Dictionary<string, object>();
                input2.Add("intMstUserId", Session["userId"].ToString());
                input2.Add("username", Session["username"].ToString());
                Dictionary<string, object> output2 = objCertificateApi.Process(input2);
                ClientScript.RegisterStartupScript(this.GetType(), "Button_Alert", "alert('Pledge To UnderTake');", true);
                Session["TopicValue"] = "Certificate";
                Response.Redirect("/html/showcertificate.aspx");
                
            }

        }
    }
}