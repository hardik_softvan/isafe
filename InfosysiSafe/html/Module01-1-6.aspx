﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module01-1-6.aspx.cs" Inherits="InfosysiSafe.html.What_should_Roy_do_in_this_situation" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
<script src="js/jquery.js"></script> 

</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">What should Roy do in this situation?</h2>
      </div>
      <div class="situation-content">
        <div class="row">
          <div class="col-md-4 text-center situation1">
            <asp:Button CssClass="siutaions" ID="btnScenario1" runat="server"  Text="Roy should share the password with Jack." OnClick="btnScenario1_Click" />
          </div>
          <div class="col-md-4 text-center situation2">
            <asp:Button CssClass="siutaions" ID="btnScenario2" runat="server" Text="Roy should not share the password with Jack as it is a violation of Infosys password policy." OnClick="btnScenario2_Click" />
          </div>
          <div class="col-md-4 text-center situation3">
            <asp:Button CssClass="siutaions" ID="btnScenario3" runat="server" Text="Instead of sharing the password with Jack, Roy should go back to the office to complete the work and take the next bus to go home." OnClick="btnScenario3_Click" />
          </div>
          <div class="col-sm-12 text-center"> <img src="../images/roy1.png" alt=""> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-idea.png" alt=""></div>
    <p>Click the most appropriate option</p>
  </div>
  <div>
    <asp:PlaceHolder ID="iframeDiv" runat="server"/>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
  <script src="../js/main.js"></script>
  <script>
      $(".infs-instruction").click(function(){
          $(".infs-instruction").toggleClass("infs-instruction1");
});
  </script>
</form>
</body>
</html>
