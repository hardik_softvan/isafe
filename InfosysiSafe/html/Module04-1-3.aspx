﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module04-1-3.aspx.cs" Inherits="InfosysiSafe.html.Module04_1_3" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content congrats-bg">
     <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Information Security Incident - Examples</h2>
      </div>
      <!-- Information Security Incident Content -->
      <div class="security-incident mt-4">
        <div class="row">
		  <!-- Tab Links -->
          <div class="col-md-4">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical"> 
				<a class="nav-link active" onclick="testalert(1)" id="v-pills-home-tab" data-toggle="pill" href="#dataLeakage" role="tab" aria-controls="v-pills-home" aria-selected="true">Data Leakage</a> 
				<a class="nav-link"  onclick="testalert(4)" id="v-pills-profile-tab" data-toggle="pill" href="#maliciousCode" role="tab" aria-controls="v-pills-profile" aria-selected="false">Malicious Code</a> 
				<a class="nav-link"  onclick="testalert(15)" id="v-pills-messages-tab" data-toggle="pill" href="#unauthorized" role="tab" aria-controls="v-pills-messages" aria-selected="false">Unauthorized<br>
              access to networks</a> 
				<a class="nav-link"  onclick="testalert(10)" id="v-pills-settings-tab" data-toggle="pill" href="#passwordSharing" role="tab" aria-controls="v-pills-settings" aria-selected="false">Password sharing</a> 
				<a class="nav-link"  onclick="testalert(10)" id="v-pills-settings-tab" data-toggle="pill" href="#pcTheft" role="tab" aria-controls="v-pills-settings" aria-selected="false">PC Theft</a> 
				<a class="nav-link"  onclick="testalert(30)" id="v-pills-settings-tab" data-toggle="pill" href="#personalData" role="tab" aria-controls="v-pills-settings" aria-selected="false">Personal data breach</a> 
			</div>
          </div>
		  <!-- Tab Content -->
          <div class="col-md-8">
            <div class="tab-content" id="v-pills-tabContent">
              <div class="tab-pane fade show active" id="dataLeakage" role="tabpanel" aria-labelledby="v-pills-home-tab">
                <ul class="infolist">
                  <li class="animation-left">Sending Infosys and client confidential information to personal mail ID's.</li>
                  <li class="animation-left">Uploading source code to external cloud based software development platform sites.</li>
                  <li class="animation-left">Copying Infosys and client confidential  information in external hard drives or pen drives.</li>
                </ul>
              </div>
              <div class="tab-pane fade" id="maliciousCode" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <ul class="infolist">
                  <li class="animation-left">Programs intended to cause harm to a computer e.g. trojans, worms and viruses</li>
                 
                </ul>
              </div>
              <div class="tab-pane fade" id="unauthorized" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                <ul class="infolist">
                  <li class="animation-left">Bypassing security mechanisms of a network or computer to gain unauthorized access.</li>
                  
                </ul>
              </div>
              <div class="tab-pane fade" id="passwordSharing" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                <ul class="infolist">
                  <li class="animation-left">Sharing login credentials (username and password) with your manager, colleague or team members for completion of a task.</li>
                  <li class="animation-left">Sharing the secure ID token and pin with your manager or a team member.</li>
                 
                </ul>
              </div>
			  <div class="tab-pane fade" id="pcTheft" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                <ul class="infolist">
                  <li class="animation-left">Loss of any computer equipment which contains information classified as highly confidential or confidential.</li>
                  
                </ul>
              </div>
			  <div class="tab-pane fade" id="personalData" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                <ul class="infolist">
                  <li class="animation-left"> Loss of personal data of employees, customers and partners that Infosys holds.</li>
                  <li class="animation-left">Loss of personal Information through phishing and social engineering attacks.</li>
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
          <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-drag.png" alt=""></div>
    <p>View content under all tabs to proceed.</p>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
    <script>
      $(".infs-instruction").click(function(){
          $(".infs-instruction").toggleClass("infs-instruction1");
      });
  </script>
</form>
</body>
</html>
