﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module03-4-1.aspx.cs" Inherits="InfosysiSafe.html.Module03_4_1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
    <script src="js/jquery.js"></script> 
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Protection of Information in Public</h2>
      </div>
      <!-- Video Content -->
      <div class="module-intro">
        <div class="video-cont text-center" data-aos="zoom-in">
          <video id="myVideo" controls autoplay="autoplay" controlsList="nodownload" preload="none" poster="images/module3/Protection_of_Information_in_Public.jpg">
            <source src="video/module3/Protection_of_Information_in_Public.mp4" type="video/mp4">
          </video>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
    <script src="../js/main.js"></script>
</form>
</body>
</html>
