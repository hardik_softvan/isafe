﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.html
{
    public partial class fail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int EmployeeId = Convert.ToInt32(Session["EmployeeId"]);
            int userId = Convert.ToInt32(Session["userId"]);
            int Module = Convert.ToInt32(Session["hdnModel"]);
            string Result = "Fail";

            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter.Add("Name", "ByAttemptCount");
            filter.Add("intMstModuleid", Module);
            filter.Add("intMstUserId", userId);
            List<IPersistantObject> Attempt = Global.Store.Find("iSafeTrnAssessment", filter);
            Dictionary<string, object> filter1 = new Dictionary<string, object>();
            filter1.Add("Name", "ByAssessmentScore");
            filter1.Add("intMstModuleid", Module);
            filter1.Add("intMstUserId", userId);
            List<IPersistantObject> Assessment = Global.Store.Find("iSafeTrnAssessment", filter1);
            List<iSafeTrnAssessment> Assess = new List<iSafeTrnAssessment>();
            iSafeTrnAssessment AssessmentDetails = (iSafeTrnAssessment)Assessment[0];

            int AttemptNumber = Attempt.Count;
            int AssessmentScore = AssessmentDetails.intScore;
            DateTime AssessmentCompletedAt = Convert.ToDateTime(AssessmentDetails.dtAssessmentCompletedAt);
            string AssessmentCompletedTime = AssessmentDetails.dtAssessmentCompletedAt.ToString();
            DateTime AssessmentCompletionTime = DateTime.Parse(AssessmentCompletedTime);
            string ExceptionStatus = "Yes";
            hdEmployeeId.Value = EmployeeId.ToString();
            hdModule.Value = Module.ToString();
            hdAttemptNumber.Value = AttemptNumber.ToString();
            hdAssessmentScore.Value = AssessmentScore.ToString();
            hdResult.Value = Result.ToString();
            hdAssessmentCompletedAt.Value = AssessmentCompletedAt.ToString();
            hdAssessmentCompletionTime.Value = AssessmentCompletedAt.ToString();
            hdExceptionStatus.Value = ExceptionStatus;
        }
        protected void btnStart_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Session["hdnModel"])==1)
            {
                Session["hdnPageSequence"] = 2;
                Session["hdnPreviFrameId"] = 1;
                Session["hdnNextiFrameId"] = 3;
                Session["hdnModel"] = 1;
                Session["TopicValue"] = "Password Policy & Privileged Access";
                Response.Redirect("Module01-1-0.aspx");
            }
            if (Convert.ToInt32(Session["hdnModel"]) == 2)
            {
                Session["hdnPageSequence"] = 1;
                Session["hdnPreviFrameId"] = 0;
                Session["hdnNextiFrameId"] = 2;
                Session["hdnModel"] = 2;
                Session["TopicValue"] = "Secure Ways of Handling Information";
                Response.Redirect("Module02-1-0.aspx");
            }
            if (Convert.ToInt32(Session["hdnModel"]) == 3)
            {
                Session["hdnPageSequence"] = 1;
                Session["hdnPreviFrameId"] = 0;
                Session["hdnNextiFrameId"] = 2;
                Session["hdnModel"] = 3;
                Session["TopicValue"] = "Acceptable Usage ";
                Response.Redirect("Module03-1-0.aspx");
            }
            if (Convert.ToInt32(Session["hdnModel"]) == 4)
            {
                Session["hdnPageSequence"] = 1;
                Session["hdnPreviFrameId"] = 0;
                Session["hdnNextiFrameId"] = 2;
                Session["hdnModel"] = 4;
                Session["TopicValue"] = "Information Security Roles & Responsibilities of Employees";
                Response.Redirect("Module04-1-0.aspx");
            }
        }

        protected void btnRestart_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Session["hdnModel"]) == 1)
            {
                Session["hdnPageSequence"] = 18;
                Session["hdnPreviFrameId"] = 19;
                Session["hdnNextiFrameId"] = 17;
                Session["hdnModel"] = 1;
                Session["TopicValue"] = "Assessment Instructions";
                Response.Redirect("Module01-2-10.aspx");
            }
            if (Convert.ToInt32(Session["hdnModel"]) == 2)
            {
                Session["hdnPageSequence"] = 24;
                Session["hdnPreviFrameId"] = 23;
                Session["hdnNextiFrameId"] = 25;
                Session["hdnModel"] = 2;
                Session["TopicValue"] = "Assessment Instructions";
                Response.Redirect("Module02-3-5.aspx");
            }
            if (Convert.ToInt32(Session["hdnModel"]) == 3)
            {
                Session["hdnPageSequence"] = 22;
                Session["hdnPreviFrameId"] = 21;
                Session["hdnNextiFrameId"] = 23;
                Session["hdnModel"] = 3;
                Session["TopicValue"] = "Acceptable Usage - Assessment Instructions";
                Response.Redirect("Module03-4-2.aspx");
            }
            if (Convert.ToInt32(Session["hdnModel"]) == 4)
            {
                Session["hdnPageSequence"] = 18;
                Session["hdnPreviFrameId"] = 17;
                Session["hdnNextiFrameId"] = 19;
                Session["hdnModel"] = 4;
                Session["TopicValue"] = "Acceptable Usage - Assessment Instructions";
                Response.Redirect("Module04-4-5.aspx");
            }
        }
    }
}