﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module03-2-2.aspx.cs" Inherits="InfosysiSafe.html.Module03_2_2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Email Usage Guidelines Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Email Usage Guidelines</h2>
      </div>
      <!-- Does Donts Content -->
      <div class="does-donts mt-4">
        <div class="row"> 
          <!-- Does -->
          <div class="col-md-6 does-content">
            <h1 class="animation-left">DO’s</h1>
            <ul class="does-list">
              <li class="animation-left">Always use your official e-mail for business purposes only.</li>
              <li class="animation-left">Report suspicious e-mails to iCERT@infosys.com </li>
              <li class="animation-left">Beware of phishing emails.</li>
              <li class="animation-left">Inspect the trustworthiness of links/attachments in emails, before clicking/opening them.</li>
			  <li class="animation-left">Be careful if a link redirects you to an unknown website that asks for your username and password</li>
            </ul>
          </div>
          <!-- Donts -->
          <div class="col-md-6 donts-content">
            <h1 class="animation-left">DON'Ts</h1>
            <ul class="donts-list">
              <li class="animation-left"><span class="danger">Do not</span> open email attachments from unknown or untrusted sources. </li>
              <li class="animation-left"><span class="danger">Do not</span> open an email that you suspect may not be legitimate. </li>
              <li class="animation-left"><span class="danger">Do not</span> send e-mails containing Infosys or client confidential information to personal e-mail accounts.</li>
              <li class="animation-left"><span class="danger">Do not</span> spread mass mails, offers, hate speeches, vulgar content, and jokes using your official e-mail account.</li>
              <li class="animation-left"> Auto forwarding of emails to personal email addresses is prohibited.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Foating Instructions -->
  <!--<div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-drag.png" alt=""></div>
    <p>Drag left or right to make your choice</p>
  </div>-->
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>

</body>
</html>
