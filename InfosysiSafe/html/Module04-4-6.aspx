﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module04-4-6.aspx.cs" Inherits="InfosysiSafe.html.Module04_4_6" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
    <script src="../js/jquery.js"></script>
    <script>
           $(document).ready(function () {
        $('#lblSelection').hide();
        });
                function ValidateForm() {
            if ($("#hdnAnswer5").val() == "")
            {
                $("#lblSelection").show();
                return false;
            } else
            {
                $("#lblSelection").hide();
                return true;
            }
            
        }
    </script>
	
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
     <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
    <div class="infs-body-container">
    <div class="container">
      <div class="questionwrap">
        <asp:HiddenField ID="hdnUserId" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdn" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer1" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer2" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer3" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer4" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer5" ClientIDMode="Static" runat="server" />
        <asp:Literal ID="ltrAssessment" runat="server"></asp:Literal>
        <asp:Panel ID = "Panel1" runat="server"></asp:Panel>

        <div class="text-center mt-4 d-none" id="btnsbmit"  runat="server">
             <a href='javascript:onclick=q5();function q5(){$("#q5").addClass("d-none");$("#q4").removeClass("d-none");$("#lblSelection").hide();$("#btnsbmit").addClass("d-none");}'class="btn infs-btn-secondary prev mr-2" > Previous </a>
          <asp:Button ID="btnSubmit" CssClass="btn infs-btn-secondary" OnClientClick="return ValidateForm();" runat="server" Text="Submit" ClientIDMode="Static" OnClick="btnSubmit_Click" />
        </div>
                   <div class="text-center mt-4">
           <asp:Label ID="lblSelection" runat="server" Text="Please Select Option" ForeColor="Red"></asp:Label>
          </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
