﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module03-2-4.aspx.cs" Inherits="InfosysiSafe.html.Module03_2_4" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
  <script src="../js/jquery.js"></script>
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" /> 
<link rel="stylesheet" href="../css/popup.css">
 <link href="../css/PhishEmail.css" rel="stylesheet" />
    <script src="../js/popup111.min.js"></script>
    <style>
        form{cursor:pointer;}
    </style>
</head>
<body class="loaded-content congrats-bg body">
     <div class="hide" id="availableLanguagesContainerBody"><span>video</span> </div>
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container pointer">
    <div class="module-title">
        <h2 class="infs-heading animation-left">What Is Wrong with the E-Mail?</h2>
     </div>
      <div class="infs-counter">
          <h4><span id="ClickCount"></span> errors out of 6 spotted.</h4>
      </div>

    <div class="row">
	<div class="col-md-12">
	<div class="phishing-mail-desktop">
        
		<div class="phishing-mail-body" onclick="clickCounter()">
			<img src="../images/phishing_mail.png">
			<div id="result"></div>
	
		<div class="phishing-mail-title">
			<a href="javascript:void(0)" class="popup" id="anchor" onclick="btnClick('popup','anchor')"><span>From:</span>ithelp@infoosys.co.in
			<span class="popuptext1" id="popup"><img src="../images/error.png"> Invalid / suspicious email id</span></a> 
			| <span> To:</span>all@infosys.co.in<br>
		
		</div>
		<div class="phishing-mail-sub">
		
			<a href="javascript:void(0)" class="popup" id="anchor1" onclick="btnClick('popup1','anchor1')"><span>Sub:</span>Urgent - Pasword will expiirre <span class="popuptext6" id="popup1"><img src="../images/error.png">Spelling mistake in subject (password) </span></a> 
		</div>
		<div class="phishing-mail-attchment">
		<a href="javascript:void(0)" class="popup" id="anchor11" onclick="btnClick('popup11','anchor11')"><b><u><img src="../images/attachment1.png" alt=""></u></b> <span class="popuptext4" id="popup11"><img src="../images/error.png"> Suspicious attachment - text: Open immediately!</span></a>
		</div>
		<div class="phishing-mail-content">
				<p>Dear User,<br>Your account password will expire today. Kindly <a href="javascript:void(0)" id="anchor9" class="popup text-pass" onclick="btnClick('popup9','anchor9')" style="color:#007bff"><b><u>Click here</u></b> <span class="popuptext" id="popup9"><img src="../images/error.png">Incorrect / suspicious url </span></a> to change the password. For more <a href="javascript:void(0)" id="anchor10" class="popup text-pass" onclick="btnClick('popup10','anchor10')"> assistence,<span class="popuptext7" id="popup10"><img src="../images/error.png">Spelling mistake in email (assistence)</span></a>
				click the attached file.</p><br>
				<a href="javascript:void(0)" class="popup" id="anchor2" onclick="btnClick('popup2','anchor2')"><p>Sincerely,<br>
				IT Desk</p><span class="popuptext4" id="popup2"><img src="../images/error.png"> Incomplete & incorrect signature.</span></a>	
		</div>
    </div>
	</div>
	</div>
	</div>
	
	<div class="row">
	<div class="col-sm-12">
	<div class="phishing-mail-mobile">
	<div class="phishing-mail-body1">
			<img src="../images/phishing-mail-mobile.png">
		<div class="phishing-mail-title1">
			<div class="border1"><span>From:</span><a class="popup" id="anchor3" onclick="btnClick('popup3','anchor3')">ithelp@infoosys.co.in <span class="popuptext1" id="popup3"><img src="../images/error.png">Invalid / suspicious email id </span></a> <br></div>
			<div class="border1"><span> To:</span>all@infosys.co.in<br></div>
			<div class="border1"><span>Sub:</span><a href="javascript:void(0)"  id="anchor7" class="popup text-pass" onclick="btnClick('popup7','anchor7')" > Password Will Expiire<br><span class="popuptext8" id="popup7"><img src="../images/error.png">Spelling mistake in subject (pasword) </span></a> </div>
		</div>
		<div class="phishing-mail-attchment1">
		<a href="javascript:void(0)" class="popup"   id="anchor4"  onclick="btnClick('popup4','anchor4')"><b><u><img src="../images/attachment1.png" alt=""></u></b> <span class="popuptext9" id="popup4"><img src="../images/error.png">Suspicious attachment - text: Open immediately!</span></a>
		</div>
		<div class="phishing-mail-content1">
				<p>Dear User,<br>Your account password will expire today.Kindly <a class="popup"  id="anchor5" onclick="btnClick('popup5','anchor5')" style="color:#086f93"><b><u>Click here</u></b> <span class="popuptext" id="popup5"><img src="../images/error.png"> Incorrect / suspicious url  </span></a> to change the password. For more <a class="popup"  id="anchor8" onclick="btnClick('popup8','anchor8')">assistence,<span class="popuptext11" id="popup8"><img src="../images/error.png">Spelling mistake in email (assistence)</span></a>
				click the attached file.</p>
				<a href="javascript:void(0)" class="popup"  id="anchor6"  onclick="btnClick('popup6','anchor6')"><p>Sincerely,<br>
				IT Desk</p><span class="popuptext12" id="popup6"><img src="../images/error.png"> Incomplete & incorrect signature.</span></a>
		</div>
	</div>
	</div>
	</div>
	</div>
    </div>
  </div>
  <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
	<div class="ins-icon-change"><div class="ins-icon"><img src="../images/icons/icon-drag.png" alt=""></div></div>
    <p>1.Click on the items that make you feel suspicious about the email. <br><br> 2.Rollover to find item. <br><br>3.Click on a clue after you spot it.
</p>	
  </div>
    <div id="myModal" class="modal fade"  data-backdrop="static" >
    <div class="modal-dialog">
        <div class="modal-content text-center">
              <div class="modal-body">
				<h5><b>Phishing mail activity successfully completed.</b></h5>
				<asp:Button ID="btnStart"  class="btn infs-btn-secondary infs-btn-secondary-popup" OnClick="btnStart_Click" runat="server" Text="CONTINUE" />
			 </div>
		</div>
    </div>
</div>
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
    <script src="../js/main.js"></script>
        <script src="../js/PhishEmail.js"></script>
    <script>
      $(".infs-instruction").click(function(){
          $(".infs-instruction").toggleClass("infs-instruction1");
});
  </script>
</form>

</body>
</html>
