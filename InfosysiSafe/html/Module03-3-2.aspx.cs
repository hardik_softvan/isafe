﻿using InfosysiSafe.API;
using InfosysiSafe.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.html
{
    public partial class Module03_3_2 : System.Web.UI.Page
    {
        DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
        Dictionary<string, object> input = new Dictionary<string, object>();
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnStart_Click(object sender, EventArgs e)
        {
            Index IndexAPI = new Index(dbAuthenticator, Global.Store, Global.Logger);
            input.Add("intMstUserid", Convert.ToString(Session["userId"]));
            input.Add("Page", "1");
            input.Add("Pageid", Convert.ToString(Convert.ToInt32(Session["hdnPageSequence"]) + 1));
            input.Add("CurrentPageid", Convert.ToString(Convert.ToInt32(Session["hdnPageSequence"])));
            input.Add("Model", Session["hdnModel"].ToString());
            Dictionary<string, object> output = IndexAPI.Process(input);
            int PageSequence = Convert.ToInt32(output["SequenceNo"]);
            int lstpage = Convert.ToInt32(Session["hdnPageSeq"]);
            if (lstpage <= PageSequence)
            {
                Session["hdnPageSessionType"] = "nextPage";
                Session["hdnPageSeq"] = output["SequenceNo"].ToString();
            }
            else
            {
                Session["hdnPageSessionType"] = "prevPage";
            }
            int currentpage = Convert.ToInt32(Session["hdnPageSequence"]) + 1;
            decimal ModelCnt = Convert.ToDecimal(output["ModelCount"]);
            Session["Pledge"] = output["Pledge"];
            IframePages(PageSequence, Convert.ToInt32(output["Model"]), output["PageName"].ToString(), Convert.ToInt32(Session["userId"]), ModelCnt, output["Topic"].ToString(), Convert.ToInt32(output["Pageid"]));
        }
        public void IframePages(int PageSequence, int Model, string PageName, int intMstUserid, decimal ModelCnt, string Topic, int Pageid)
        {
            int PageNextValue = Convert.ToInt32(PageSequence) + 1;
            int PagePrevValue = Convert.ToInt32(PageSequence) - 1;
            Session["Pageid"] = Pageid;
            Session["PageSequence"] = PageSequence;
            Session["hdnPageSequence"] = PageSequence.ToString();
            Session["hdnModel"] = Model.ToString();
            Session["hdnNextiFrameId"] = PageNextValue.ToString();
            Session["hdnEmpId"] = Session["userId"].ToString();
            Session["hdnNextiFrameId"] = PageName;
            Session["hdnPreviFrameId"] = PagePrevValue.ToString();
            Session["TopicValue"] = Topic;
            Response.Redirect(PageName);
        }
    }
}