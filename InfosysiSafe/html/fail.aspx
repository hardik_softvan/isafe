﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="fail.aspx.cs" Inherits="InfosysiSafe.html.fail" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
</head>
<body class="loaded-content failed-bg">
<form id="form1" runat="server">
  <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <div class="container-fluid"> 
    <!-- Fail Main Content -->
    <section>
      <asp:HiddenField ID="hdEmployeeId" runat="server"  />
      <asp:Label ID="lblEmployeeId" Text="1" runat="server"/>
      <asp:HiddenField ID="hdModule" runat="server" />
      <asp:HiddenField ID="hdAttemptNumber" runat="server" />
      <asp:HiddenField ID="hdAssessmentScore" runat="server" />
      <asp:HiddenField ID="hdResult" runat="server" />
      <asp:HiddenField ID="hdAssessmentCompletedAt" runat="server" />
      <asp:HiddenField ID="hdAssessmentCompletionTime" runat="server" />
      <asp:HiddenField ID="hdExceptionStatus" runat="server" />
      <div class="infs-body-container">
        <div class="maincontent failwrap">
          <div class="congratscontent"> <img src="../images/fail.png" alt="" title="" data-aos="zoom-in" /><br />
            <h3 data-aos="zoom-in">Take the quiz again</h3>
            
            <div class="failbtn">
              <div class="submitbtn"> 
                  <%--<a href="Module01-2-10.aspx" class="btn infs-btn-secondary">Take Quiz Again</a> --%>
                  <asp:Button ID="btnRestart"   class="btn infs-btn-secondary hide" runat="server" Text="Take Quiz Again" OnClick="btnRestart_Click" />
                    <asp:Button ID="btnStart"   class="btn infs-btn-secondary hide" runat="server" Text="Restart Module" OnClick="btnStart_Click" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
