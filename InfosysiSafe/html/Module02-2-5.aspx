﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module02-2-5.aspx.cs" Inherits="InfosysiSafe.html.Module02_2_5" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/swipe.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
     <link href="../css/no-scroll.css" rel="stylesheet" /> 
    <script src="../js/main.js"></script>
</head>

<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
    <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <div class="infs-body-container">
    <div class="container">
      <div class="row mt-5 mb-5">
        <div class="col-md-3 col-sm-5 text-center mb-4"> <img src="../images/policy.png" alt=""> </div>
        <div class="col-md-9 col-sm-7">
          <h3 class="infs-heading infs-re-heading animation-left">Classification of Information - Activity Instructions</h3>
          <div class="reinforcement-box">
            <ul class="infolist">
              <li class="animation-left">Items representing different forms of data will appear at a time </li>
              <li class="animation-left">Classify each item by dropping it in appropriate drawer.</li>
              <li class="animation-left">To proceed, click the Start button.</li>
            </ul>
          </div>
        </div>
        <div class="text-center mt-4 margin-center">
             <asp:Button ID="btnStart"  class="btn infs-btn-secondary" runat="server" Text="START" OnClick="btnStart_Click" />
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
