﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module04-4-2.aspx.cs" Inherits="InfosysiSafe.html.Module04_4_2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content congrats-bg">
       <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Roles & Responsibilities of Managers</h2>
      </div>
      <div class="reinforcement-box infs-roles-tab">
        <h5>Managers need to take charge, anticipate and solve problems and imbibe secure work culture among their teams. They must :</h5>
        <!-- Nav tabs -->
        <ul class="nav nav-pills nav-fill">
          <li class="nav-item"> <a class="nav-link active" data-toggle="tab" onclick="testalert(1)" href="#Strategic">Strategic Leadership</a> </li>
          <li class="nav-item"> <a class="nav-link" data-toggle="tab" onclick="testalert(15)" href="#Compliance">Compliance</a> </li>
          <li class="nav-item"> <a class="nav-link" data-toggle="tab" onclick="testalert(55)" href="#Monitoring">Monitoring</a> </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane container active" id="Strategic">
            <ul class="infolist">
              <li class="animation-left">Encourage employees to report information security incidents</li>
              <li class="animation-left">Take immediate actions on information security incidents</li>
              <li class="animation-left">Approach ISG to mitigate risks of third party solutions</li>
              <li class="animation-left">Ensure teams complete mandatory information security trainings</li>
              <li class="animation-left">Encourage a culture of positive information security behaviour</li>
            </ul>
          </div>
          <div class="tab-pane container fade" id="Compliance">
            <ul class="infolist">
              <li class="animation-left">Ensure teams adhere to Infosys information security policies</li>
              <li class="animation-left">Plan periodic internal audits for projects and ensure timely closure of non-conformance</li>
              <li class="animation-left">Track and ensure that all the laptops used in the team are encrypted</li>
              <li class="animation-left">Take prior client consent before revealing any information</li>
             
            </ul>
          </div>
          <div class="tab-pane container fade" id="Monitoring">
            <ul class="infolist">
              <li class="animation-left">Effective monitoring of accesses</li>
              <li class="animation-left">Periodic reviews of special accesses</li>
              <li class="animation-left">Removal of previously held access rights in case of any internal movements</li>
             
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
