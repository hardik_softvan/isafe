﻿using InfosysiSafe.API;
using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;

namespace InfosysiSafe.html
{
    public partial class Assessment_Instructions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnStart_Click(object sender, EventArgs e)
        {
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            AssessmentApi objAssessmentApi = new AssessmentApi(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("flgresult", "false");
            input.Add("intScore", 0);
            input.Add("userid", Session["userId"].ToString());
            Dictionary<string, object> output = objAssessmentApi.Process(input);
            Session["TopicValue"] = "Assessment";
            if (output.ContainsKey("error"))
            {

            }
            else
            {
                Session["Module1AssesmentID"] = output["userid"].ToString();
                Server.Transfer("Module01-2-11.aspx");
            }
           
        }

    }
}