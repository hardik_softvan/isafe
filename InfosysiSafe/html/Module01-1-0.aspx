﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module01-1-0.aspx.cs" Inherits="InfosysiSafe.html.Module01_1_0" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
    <script src="js/jquery.js"></script> 

</head>
<body class="loaded-content start-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
    <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <div class="infs-body-container">
  <div class="container">
    <div class="module-title">
      <h2 class="infs-heading animation-left">Password Policy & Privileged Access</h2>
    </div>
    <!-- Video Content -->
    <div class="start-screen text-center"> <img src="../images/module01-start.png" alt="">
      <div>
          <asp:Button ID="btnStart"  class="btn infs-btn-secondary" runat="server" Text="START" OnClick="btnStart_Click" />
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
<script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script> 
  <script type="text/javascript" src="../js/swipe.js"></script> 
  <script type="text/javascript" src="../js/jquery.transform2d.js"></script> 
  <script type="text/javascript" src="../js/main.js"></script>
</form>
</html>
