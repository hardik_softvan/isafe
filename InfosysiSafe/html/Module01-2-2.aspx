﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module01-2-2.aspx.cs" Inherits="InfosysiSafe.html.Module01_2_2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Privileged Access - Scenario</h2>
      </div>
      <!-- Video Content -->
      <div class="module-intro">
        <div class="video-cont text-center" data-aos="zoom-in">
          <video id="myVideo" controls autoplay="autoplay" controlsList="nodownload" preload="none" poster="images/module1/Privileged_Access_Scenario.jpg">
            <source src="video/module1/Privileged_Access_Scenario.mp4" type="video/mp4">
          </video>
        </div>
      </div>
    </div>
  </div>
   <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-drag.png" alt=""></div>
    <p>Click on the next button after viewing the content</p>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
      <script>
      $(".infs-instruction").click(function(){
          $(".infs-instruction").toggleClass("infs-instruction1");
        });
  </script>
</form>
</body>
</html>
