﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module03-1-4.aspx.cs" Inherits="InfosysiSafe.html.Module03_1_4" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
    <link rel="stylesheet" href="../css/animatecheckcross.css">
    <script src="../js/jquery.js"></script>
    <link rel="stylesheet" href="../css/popup.css">
	
	<style>
		.internet-activity p{
		border-radius: 5px;
		background: #ffe082;
		 font-style: italic;
		 padding: 1rem;
		 }
	    .dim {
            opacity:0.5;
	    }
	    .check svg { top: 39% !important; left: 29% !important; }
	    .cross svg { top: 39% !important; left: 29% !important; }
	</style>
    <script>
var img = 1;
var ActivityScrore = 0;
var behavior = "";
function btnGood() {
    behavior = "Good";
    Activity(behavior);
}

function btnBad() {
    behavior = "Bad";
    Activity(behavior);
}
function onload() {
    $('#img').attr("src", "../images/Activity/GoodBad1.jpg");
	$('#msg').html("<p><b>Scenario 1 : </b>An Infoscion is surfing the internet when a pop-up asking to install an unknown application appears on the browser. He immediately cancels it.</p>")
}

        function usefulltip()
        {

        }

        function Activity(behavior) {
             disablelikebtn('#btn'+behavior);
            $("#usefultips").removeClass('d-none');
            if (img == 1) {
                $("#tip").html('It is always advisable to terminate the browser program, because the software may contain a virus or a Trojan.');
                if (behavior == "Good") {
                    Answer("check");
                }
                else {
                    wrong("cross");
                }
                img = 2;
            } else if (img == 2) {
                $("#tip").html('Uploading confidential information on unapproved platforms is prohibited.');
               // disablelikebtn();
                if (behavior == "Bad") {
                    Answer("check");
                }
                else {
                    wrong("cross");
                }
                img = 3;
            } else if (img == 3) {
                $("#tip").html('Avoid installing software directly from the internet. Before installing any software on your computer, visit SSVP portal (http://ssvp/) and follow the software exception process');
               // disablelikebtn();
                if (behavior == "Bad") {
                    Answer("check");
                }
                else {
                    wrong("cross");
                }
                img = 4;
            } else if (img == 4) {
                $("#tip").html('Watching online videos which are not relevant to business or playing games online is strictly prohibited.');
               // disablelikebtn();
                if (behavior == "Bad") {
                    Answer("check");
                }
                else {
                    wrong("cross");
                }
                img = 5;
            }
            else if (img == 5) {
                $("#tip").html('Bypassing corporate internet access through usage of unapproved proxies is strictly prohibited If you want to get a site unblocked for valid business reasons, please approach IT team with approvals.');
               // disablelikebtn();
                if (behavior == "Bad") {
                    Answer("check");
                }
                else {
                    wrong("cross");
                }
                img = 6;
            }
            else if (img == 6) {
                $("#tip").html('Never upload Infosys or client confidential information on social media websites');
               // disablelikebtn();
                if (behavior == "Bad") {
                    Answer("check");
                }
                else {
                    wrong("cross");
                }
                img = 7;
            }
            else if (img == 7) {
                $("#tip").html('Report Information Security Incidents as soon as you notice it. It helps us to reduce the impact and contain further damage.');
               // disablelikebtn();
                if (behavior == "Good") {
                    Answer("check");
                }
                else {
                    wrong("cross");
                }
            }
        }

/****************************************************/
        var imgnext=1;
        function btnNext()
        {
            $("#usefultips").addClass('d-none');
           if (imgnext == 1) {
             disableNxtBtn();
        // $("#tip").html('It is always advisable to terminate the browser program, because the software may contain a virus or a Trojan.');
		$('#msg').html("<p><b>Scenario 2 : </b>An employee uploads confidential files from his laptop onto external cloud storage site.</p>")
               if (behavior == "Good") {
            ActivityScrore = ActivityScrore + 1;
        }
        $('#img').attr("src", "../images/Activity/GoodBad2.jpg");
        imgnext = 2;
         } else if (imgnext == 2) {
             disableNxtBtn();
            // $("#tip").html('Uploading confidential information on unapproved platforms is prohibited.');
			 $('#msg').html("<p><b>Scenario 3 : </b>An employee downloads an unauthorized software on Infosys provided laptop.</p>") 
               if (behavior == "Bad") {
               ActivityScrore = ActivityScrore + 1;
        }
        $('#img').attr("src", "../images/Activity/GoodBad3.jpg");
        imgnext = 3;
         } else if (imgnext == 3) {
             disableNxtBtn();
        
       // $("#tip").html('Avoid install software directly from the internet. Before installing any software on your computer, visit SSVP portal (http://ssvp/) and follow the software exception process');
		 $('#msg').html("<p><b>Scenario 4 : </b>An employee watches movie trailers on company laptop .</p>")
        if (behavior == "Bad") {
            ActivityScrore = ActivityScrore + 1;
        }
        $('#img').attr("src", "../images/Activity/GoodBad4.jpg");
        imgnext = 4;
         } else if (imgnext == 4) {
             disableNxtBtn();
        
        // $("#tip").html('Watching online videos which are not relevant to business or playing games online is strictly prohibited.');
		  $('#msg').html("<p><b>Scenario 5 : </b>An Employee bypasses the proxy settings to open blocked or malicious website.</p>")
        if (behavior == "Bad") {
            ActivityScrore = ActivityScrore + 1;
        }
        $('#img').attr("src", "../images/Activity/GoodBad5.jpg");
        imgnext = 5;
    }
         else if (imgnext == 5) {
             disableNxtBtn();
        
         //  $("#tip").html('Bypassing corporate internet access through usage of unapproved proxies is strictly prohibited If you want to get a site unblocked for valid business reasons, please approach IT team with approvals.');
		  $('#msg').html("<p><b>Scenario 6 : </b>An employee posts client-related information on social media account.</p>")
        if (behavior == "Bad") {
            ActivityScrore = ActivityScrore + 1;
        }
        $('#img').attr("src", "../images/Activity/GoodBad6.jpg");
        imgnext = 6;
    }
         else if (imgnext == 6) {
             disableNxtBtn();
       
        // $("#tip").html('Never upload Infosys or client confidential information on social media websites');
		 $('#msg').html("<p><b>Scenario 7 : </b>An employee receives an email from a shopping site with an offer to win an iPhone by clicking a link and responding to a simple survey. Employee identifies this as a phishing email and reports it to the Information Security team.</p>")
		 
        if (behavior == "Bad") {
            ActivityScrore = ActivityScrore + 1;
        }
        $('#img').attr("src", "../images/Activity/GoodBad7.jpg");
        imgnext = 7;
        }
         else if (imgnext == 7) {
             disableNxtBtn();
        
           //  $("#tip").html('Report Information security Incidents as soon as you notice it. It helps us to reduce the impact and contain further damage.');
			  
        if (behavior == "Good") {
            ActivityScrore = ActivityScrore + 1;
        }
        $("#myModal").modal('show');
        $('#score').text(ActivityScrore);
        $('#img').attr("src", "../images/Activity/GoodBad7.jpg");
        $.ajax({
        url: "Module03-1-4.aspx/btnActivity",
        data: JSON.stringify({ "TotalScore": ActivityScrore}),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
        }
    });

    }


        }

        function disablelikebtn(behavior)
        {
            document.getElementById("btnGood").disabled = true;
            document.getElementById("btnBad").disabled = true;
            document.getElementById("btnNex").disabled = false;
            $(behavior).addClass('dim');
          //$("#btnBad").addClass('dim');
            $("#btnNex").removeClass('dim');
        }

        function disableNxtBtn()
        {
            document.getElementById("btnNex").disabled = true;
            document.getElementById("btnGood").disabled = false;
            document.getElementById("btnBad").disabled = false;
            $("#btnGood").removeClass('dim');
            $("#btnBad").removeClass('dim');
            $("#btnNex").addClass('dim');
        }



     function Answer(useranswer) {
    $('#audiocheck').get(0).play();
    $("#Result").html("<div class='" + useranswer + "''><svg  class='checkmark' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 52 52'><circle class='checkmark_circle_success' cx='26' cy='26' r='25' fill='#7ac142'/><path class='checkmark_check' fill='none' d='M14.1 27.2l7.1 7.2 16.7-16.8'/></svg></div>");
    setTimeout(function () {
        $("#Result").html("");
    }, 2000);
        }

        function wrong(useranswer) {
    $('#audiocross').get(0).play();
    $("#Result").html("<div class='" + useranswer + "'><svg  class='checkmark' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 52 52'><circle class='checkmark_circle_error' cx='26' cy='26' r='25' fill='#ee3939'/><path class='checkmark_check' fill='#ee3939' d='M16 16 36 36 M36 16 16 36 red'/></svg></div>");
    setTimeout(function () {
        $("#Result").html("");
    }, 2000);
}


    </script>
</head>
<body class="loaded-content congrats-bg" onload=" onload();">
<form id="form1" runat="server">
  <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Main Content -->
  <div class="infs-body-container">
  <div class="container">
    <div class="module-title">
      <h2 class="infs-heading" data-aos="fade-left"  data-aos-anchor="#trigger-left" data-aos-offset="500" data-aos-duration="500">Identify good and bad internet usage behavior - Activity</h2>
    </div>
    <!-- Good Bad Activity -->
    <div class="goodbad-activity">
      <div class="row">
        <div class="col-md-7">
          <div class="activity-img"> 
               <img id="img"  alt=""> 
			   <div class="internet-activity" id="msg"></div>
				</div>
        </div>
        <div class="col-md-5">
          <div class="activity-rhs">
            <h4 class="question">Is it a <span class="success">GOOD</span> or <span class="danger">BAD</span> behaviour?</h4>
            <div class="goodbad-btns text-center">
                <input id="btnGood" type="image" src="../images/btn-good.png" class="btn-good" onclick="btnGood(); return false;">
                <input id="btnBad" type="image" src="../images/btn-bad.png" class="btn-bad" onclick="btnBad(); return false;">
                <input type="button"  value="Next" onclick="btnNext()" id="btnNex" class="btn infs-btn-secondary" style="margin-bottom:15px;">
            </div>
            <div id="usefultips" class="useful-tips  d-none"> <img src="../images/icons/icon-idea.png" alt="">
              <h4>USEFUL TIP</h4>
              <p id ="tip"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

      <div id="myModal" class="modal fade"  data-backdrop="static" >
    <div class="modal-dialog">
		
        <div class="modal-content text-center">
           
			
                <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
              <div class="modal-body">
				<h5><b>You have answered <span id="score"></span> out of 7 correctly.</b></h5>
				<asp:Button ID="btnStart"  class="btn infs-btn-secondary infs-btn-secondary-popup" OnClick="btnStart_Click" runat="server" Text="CONTINUE" />
				
			 </div>
			 <!--<div class="modal-footer">
			 
			 </div>-->
		</div>
    </div>
</div>
       <div id="Result"></div>


  <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-idea.png" alt=""></div>
    <p>Identify the mistakes in the image by clicking on them.</p>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
      <script>
          $(".infs-instruction").click(function () {
              $(".infs-instruction").toggleClass("infs-instruction1");
          });
  </script>
</form>
    <audio id="audiocross">
  <source src="../audio/Wrong.wav" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
    <audio id="audiocheck">
  <source src="../audio/Right.wav" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
</body>
</html>
