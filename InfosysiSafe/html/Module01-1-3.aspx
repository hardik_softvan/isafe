﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module01-1-3.aspx.cs" Inherits="InfosysiSafe.html.Password_Policy_Activity" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/swipe.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
<link rel="stylesheet" href="../css/animatecheckcross.css">
<link rel="stylesheet" href="../css/popup.css">
<script src="../js/popup111.min.js"></script>

</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
       <div class="hide" id="availableLanguagesContainerBody"><span>video</span> </div>
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container-fluid">
      <div class="module-title">
        <h2 class="infs-heading animation-left">DO’s & DON’Ts of Password Policy</h2>
      </div>
      <div class="row">
        <div class="activitywrap" id="tinderslide">
          <ul>
		  <li>
              <div class="buddy siutaions">
                <div class="avatar">Use your name, username or user ID as password 
                  <div class="dosnumber"><span>12</span>Question</div>
                </div>
              </div>
            </li>
		  <li>
              <div class="buddy siutaions">
                <div class="avatar">Set different passwords for different accounts
                  <div class="dosnumber"><span>11</span>Question</div>
                </div>
              </div>
            </li>
			<li>
              <div class="buddy siutaions">
                <div class="avatar">Ask a colleague for his/her password
                  <div class="dosnumber"><span>10</span>Question</div>
                </div>
              </div>
            </li>
			<li>
              <div class="buddy siutaions">
                <div class="avatar">Hardcode username and password in source code
                  <div class="dosnumber"><span>9</span>Question</div>
                </div>
              </div>
            </li>
			<li>
              <div class="buddy siutaions">
                <div class="avatar">Change the default password after you log in to a system for the first time
                  <div class="dosnumber"><span>8</span>Question</div>
                </div>
              </div>
            </li>
			<li>
              <div class="buddy siutaions">
                <div class="avatar"> Share passwords/pins of VPN, RSA tokens and client provided credentials
                  <div class="dosnumber"><span>7</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar">Use ‘remember password’ feature on web pages 
                  <div class="dosnumber"><span>6</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar">Write a password on a notepad
                  <div class="dosnumber"><span>5</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                  <div class="avatar">Share your password with anyone (including your manager) 
                  <div class="dosnumber"><span>4</span>Question</div>
                </div>
                
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar">Use different combinations of letters, digits, and special characters in a password 
                  <div class="dosnumber"><span>3</span>Question</div>
                </div>
              </div>
            </li>
            <li>
              <div class="buddy siutaions">
                <div class="avatar">Change your password every 60 days 
                  <div class="dosnumber"><span>2</span>Question</div>
                </div>
               </div>
            </li>
            <li>
              <div class="buddy siutaions">
                  <div class="avatar">Sharing passwords over email
                  <div class="dosnumber"><span>1</span>Question</div>
                </div>
                
              </div>
            </li>
          </ul>
          <div class="actions"> <a href="#" class="dislike"><i></i></a> <a href="#" class="like"><i></i></a> </div>
           <div id="Result"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-drag.png" alt=""></div>
    <p>Drag left or right to make your choice</p>
  </div>
    
<div id="myModal" class="modal fade"  data-backdrop="static" >
    <div class="modal-dialog">
        <div class="modal-content text-center">
              <div class="modal-body">
				<h5><b>You got <span id="score"></span> out of 12 right.</b></h5>
				<asp:Button ID="btnStart"  class="btn infs-btn-secondary infs-btn-secondary-popup" OnClick="btnStart_Click"  runat="server" Text="CONTINUE" />
			 </div>
		</div>
    </div>
</div>

  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script> 
  <script type="text/javascript" src="../js/swipe.js"></script> 
  <script type="text/javascript" src="../js/jquery.transform2d.js"></script> 
  <script type="text/javascript" src="../js/main.js"></script>
    <script>
      $(".infs-instruction").click(function(){
          $(".infs-instruction").toggleClass("infs-instruction1");
});
  </script>
</form>
       <audio id="audiocross">
  <source src="../audio/Wrong.wav" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
    <audio id="audiocheck">
  <source src="../audio/Right.wav" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
</body>
</html>
