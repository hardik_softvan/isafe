﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module02-3-2.aspx.cs" Inherits="InfosysiSafe.html.Module02_3_2" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />  
<script src="../js/jquery.js"></script>
<link rel="stylesheet" href="../css/popup.css">
<script src="../js/popup111.min.js"></script>
    <style>
        #Progress {
    width: 100%;
    background-color: grey;
}
        .timer
        {
         color:white;
         font-weight:bold;
        }
    </style>

</head>
<body class="loaded-content rapidfire-bg">
    <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="">
      <div class="rapidfire-quiz" > 
        <!----  Counter Timer ---->
        <asp:HiddenField ID="hdnUserId" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdn" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer1" Value="0" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer2" Value="0" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer3" Value="0" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer4" Value="0" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnAnswer5" Value="0" ClientIDMode="Static" runat="server" />

        <asp:Literal ID="ltrAssessment" runat="server"></asp:Literal>
        <asp:Panel ID = "Panel1" runat="server"></asp:Panel>
        <div class="text-center mt-4 d-none" id="btnsbmit"  runat="server">
          <%--<asp:Button ID="btnSubmit" CssClass="btn infs-btn-secondary mb-4" runat="server" Text="Submit"  OnClientClick="btnSub()" />--%>
           <input type="button" id="btn5"  class="btn infs-btn-secondary mb-4 d-none" value="Submit"   onclick="btnSub()" />
        </div>
          
      </div>
    </div>
  </div>

 <div id="myModal" class="modal fade"  data-backdrop="static" >
    <div class="modal-dialog">
        <div class="modal-content text-center">
              <div class="modal-body">
				<h5><b>You got <span id="score"></span> out of 5 right.</b></h5>
				<asp:Button ID="btnStart"  class="btn infs-btn-secondary infs-btn-secondary-popup" OnClick="btnSubmit_Click"  runat="server" Text="CONTINUE" />
			 </div>
		</div>
    </div>
</div>
  <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-idea.png" alt=""></div>
    <p>Select the right option & click 'Submit'</p>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
    <script src="../js/rapidfire.js"></script>
    <script>
        $(".infs-instruction").click(function () {
            $(".infs-instruction").toggleClass("infs-instruction1");
        });
  </script>
    

</form>
</body>
</html>
