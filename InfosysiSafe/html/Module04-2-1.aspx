﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module04-2-1.aspx.cs" Inherits="InfosysiSafe.html.Module04_2_1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="module-title">
        <h2 class="infs-heading animation-left">Information Security Policies</h2>
      </div>
      <!-- Policies Content -->
      <div class="security-policies">
        <div class="row">
          <div class="col-md-6">
            <div class="policies-content policy-eq-col">
              <div class="policy-header"> <img src="../images/icons/icon-internet-policy.png" alt="">
                <div class="header-text">
                  <h6>Information Security Policy</h6>
                  <p class="margin-0">Information Security Group (ISG) has undertaken a detailed exercise on refining 15 of the existing ISMS policies for Infosys Group.
                      These policies are high-level statements covering management intent and expectations on cybersecurity. 
                  </p>
                </div>
              </div>
              <div class="policy-list">
                <p class="bold text-uppercase animation-left">Excerpts from the policy</p>
                <ul class="infolist">
                  <li class="animation-left">Infosys limited and its subsidiaries.</li>
                  <li class="animation-left">All employees, third parties, vendors, consultants, contractors, customer representatives and others who are allowed access to information assets, IT infrastructure and relevant resources.</li>
                  <li class="animation-left">Information assets, infrastructure and resources that are owned, leased, hosted within or in third party datacenters.</li>
                     <li class="animation-left">Information that is entrusted by employees, customers, vendors, investors and the public at large.</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="policies-content policy-eq-col">
              <div class="policy-header"> <img src="../images/icons/icon-acceptable-policy.png" alt="">
                <div class="header-text">
                  <h6>Acceptable Usage Policy</h6>
                  <p class="margin-0">
This policy provides the guidance and Infosys’s rules on how each individual shall use the Infosys resources such as Information system, software, computer equipment, connectivity, etc., and technology that is used to handle Infosys’s information.</p>
                </div>
              </div>
              <div class="policy-list">
                <p class="bold text-uppercase animation-left"> This policy is applicable to:</p>
                <ul class="infolist">
                  <li class="animation-left">Infosys limited and its subsidiaries.</li>
                  <li class="animation-left">All employees, third parties, vendors, consultants, contractors, customer representatives and others who are allowed access to information assets, IT infrastructure and relevant resources.</li>
                  <li class="animation-left">Information assets, infrastructure and resources that are owned, leased, hosted within or in third party datacenters.</li>
                    <li class="animation-left">Information that is entrusted by employees, customers, vendors, investors and the public at large.</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script> 
  <script>
	$(function () {
		$('.policy-eq-col').matchHeight();
	});
  </script>
</form>
</body>
</html>
