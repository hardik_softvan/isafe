﻿using InfosysiSafe.API;
using InfosysiSafe.Domain;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.html
{
    public partial class Module04_4_6 : System.Web.UI.Page
    {
        string CorrectAnswer1, CorrectAnswer2, CorrectAnswer3, CorrectAnswer4, CorrectAnswer5;
        string UserAnswer1, UserAnswer2, UserAnswer3, UserAnswer4, UserAnswer5;
        int Totalresult = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
#if DEBUG

                //Session["Module1AssesmentID"] = "1";
#endif
                ViewState["userid"] = Session["Module1AssesmentID"].ToString();
                BindData();
            }
        }

        private void BindData()
        {
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            UserLogin loginAPI = new UserLogin(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> filter = new Dictionary<string, object>();
            int module = Convert.ToInt32(Session["hdnModel"]);
            int questionumber = 1;
            filter.Add("Name", "Random5");
            filter.Add("intMstModuleId", module);
            StringBuilder sb1 = new StringBuilder();
            List<IPersistantObject> questions = Global.Store.Find("iSafeMstQuestion", filter);
            List<iSafeMstQuestion> questionlist = new List<iSafeMstQuestion>();
            for (int i = 0; i < questions.Count; i++)
            {
                var jdata = questions[i];
                string jsondata = Convert.ToString(jdata).Replace("\"", "\'");
                int quetionid = 0;
                int optioncount = 1;
                JArray parsedArray = JArray.Parse(jsondata);
                foreach (JObject parsedObject in parsedArray.Children<JObject>())
                {
                    if (questionumber == 1)
                    {
                        sb1.Append("<div id='q" + questionumber + "' class=''>");
                    }
                    else
                    {
                        sb1.Append("<div id='q" + questionumber + "' class='d-none'>");
                    }
                    foreach (JProperty parsedProperty in parsedObject.Properties())
                    {
                        string propertyName = parsedProperty.Name;

                        if (propertyName.Equals("id"))
                        {
                            quetionid = (int)parsedProperty.Value;
                        }
                        if (propertyName.Equals("txtQuestion"))
                        {
                            string propertyValue = (string)parsedProperty.Value;
                            ViewState["" + questionumber + ""] = quetionid;
                            sb1.Append("<div class=\"module-title\"><h3 class='infs-heading'>Question " + questionumber + " of 5</h3></div><h4  class=\"questions\"> " + propertyValue + "</h4><div class=\"row\">");
                        }

                        if (propertyName.Contains("intCorrectOptNo"))
                        {
                            string propertyValue = (string)parsedProperty.Value;
                            ViewState["ans" + questionumber + ""] = propertyValue;
                        }

                        if (propertyName.Contains("txtOpt"))
                        {
                            string propertyValue = (string)parsedProperty.Value;
                            if (propertyValue != "")
                            {
                                sb1.Append("<div class=\"col-sm-6\"><div class='answers'><input type='radio' onclick='$(\"#hdnAnswer" + questionumber + "\").val(\"" + optioncount + "\")' id='qname" + quetionid + "_" + optioncount + "' name='qname" + quetionid + "' value=" + questionumber + " runat=\"server\">  <label for=\"qname" + quetionid + "_" + optioncount + "\"><span>" + propertyValue + "</span></label><span class=\"answeroption\">" + optioncount + "</span></div></div>");
                            }
                                optioncount++;
                        }
                    }
                    int previous = questionumber - 1;
                    int questionumberLink = questionumber + 1;
                    if (questionumber == 1)
                    {
                        string answer = "qname" + quetionid;
                        sb1.Append("</div><div class=\"text-center mt-4\"><a href='javascript:onclick=q" + questionumber + "();function q" + questionumber + "(){if($(\"#hdnAnswer" + questionumber + "\").val()==\"\"){ $(\"#lblSelection\").show();}else{$(\"#q" + questionumber + "\").addClass(\"d-none\");$(\"#q" + questionumberLink + "\").removeClass(\"d-none\");$(\"#lblSelection\").hide();}};' class=\"btn infs-btn-secondary\" >Next </a></div>");


                    }
                    else if (questionumber == 4)
                    {
                       sb1.Append("</div><div class=\"text-center mt-4\"><a href='javascript:onclick=q" + questionumber + "();function q" + questionumber + "(){$(\"#q" + questionumber + "\").addClass(\"d-none\");$(\"#q" + previous + "\").removeClass(\"d-none\");$(\"#lblSelection\").hide();}' class=\"btn infs-btn-secondary prev mr-2\" > Previous </a><a href='javascript:onclick=q" + questionumber + "();function q" + questionumber + "(){if($(\"#hdnAnswer" + questionumber + "\").val()==\"\"){ $(\"#lblSelection\").show();}else{$(\"#q" + questionumber + "\").addClass(\"d-none\");$(\"#q" + questionumberLink + "\").removeClass(\"d-none\");$(\"#btnsbmit\").removeClass(\"d-none\");$(\"#lblSelection\").hide();}};' class=\"btn infs-btn-secondary\" > Next </a></div>");
                    }
                    else if (questionumber == 5)
                    {
                        sb1.Append("</div>");
                    }
                    else
                    {
                        Panel1.Visible = false;
                        sb1.Append("</div><div class=\"text-center mt-4\"><a href='javascript:onclick=q" + questionumber + "();function q" + questionumber + "(){$(\"#q" + questionumber + "\").addClass(\"d-none\");$(\"#q" + previous + "\").removeClass(\"d-none\");$(\"#lblSelection\").hide();}' class=\"btn infs-btn-secondary prev mr-2\" > Previous </a><a href='javascript:onclick=q" + questionumber + "();function q" + questionumber + "(){if($(\"#hdnAnswer" + questionumber + "\").val()==\"\"){ $(\"#lblSelection\").show();}else{$(\"#q" + questionumber + "\").addClass(\"d-none\");$(\"#q" + questionumberLink + "\").removeClass(\"d-none\");$(\"#lblSelection\").hide();}};' class=\"btn infs-btn-secondary\" > Next </a></div>");
                    }
                    sb1.Append("</div>");

                    questionumber++;
                }
                iSafeMstQuestion question = (iSafeMstQuestion)questions[i];
                questionlist.Add(question);
            }

            var abc = questionlist.Count;
            ltrAssessment.Text = sb1.ToString();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string Question1 = ViewState["1"].ToString();
            string Question2 = ViewState["2"].ToString();
            string Question3 = ViewState["3"].ToString();
            string Question4 = ViewState["4"].ToString();
            string Question5 = ViewState["5"].ToString();
            CorrectAnswer1 = ViewState["ans1"].ToString();
            CorrectAnswer2 = ViewState["ans2"].ToString();
            CorrectAnswer3 = ViewState["ans3"].ToString();
            CorrectAnswer4 = ViewState["ans4"].ToString();
            CorrectAnswer5 = ViewState["ans5"].ToString();
            UserAnswer1 = hdnAnswer1.Value.ToString();
            UserAnswer2 = hdnAnswer2.Value.ToString();
            UserAnswer3 = hdnAnswer3.Value.ToString();
            UserAnswer4 = hdnAnswer4.Value.ToString();
            UserAnswer5 = hdnAnswer5.Value.ToString();
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            AssessmentQuestion AssessmentQuestionAPI = new AssessmentQuestion(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("intMstModuleId", ViewState["userid"].ToString());
            input.Add("Question1", Question1.ToString());
            input.Add("Answer1", UserAnswer1);
            input.Add("Question2", Question2.ToString());
            input.Add("Answer2", UserAnswer2);
            input.Add("Question3", Question3.ToString());
            input.Add("Answer3", UserAnswer3);
            input.Add("Question4", Question4.ToString());
            input.Add("Answer4", UserAnswer4);
            input.Add("Question5", Question5.ToString());
            input.Add("Answer5", UserAnswer5);
            Dictionary<string, object> output = AssessmentQuestionAPI.Process(input);
            if (output.ContainsKey("error"))
            {
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Button_Alert", "alert('Submited Sucessfully');", true);
            }
            result();
        }
        public void result()
        {
            if (CorrectAnswer1 == UserAnswer1.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 20;
            }
            if (CorrectAnswer2 == UserAnswer2.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 20;
            }
            if (CorrectAnswer3 == UserAnswer3.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 20;
            }
            if (CorrectAnswer4 == UserAnswer4.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 20;
            }
            if (CorrectAnswer5 == UserAnswer5.Replace("ans", "").ToString())
            {
                Totalresult = Totalresult + 20;
            }
            Session["TopicValue"] = "Assessment Result";
            if (Totalresult > 70)
            {
                iSafeTrnAssessment iSafeTrnAssessmentDomain = new iSafeTrnAssessment();
                iSafeTrnAssessmentDomain.intId = Convert.ToUInt32(ViewState["userid"].ToString());
                iSafeTrnAssessmentDomain.intScore = Totalresult;
                iSafeTrnAssessmentDomain.intMstModuleId = Convert.ToInt32(Session["hdnModel"].ToString());
                iSafeTrnAssessmentDomain.flgResult = false;
                iSafeTrnAssessmentDomain.dtAssessmentStartAt = DateTime.Now;
                iSafeTrnAssessmentDomain.dtAssessmentCompletedAt = DateTime.Now;
                Global.Store.Update(iSafeTrnAssessmentDomain);
                Dictionary<string, object> TopicCnt = new Dictionary<string, object>();
                TopicCnt.Add("Name", "ModelCount");
                TopicCnt.Add("Modelid", Convert.ToInt32(Session["hdnModel"].ToString()));
                List<IPersistantObject> Topic = Global.Store.Find("iSafeMstPage", TopicCnt);
                Session["PageSequence"] = Topic.Count + 0.5;
                Response.Redirect("congratulations.aspx");
            }
            else
            {
                iSafeTrnAssessment iSafeTrnAssessmentDomain = new iSafeTrnAssessment();
                iSafeTrnAssessmentDomain.intId = Convert.ToUInt32(ViewState["userid"].ToString());
                iSafeTrnAssessmentDomain.intScore = Totalresult;
                iSafeTrnAssessmentDomain.intMstModuleId = Convert.ToInt32(Session["hdnModel"].ToString());
                iSafeTrnAssessmentDomain.flgResult = false;
                iSafeTrnAssessmentDomain.dtAssessmentStartAt = DateTime.Now;
                iSafeTrnAssessmentDomain.dtAssessmentCompletedAt = DateTime.Now;
                Global.Store.Update(iSafeTrnAssessmentDomain);
                Response.Redirect("fail.aspx");
            }

        }
    }
}