﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module02-3-1.aspx.cs" Inherits="InfosysiSafe.html.Module02_3_1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
     <link href="../css/no-scroll.css" rel="stylesheet" />  
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
     <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <!-- Main Content Instructions -->
  <div class="infs-body-container">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-5 text-center mb-4"> <img src="../images/policy.png" alt=""> </div>
        <div class="col-md-9 col-sm-7">
          <h3 class="infs-heading infs-re-heading animation-left">Information Leakage - Rapid Fire Quiz - Activity Instructions</h3>
          <div class="reinforcement-box">
            <ul class="infolist">
			  
			  <li class="animation-left">You will be asked <b> 5 multiple choice questions</b></li>
			  <li class="animation-left">The question will be based on the topic - <b>Information Leakage </b></li>
              <li class="animation-left">You will get only <b>ten seconds </b> to answer all questions</li>
			  <li class="animation-left">You can check the <b>time in the clock given on the left hand side</b></li>
			  <li class="animation-left">You need to <b> select the correct answer </b> from the available options</li>
              
             
            </ul>
          </div>
        </div>
        <div class="text-center mt-4 margin-center">
          <asp:Button ID="btnStart" CssClass="btn-block infs-btn-secondary" runat="server" Text="Start" ClientIDMode="Static" OnClick="btnStart_Click" />
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
