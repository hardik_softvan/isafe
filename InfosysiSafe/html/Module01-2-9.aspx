﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module01-2-9.aspx.cs" Inherits="InfosysiSafe.html.Module01_2_9" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link rel="stylesheet" href="../css/no-scroll.css" />
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
  <div class="infs-body-container">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 text-center mb-4"> <img src="../images/mascot1.png" alt=""> </div>
        <div class="col-sm-9">
          <h2 class="infs-heading infs-re-heading animation-left">Module Reinforcement</h2>
          <div class="reinforcement-box">
            <ul class="infolist">
              <li class="animation-left"><span class="danger">DO NOT</span> share your password with anyone under any circumstances</li>
              <li class="animation-left">Create a strong password that complies with the Infosys Password Policy</li>
              <li class="animation-left">Use a passphrase for added security</li>
              <li class="animation-left">Use the privileged access only for official purpose; <span class="danger">DO NOT</span> misuse it or share it with anyone</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
