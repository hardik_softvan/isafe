﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Module01-2-8.aspx.cs" Inherits="InfosysiSafe.html.Module01_2_8" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta content="BlendTrans(Duration=0.01)" http-equiv="Page-Exit" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/swipe.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
<link href="../css/no-scroll.css" rel="stylesheet" />
<link rel="stylesheet" href="../css/animatecheckcross.css">
<script src="../js/popup111.min.js"></script>
<link rel="stylesheet" href="../css/popup.css">
   
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
  <!-- Main Content -->
    <div class="hide" id="availableLanguagesContainerBody"><span>video</span> </div>
  <div class="infs-body-container">
    <div class="container-fluid">
      <div class="module-title">
        <h2 class="infs-heading animation-left">DO's & DON'Ts of Privileged Access</h2>
      </div>
      <div class="row">
        <div class="activitywrap" id="tinderslide">
          <ul>
			  <li>
                  <div class="buddy siutaions">
                    <div class="avatar-privileged">As a privileged user, I have the discretion to perform activities without following change  management process  
                      <div class="dosnumber"><span>9</span>Question</div>
                    </div>
                  </div>
                </li>
				<li>
                  <div class="buddy siutaions">
                    <div class="avatar-privileged">I use WebEX session to share my screen with an ex-team member, so that he can use my admin credentials to fix an issue that I am struggling with
                      <div class="dosnumber"><span>8</span>Question</div>
                    </div>
                  </div>
                </li>
				<li>
                  <div class="buddy siutaions">
                    <div class="avatar-privileged">I have been asked to enable the USB access in a colleague’s laptop. Since this was not approved by her manager, I refused to do it
                      <div class="dosnumber"><span>7</span>Question</div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="buddy siutaions">
                    <div class="avatar-privileged">As a privileged user, I manipulate the logs under my control
                      <div class="dosnumber"><span>6</span>Question</div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="buddy siutaions">
                    <div class="avatar-privileged">I enable the USB drive of my system using my privileged access so that I can transfer some movies that I earlier downloaded, to an external hard disk
                      <div class="dosnumber"><span>5</span>Question</div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="buddy siutaions">
                    <div class="avatar-privileged">Even though I have privileged access, I should not install an unapproved software
                      <div class="dosnumber"><span>4</span>Question</div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="buddy siutaions">
                    <div class="avatar-privileged">I create a guest account for my personal visitor so he can use Infosys wifi to access his personal mail account
                      <div class="dosnumber"><span>3</span>Question</div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="buddy siutaions">
                    <div class="avatar-privileged">I am an application administrator. Because I know my colleague very well, I grant him access without his manager’s approval
                      <div class="dosnumber"><span>2</span>Question</div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="buddy siutaions">
                    <div class="avatar-privileged">I have been given privileged access temporarily for project work. I create another local admin user account to enjoy the privileged access even after my temporary privileged access expires 
                      <div class="dosnumber"><span>1</span>Question</div>
                    </div>
                  </div>
                </li>
              </ul>
          <div class="actions"> <a href="#" class="dislike"><i></i></a> <a href="#" class="like"><i></i></a> </div>
              <div id="Result"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- Foating Instructions -->
  <div class="infs-instruction" id="instruction">
    <div class="ins-icon"><img src="../images/icons/icon-drag.png" alt=""></div>
    <p>Drag left or right to make your choice</p>
  </div>
    <div id="myModal" class="modal fade" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content text-center">
              <div class="modal-body">
				<h5><b>You got <span id="score"></span> out of 9 right.</b></h5>
				 <asp:Button ID="btnStart"  class="btn infs-btn-secondary infs-btn-secondary-popup" OnClick="btnStart_Click" runat="server" Text="CONTINUE" />
			 </div>
		</div>
    </div>
</div>



  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script> 
  <script type="text/javascript" src="../js/swipe.js"></script> 
  <script type="text/javascript" src="../js/jquery.transform2d.js"></script> 
  <script type="text/javascript" src="../js/PasswordPolicy.js"></script>
    <script>
      $(".infs-instruction").click(function(){
          $(".infs-instruction").toggleClass("infs-instruction1");
});
  </script>
</form>
 <audio id="audiocross">
  <source src="../audio/Wrong.wav" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
    <audio id="audiocheck">
  <source src="../audio/Right.wav" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
</body>
</html>
