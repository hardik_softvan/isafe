﻿using InfosysiSafe.API;
using InfosysiSafe.Domain;
using InfosysiSafe.User;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.html
{
    public partial class congratulations : System.Web.UI.Page
    {
        DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
        Dictionary<string, object> input = new Dictionary<string, object>();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                int EmployeeId = Convert.ToInt32(Session["EmployeeId"]);
                int userId = Convert.ToInt32(Session["userId"]);
                int Module = Convert.ToInt32(Session["hdnModel"]);
                string Result = "Pass";

                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter.Add("Name", "ByAttemptCount");
                filter.Add("intMstModuleid", Module);
                filter.Add("intMstUserId", userId);
                List<IPersistantObject> Attempt = Global.Store.Find("iSafeTrnAssessment", filter);
                //  List<iSafeTrnAssessment> AttemptCount = new List<iSafeTrnAssessment>();
                // iSafeTrnAssessment attemptCnt = (iSafeTrnAssessment)Attempt[0];

                Dictionary<string, object> filter1 = new Dictionary<string, object>();
                filter1.Add("Name", "ByAssessmentScore");
                filter1.Add("intMstModuleid", Module);
                filter1.Add("intMstUserId", userId);
                List<IPersistantObject> Assessment = Global.Store.Find("iSafeTrnAssessment", filter1);
                List<iSafeTrnAssessment> Assess = new List<iSafeTrnAssessment>();
                iSafeTrnAssessment AssessmentDetails = (iSafeTrnAssessment)Assessment[0];

                int AttemptNumber = Attempt.Count;
                int AssessmentScore = AssessmentDetails.intScore;
                DateTime AssessmentCompletedAt = Convert.ToDateTime(AssessmentDetails.dtAssessmentCompletedAt);
                // DateTime AssessmentCompletionDate = DateTime.ParseExact(Convert.ToString(AssessmentCompletedAt), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);//date
                string AssessmentCompletedTime = AssessmentDetails.dtAssessmentCompletedAt.ToString();
                DateTime AssessmentCompletionTime = DateTime.Parse(AssessmentCompletedTime);
                //string AssessmentTimeComplete = AssessmentCompletionTime.ToShortTimeString();//time
                string ExceptionStatus = "Yes";


                hdEmployeeId.Value = EmployeeId.ToString();
                hdModule.Value = Module.ToString();
                hdAttemptNumber.Value = AttemptNumber.ToString();
                hdAssessmentScore.Value = AssessmentScore.ToString();
                hdResult.Value = Result.ToString();
                hdAssessmentCompletedAt.Value = AssessmentCompletedAt.ToString();
                hdAssessmentCompletionTime.Value = AssessmentCompletedAt.ToString();
                hdExceptionStatus.Value = ExceptionStatus;
                msg();

                if (Module == 4)
                {
                    Session["Pledge"] = "1";
                }
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(System.Web.UI.Page), "Script", "function sendResultTest(){var obj = {'EmployeeId': " + EmployeeId + ",'Module':" + Module + ",'AttemptNumber':" + AttemptNumber + ",'AssessmentScore':" + AssessmentScore + ",'Result':" + Result + ",'AssessmentCompletedTime':" + AssessmentCompletionTime + ",'AssessmentCompletionTime':" + AssessmentCompletionTime + ",'ExceptionStatus':" + ExceptionStatus + "};  $.ajax({url: 'http://localhost:61619/Certificate.asmx/GetEmployeeId',data: JSON.stringify(obj),contentType: 'application/json; charset=utf-8',method:'post',dataType:'json',success: function (result) {alert(result.d);},error: function (err) {alert(err);}}); }};sendResultTest();", true);
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(System.Web.UI.Page), "Script", "sendResult();", true);


            }
        }

        protected void btnStart_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Session["hdnModel"]) != 4)
            {
                Index IndexAPI = new Index(dbAuthenticator, Global.Store, Global.Logger);
            input.Add("intMstUserid", Convert.ToString(Session["userId"]));
            input.Add("Page", "4");
            input.Add("Pageid", Convert.ToString(Convert.ToInt32(Session["hdnPageSequence"]) + 1));
            input.Add("CurrentPageid", Convert.ToString(Convert.ToInt32(Session["hdnPageSequence"])));
            input.Add("Model", Session["hdnModel"].ToString());
            Dictionary<string, object> output = IndexAPI.Process(input);
            int PageSequence = Convert.ToInt32(output["SequenceNo"]);
            int lstpage = Convert.ToInt32(Session["hdnPageSeq"].ToString());
            if (lstpage <= PageSequence)
            {
                Session["hdnPageSessionType"] = "nextPage";
                Session["hdnPageSeq"] = output["SequenceNo"].ToString();
            }
            else
            {
                Session["hdnPageSessionType"] = "prevPage";
            }
            int currentpage = Convert.ToInt32(Session["hdnPageSequence"]) + 1;
            decimal ModelCnt = Convert.ToDecimal(output["ModelCount"]);
            Session["Pledge"] = output["Pledge"];
            IframePages(PageSequence, Convert.ToInt32(output["Model"]), output["PageName"].ToString(), Convert.ToInt32(Session["userId"]), ModelCnt, output["Topic"].ToString(), Convert.ToInt32(output["Pageid"]));
            }
            else
            {
                Session["TopicValue"] = "Pledge";
                Response.Redirect("/html/pledge.aspx");
            }
            }
            public void IframePages(int PageSequence, int Model, string PageName, int intMstUserid, decimal ModelCnt, string Topic, int Pageid)
        {
            int PageNextValue = Convert.ToInt32(PageSequence) + 1;
            int PagePrevValue = Convert.ToInt32(PageSequence) - 1;
            ViewState["PageSequence"] = PageSequence;
            ViewState["PageNextValue"] = PageNextValue;
            ViewState["PagePrevValue"] = PagePrevValue;
            ViewState["Model"] = Model;
            Session["Model"] = Model;
            Session["Pageid"] = Pageid;
            Session["PageSequence"] = PageSequence;
            Session["hdnPageSequence"] = PageSequence.ToString();
            Session["hdnModel"] = Model.ToString();
            Session["TopicValue"] = Topic;
            // hdnNextiFrameId.Value = PageNextValue.ToString();
            Session["hdnNextiFrameId"] = PageNextValue.ToString();
            // hdnEmpId.Value = Session["userId"].ToString();
            Session["hdnEmpId"] = Session["userId"].ToString();
            Session["hdnNextiFrameId"] = PageName;
            // hdnPreviFrameId.Value = PagePrevValue.ToString();
            Session["hdnPreviFrameId"] = PagePrevValue.ToString();
            decimal TopicCnt = 100 / ModelCnt;
            int Progressbar = Convert.ToInt32((Convert.ToInt32(PageSequence) * TopicCnt));
            Session["Progressbar"] = Progressbar;
            Response.Redirect(PageName);
        }
        public void msg()
        {
            if (Convert.ToInt32(Session["hdnModel"]) == 1)
            {
                ltrlSuccess.Text = "You have passed Module 1 – Password Policy & Privileged Access";
            }
            if (Convert.ToInt32(Session["hdnModel"]) == 2)
            {
                ltrlSuccess.Text = "You have passed Module 2 – Secure Ways of Handling Information";
            }
            if (Convert.ToInt32(Session["hdnModel"]) == 3)
            {
                ltrlSuccess.Text = "You have passed Module 3 - Acceptable Usage";
            }
            if (Convert.ToInt32(Session["hdnModel"]) == 4)
            {
                ltrlSuccess.Text = "You have passed Module 4 - Information Security Roles & Responsibilities of Employees";
            }
        }
    }
}