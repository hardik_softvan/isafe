﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="congratulations.aspx.cs" Inherits="InfosysiSafe.html.congratulations" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>.:: Infosys ::.</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Infosys">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/aos.css">
<link rel="stylesheet" href="../css/infosys.css">
 <link href="../css/no-scroll.css" rel="stylesheet" />
<script>
function sendResult() {
	setTimeout(function(){ 
		var EmployeeId = $("#hdEmployeeId").val();
		var Module = $('#hdModule').val();
		var AttemptNumber = $('#hdAttemptNumber').val();
		var AssessmentScore = $('#hdAssessmentScore').val();
		var Result = $('#hdResult').val();
		var AssessmentCompletedTime = $('#hdAssessmentCompletedTime').val();
		var AssessmentCompletedAt = $('#hdAssessmentCompletionTime').val();
		var ExceptionStatus = $('#hdExceptionStatus').val();

		var d = new Date(AssessmentCompletedAt);
		var date = d.getDate();
		var month = parseInt(d.getMonth()) + 1;
		var year = d.getFullYear();
		var getdate = date + "/" + month + "/" + year;

		var hour = d.getHours();
		var minutes = d.getMinutes();
		var seconds = d.getSeconds();
		var time = hour + ":" + minutes + ":" + seconds;
		var obj = {
			"EmployeeId": EmployeeId,
			"Module":Module,
			"AttemptNumber":AttemptNumber,
			"AssessmentScore":AssessmentScore,
			"Result":Result,
			"AssessmentCompletedTime":AssessmentCompletedAt,
			"AssessmentCompletedAt":AssessmentCompletedAt,
		   "ExceptionStatus":ExceptionStatus
		};

		$.ajax({
			url: "http://localhost:61619/Certificate.asmx/EmployeeResult",
			data: JSON.stringify({ EmployeeId: EmployeeId, Module: Module, AttemptNumber: AttemptNumber, AssessmentScore: AssessmentScore, Result:Result, AssessmentCompletedTime:time,AssessmentCompletedAt:getdate,ExceptionStatus:ExceptionStatus }),
			contentType: "application/json; charset=utf-8",
			method:'post',
			 dataType:"json",
			success: function (result) {
				//alert(result.d);
		  },
			error: function (err) {
				alert(err);
			 }
		});
		  }, 1000);
		}
</script>
</head>
<body class="loaded-content congrats-bg">
<form id="form1" runat="server">
     <div class="d-none" id="availableLanguagesContainerBody"> <span>video</span></div>
  <div class="container-fluid"> 
    <!-- Main Content -->
    <section>
      <asp:HiddenField ID="hdEmployeeId" runat="server"  />
      <asp:Label ID="lblEmployeeId" Text="1" runat="server"/>
      <asp:HiddenField ID="hdModule" runat="server" />
      <asp:HiddenField ID="hdAttemptNumber" runat="server" />
      <asp:HiddenField ID="hdAssessmentScore" runat="server" />
      <asp:HiddenField ID="hdResult" runat="server" />
      <asp:HiddenField ID="hdAssessmentCompletedAt" runat="server" />
      <asp:HiddenField ID="hdAssessmentCompletionTime" runat="server" />
      <asp:HiddenField ID="hdExceptionStatus" runat="server" />
      <div class="infs-body-container">
        <div class="maincontent congratswrap">
          <div class="congratscontent"> <img src="../images/congrats.png" alt="" title="" data-aos="zoom-in" /><br />
            <h3 data-aos="zoom-in">
                <asp:Literal ID="ltrlSuccess" runat="server"></asp:Literal></h3>
            <div class="failbtn">
              <div class="submitbtn hidden">
                     <asp:Button ID="btnStart"   class="btn infs-btn-secondary mb-4" runat="server" Text="CONTINUE" OnClick="btnStart_Click" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- Custom Javascript --> 
  <script type="text/javascript" src="../js/infosys-lib.js"></script> 
  <script type="text/javascript" src="../js/aos.js"></script> 
  <script type="text/javascript" src="../js/infosys.js"></script>
</form>
</body>
</html>
