﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.Master" AutoEventWireup="true" CodeBehind="reports.aspx.cs" Inherits="InfosysiSafe.Admin.reports" ValidateRequest="false"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        table .admin-table{
          width:50%;
           border:1px solid #ccc;

        }
        
       .admin-table td:nth-of-type(odd){
            
			padding:10px;
			font-weight:bold;
        }
		 .admin-table tr{
			border:1px solid #ccc;
			
		}
		
    </style>
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upnl" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportoCSV" />
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>

        <ContentTemplate>
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <!-- iSafe Header -->
                <div class="header">
                    <button type="button" class="hamburger is-closed" data-toggle="offcanvas"><span class="hamb-top"></span><span class="hamb-middle"></span><span class="hamb-bottom"></span></button>
                </div>
                <!-- iSafe Content -->
                <div class="content-wrapper">
                    <div class="main-tabs">
                        <asp:Button ID="btnExportoCSV" Visible="false" runat="server" class="btn btn-primary" Text="Export to CSV"  OnClick="ExportToExcel" />
                          <asp:Button ID="btnExport" runat="server" Text="Export to CSV" class="btn btn-primary" OnClick="btnExportoCSV_Click" />
                        <div style="float:right">
                        <asp:Literal ID="ltrlDetails" runat="server"></asp:Literal>
                            </div>
                    </div>
                    <h3 class="header-tabs">Users List</h3>
                    <!-- DATA Filters -->
                    <div class="iSafe-filters iSafe-form">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="employeeID">Employee Name</label>
                                <div class="form-group">
                                    <asp:TextBox runat="server" placeholder="Enter Employee Name" CssClass="form-control" ID="txtEmployeeName"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fromDate"  style="display:none;">From Date</label>
                                    <asp:TextBox runat="server" placeholder="Select From Date" CssClass="form-control fromDate" ID="txtFromDate"  style="display:none;"></asp:TextBox>
                                    <label for="fromDate">Employee ID</label>
                                    <asp:TextBox runat="server" placeholder="Enter Employee ID"  CssClass="form-control" ID="txtEmployeeid"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4" style="display:none;">
                                <label for="toDate">To Date</label>
                                <asp:TextBox runat="server" placeholder="Select To Date" CssClass="form-control toDate" ID="txtToDate"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Button ID="btnEmployeeId" runat="server" CssClass="btn iSafe-btn-upload" Text="GO" OnClick="btnEmployeeId_Click" />
                                <asp:Button ID="btnReset" runat="server" CssClass="btn iSafe-btn-upload" Text="RESET" OnClick="btnReset_Click" />
                            </div>
                        </div>
                    </div>
                    <asp:Label ID="lblMessage" Style="" runat="server" ForeColor="Red" Text=""></asp:Label>
                    <br />
                    <asp:GridView ID="userListGrid" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover" runat="server" AlternatingRowStyle-BackColor="#f0f0f0" HeaderStyle-BackColor="#337ab7"
                      HeaderStyle-ForeColor="White">
                        <Columns>
                            <asp:BoundField DataField="txtName" HeaderText="Name" />
                            <asp:BoundField DataField="txtEmailId" HeaderText="Email ID" />
                            <asp:BoundField DataField="dtCreatedOn" HeaderText="Created On" />
                                <asp:TemplateField ItemStyle-Width="30px" HeaderText="View">
                                <ItemTemplate> 
                                    <asp:LinkButton ID="btnGenerate" ClientIDMode="AutoID" runat="server"
                                        CommandName='<%#Eval("intId") %>'
                                        OnCommand="btnGenerate_Command" Text='View' >
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:Button ID="btnPrev" runat="server" Text="<<" OnClick="btnPrev_Click" />
                    <asp:Button ID="btnNext" runat="server" Text=">>" OnClick="btnNext_Click" />
                    <span runat="server" id="spnPageNo"></span>
                    
                    
                </div>
            </div>

            <asp:Literal ID="ltrReport" runat="server"></asp:Literal>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div>

      
<div id="Grid" style="display:none">
    <asp:Literal ID="lblReportData" runat="server"></asp:Literal>
</div>
<br />
<asp:HiddenField ID="hfGridHtml" runat="server" />
      
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("[id*=btnExport]").click(function () {
            $("[id*=hfGridHtml]").val($("#Grid").html());
        });
    });
</script>
        </div>
<script>
$(document).ready(function () {
	$("#reports").addClass("active");
});
</script>

</asp:Content>
