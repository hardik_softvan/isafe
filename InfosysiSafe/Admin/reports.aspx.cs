﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.Admin
{
    public partial class reports : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
           

            if (!IsPostBack)
            {
                if (Session["RoleId"] != null) {
                    ViewState["FromPagging"] = 1;
                    ViewState["ToPagging"] =10;
                    BindData();
                    Details();
                 // btnPrev.Visible = false;
                 // BindReports();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        public void Details()
        {
            Dictionary<string, object> Filter = new Dictionary<string, object>();
            Filter.Add("Name", "ByUserAll");
            List<IPersistantObject> AllUser = Global.Store.Find("iSafeTrnCertificate", Filter);

            int TotalUserAcess = UserCnt();
            int TotalUserCompele = AllUser.Count;
            ltrlDetails.Text = "<span><b>Number of employees accessed</b>: "+ TotalUserAcess + "</span> <br> <span><b>Number of employees completed: </b>"+ TotalUserCompele + "</span>";

        }


        public int UserCnt()
        {
            Dictionary<string, object> Filter = new Dictionary<string, object>();
            Filter.Add("Name", "ByUser");
            List<IPersistantObject> AllUser = Global.Store.Find("iSafeMstUser", Filter);
            return AllUser.Count;

        }


        private void BindData()
        {
            Dictionary<string, object> Filter = new Dictionary<string, object>();
            Filter.Add("Name", "ByUserPagging");
            Filter.Add("FromPagging", ViewState["FromPagging"]);
            Filter.Add("ToPagging", ViewState["ToPagging"]);
            List<IPersistantObject> AllUser = Global.Store.Find("iSafeMstUser", Filter);
            List<iSafeMstUser> userall = new List<iSafeMstUser>();

            int TotalUser = UserCnt();
            spnPageNo.InnerText = "[" + ViewState["FromPagging"] + "-" + ViewState["ToPagging"] + "] No. of Page "+ TotalUser;
            if (Convert.ToInt32(ViewState["ToPagging"]) >= TotalUser)
            {
                btnNext.Visible = false;
            }
            else
            {
                btnNext.Visible = true;
            }
            if(Convert.ToInt32(ViewState["FromPagging"]) <= 10)
            {
                btnPrev.Visible = false;
            }
            else
            {
                btnPrev.Visible = true;
            }

            for (int i = 0; i < AllUser.Count; i++)
            {
                iSafeMstUser user = (iSafeMstUser)AllUser[i];
                userall.Add(user);
            }
            var abc = userall.Count;
            userListGrid.DataSource = userall;
            userListGrid.DataBind();
        }

        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            string EmployeeName = txtEmployeeName.Text;
            string EmployeeId = txtEmployeeid.Text;
            if (EmployeeName == "" && EmployeeId == "")
            {
                lblMessage.Text = "";
                BindData();
            }
            else if (EmployeeName != "" && EmployeeId == "")
            {
                lblMessage.Text = "";
                searchByLoginName(EmployeeName);
            }
            else if (EmployeeName == "" && EmployeeId != "")
            {
                lblMessage.Text = "";
                SearchByEmployeeID(EmployeeId);
            }
            else if (EmployeeName != "" && EmployeeId != "")
            {
                lblMessage.Text = "";
                searchByNameAndyEmployeeID(EmployeeName, EmployeeId);
            }
            else
            {
                BindData();
            }

            userListGrid.PageIndex = e.NewPageIndex;
            userListGrid.DataBind();
        }

        protected void btnEmployeeId_Click(object sender, EventArgs e)
        {
            userListGrid.PageIndex = 0;
            string EmployeeName = txtEmployeeName.Text;
            string EmployeeId = txtEmployeeid.Text;

            if (EmployeeName == "" && EmployeeId == "")
            {
                lblMessage.Text = "Please Enter Values to Search";
            }
            else if (EmployeeName == "" && EmployeeId != "")
            {
                lblMessage.Text = "";
                SearchByEmployeeID(EmployeeId);
            }
            else if (EmployeeName != "" && EmployeeId == "")
            {
                lblMessage.Text = "";
                searchByLoginName(EmployeeName);
            }
            else if (EmployeeName != "" && EmployeeId != "")
            {
                lblMessage.Text = "";
                searchByNameAndyEmployeeID(EmployeeName, EmployeeId);
            }
            else
            {
                ViewState["FromPagging"] = 1;
                ViewState["ToPagging"] = 10;
                BindData();
            }
        }

        public void searchByLoginName(string Name)
        {
            ViewState["FromPagging"] = 1;
            ViewState["ToPagging"] = 10;
            Dictionary<string, object> Filter = new Dictionary<string, object>();
            Filter.Add("Name", "BySearchUser");
            Filter.Add("EmployeeName", Name);
            Filter.Add("FromPagging", ViewState["FromPagging"]);
            Filter.Add("ToPagging", ViewState["ToPagging"]);
            List<IPersistantObject> AllUser = Global.Store.Find("iSafeMstUser", Filter);
            List<iSafeMstUser> userall = new List<iSafeMstUser>();


            int TotalUser = AllUser.Count;
            spnPageNo.InnerText = "[" + ViewState["FromPagging"] + "-" + ViewState["ToPagging"] + "] Total " + TotalUser;
            if (Convert.ToInt32(ViewState["ToPagging"]) >= TotalUser)
            {
                btnNext.Visible = false;
            }
            else
            {
                btnNext.Visible = true;
            }
            if (Convert.ToInt32(ViewState["FromPagging"]) <= 10)
            {
                btnPrev.Visible = false;
            }
            else
            {
                btnPrev.Visible = true;
            }

            for (int i = 0; i < AllUser.Count; i++)
            {
                iSafeMstUser user = (iSafeMstUser)AllUser[i];
                userall.Add(user);
            }
            var abc = userall.Count;
            userListGrid.DataSource = userall;
            if (AllUser.Count != 0)
            {
                userListGrid.DataBind();
            }
            else
            {
                userListGrid.DataBind();
                lblMessage.Text = "No Search Result";
                NoResultFound();
            }
        }


        public void NoResultFound()
        {
            btnPrev.Visible = false;
            btnNext.Visible = false;
            spnPageNo.Visible = false;
        }


        public void SearchByEmployeeID(string EmployeeId)
        {
            ViewState["FromPagging"] = 1;
            ViewState["ToPagging"] = 10;
            Dictionary<string, object> Filter = new Dictionary<string, object>();
            Filter.Add("Name", "BySearchEmployeeId");
            Filter.Add("EmployeeId", EmployeeId);
            Filter.Add("FromPagging", ViewState["FromPagging"]);
            Filter.Add("ToPagging", ViewState["ToPagging"]);
            List<IPersistantObject> AllUser = Global.Store.Find("iSafeMstUser", Filter);
            List<iSafeMstUser> userall = new List<iSafeMstUser>();

            int TotalUser = AllUser.Count;
            spnPageNo.InnerText = "[" + ViewState["FromPagging"] + "-" + ViewState["ToPagging"] + "] Total " + TotalUser;
            if (Convert.ToInt32(ViewState["ToPagging"]) >= TotalUser)
            {
                btnNext.Visible = false;
            }
            else
            {
                btnNext.Visible = true;
            }
            if (Convert.ToInt32(ViewState["FromPagging"]) <= 10)
            {
                btnPrev.Visible = false;
            }
            else
            {
                btnPrev.Visible = true;
            }

            for (int i = 0; i < AllUser.Count; i++)
            {
                iSafeMstUser user = (iSafeMstUser)AllUser[i];
                userall.Add(user);
            }
            var abc = userall.Count;
            userListGrid.DataSource = userall;
            if (AllUser.Count != 0)
            {
                userListGrid.DataBind();
            }
            else
            {
                userListGrid.DataBind();
                lblMessage.Text = "No Search Result";
                NoResultFound();
            }
        }

        public void searchByNameAndyEmployeeID(string Name, string EmployeeId)
        {
            ViewState["FromPagging"] = 1;
            ViewState["ToPagging"] = 10;
            Dictionary<string, object> Filter = new Dictionary<string, object>();
            Filter.Add("Name", "BySearchNameAndEmployeeID");
            Filter.Add("EmployeeName", Name);
            Filter.Add("EmployeeId", EmployeeId);
            Filter.Add("FromPagging", ViewState["FromPagging"]);
            Filter.Add("ToPagging", ViewState["ToPagging"]);
            // FirstTopic.Add("Topicid", userPageSequence.intId);
            List<IPersistantObject> AllUser = Global.Store.Find("iSafeMstUser", Filter);
            List<iSafeMstUser> userall = new List<iSafeMstUser>();

            int TotalUser = AllUser.Count;
            spnPageNo.InnerText = "[" + ViewState["FromPagging"] + "-" + ViewState["ToPagging"] + "] Total " + TotalUser;
            if (Convert.ToInt32(ViewState["ToPagging"]) >= TotalUser)
            {
                btnNext.Visible = false;
            }
            else
            {
                btnNext.Visible = true;
            }
            if (Convert.ToInt32(ViewState["FromPagging"]) <= 10)
            {
                btnPrev.Visible = false;
            }
            else
            {
                btnPrev.Visible = true;
            }


            for (int i = 0; i < AllUser.Count; i++)
            {
                iSafeMstUser user = (iSafeMstUser)AllUser[i];
                userall.Add(user);
            }
            var abc = userall.Count;
            userListGrid.DataSource = userall;
            if (AllUser.Count != 0)
            {
                userListGrid.DataBind();
            }
            else
            {
                userListGrid.DataBind();
                lblMessage.Text = "No Search Result";
                NoResultFound();
            }
        }
        //*******************view report by id ***************************//
        protected void btnGenerate_Command(object sender, CommandEventArgs e)
        {
            int id = Convert.ToInt32(e.CommandName);
            Dictionary<string, object> Filter = new Dictionary<string, object>();
            Filter.Add("Name", "ByUserid");
            Filter.Add("intId", id);
            List<IPersistantObject> AllUser = Global.Store.Find("iSafeMstUser", Filter);
            List<iSafeMstUser> userall = new List<iSafeMstUser>();
            iSafeMstUser user = (iSafeMstUser)AllUser[0];
            StringBuilder sb = new StringBuilder();
            DataTable Department = bindDepartment(user.intId.ToString());
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter.Add("Name", "ByAll");
            List<IPersistantObject> MenuList = Global.Store.Find("iSafeMstModule", filter);
            List<iSafeMstModule> Model = new List<iSafeMstModule>();
            Dictionary<string, object> filter1 = new Dictionary<string, object>();
            filter1.Add("Name", "ByPledge");
            filter1.Add("UserId", user.intId);
            List<IPersistantObject> Pledge = Global.Store.Find("iSafeTrnPledge", filter1);
            sb.Append("<table class='table-bordered admin-table'>");
            for (int cnt = 0; cnt < MenuList.Count; cnt++)
            {
                iSafeMstModule MenuLists = (iSafeMstModule)MenuList[cnt];
                DataTable StartOn = bindModuleStartOn(user.intId.ToString(), MenuLists.intId.ToString());
                DataTable EndOn = bindModuleEndOn(user.intId.ToString(), MenuLists.intId.ToString());
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString()))
                {
                    using (SqlCommand cmd = new SqlCommand("iSafeMstUser_Reports", con))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@userid", user.intId);
                            cmd.Parameters.AddWithValue("@moduleid", MenuLists.intId);
                            DataTable dt = new DataTable();
                            sda.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                if (MenuLists.intId == 1)
                                {
                                    sb.Append("<tr>");
                                    sb.Append("<td>EmployeeName :</td><td>" + dt.Rows[0]["EmployeeName"]);
                                    sb.Append("</td> <td>First Access :</td><td>" + dt.Rows[0]["FirstAccess"] + "</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    sb.Append("<td>Employee ID :</td><td>" + dt.Rows[0]["EmployeeId"] + "</td>");
                                    sb.Append("<td>Latest Access :</td><td>" + dt.Rows[0]["LatestAccess"] + "</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    sb.Append("<td>Department :</td><td>" + dt.Rows[0]["Department"] + "</td>");
                                    if (Pledge.Count > 0)
                                    {
                                        sb.Append("<td>Course Status :</td><td>Complete</td>");
                                    }
                                    else
                                    {
                                        sb.Append("<td>Course Status :</td><td>Pending</td>");
                                    }
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                }
                                if (MenuLists.intId <= 3)
                                {
                                    sb.Append("<td>" + MenuLists.txtName + " </td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    if (StartOn.Rows.Count > 0)
                                    {
                                        sb.Append("<td>Started On :</td><td>" + StartOn.Rows[0]["ModuleStartOn"] + "</td>");
                                    }
                                    else
                                    {
                                        sb.Append("<td>Started On :</td><td>--</td>");
                                    }
                                    sb.Append("<td>No Of Attempts :</td><td>" + dt.Rows[0]["NoOfAttempts"] + "</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    if (EndOn.Rows.Count > 0)
                                    {
                                        sb.Append("<td>Ended On :</td><td>" + EndOn.Rows[0]["ModuleEndOn"] + "</td>");
                                    }
                                    else
                                    {
                                        DataTable EndLastOn = ModuleEndLastOn(user.intId.ToString());
                                        if (EndLastOn.Rows.Count > 0)
                                        {
                                            if (MenuLists.intId == Convert.ToUInt32(EndLastOn.Rows[0]["Moduleid"]))
                                            {
                                                sb.Append("<td>Ended On :</td><td>" + EndLastOn.Rows[0]["ModuleEndOn"] + "</td>");
                                            }
                                            else
                                            {
                                                sb.Append("<td>Ended On :</td><td>--</td>");
                                            }
                                        }
                                        else
                                        {
                                            sb.Append("<td>Ended On :</td><td>--</td>");
                                        }
                                    }
                                    sb.Append("<td>Latest Assessment Score :</td><td>" + dt.Rows[0]["LatestAssessmentScore"] + "</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    if (Convert.ToInt32(dt.Rows[0]["Result"]) > 69)
                                    {
                                        sb.Append("<td>Status :</td><td> Passed</td>");
                                    }
                                    else
                                    {

                                        sb.Append("<td>Status :</td><td>Not Passed</td>");

                                    }
                                    sb.Append("<td>Assessment Passing Score :</td><td>" + dt.Rows[0]["AssessmentPassingScore"] + "</td>");
                                    sb.Append("</tr>");
                                }

                                else
                                {
                                    // 4th module assessment
                                    sb.Append("<td>" + MenuLists.txtName + " </td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    if (StartOn.Rows.Count > 0)
                                    {

                                        DataTable EndLastOn = ModuleEndLastOn(user.intId.ToString());
                                        if (MenuLists.intId == Convert.ToUInt32(EndLastOn.Rows[0]["Moduleid"]))
                                        {
                                            sb.Append("<td>Started On :</td><td>" + StartOn.Rows[0]["ModuleStartOn"] + "</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td>Started On :</td><td>--</td>");
                                        }

                                    }
                                    else
                                    {
                                        sb.Append("<td>Started On :</td><td>NA</td>");
                                    }
                                    sb.Append("<td>No Of Attempts :</td><td>NA</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    if (EndOn.Rows.Count > 0)
                                    {

                                        DataTable EndLastOn = ModuleEndLastOn(user.intId.ToString());
                                        if (EndLastOn.Rows.Count > 0)
                                        {
                                            if (MenuLists.intId == Convert.ToUInt32(EndLastOn.Rows[0]["Moduleid"]))
                                            {
                                                sb.Append("<td>Ended On :</td><td>" + EndLastOn.Rows[0]["ModuleEndOn"] + "</td>");
                                            }
                                            else
                                            {
                                                sb.Append("<td>Ended On :</td><td>--</td>");
                                            }
                                        }
                                        else
                                        {
                                            sb.Append("<td>Ended On :</td><td>--</td>");
                                        }
                                    }
                                    else
                                    {
                                        sb.Append("<td>Ended On :</td><td>NA</td>");
                                    }
                                    sb.Append("<td>Latest Assessment Score :</td><td>NA</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    if (Convert.ToInt32(dt.Rows[0]["Result"]) > 69)
                                    {
                                        sb.Append("<td>Status :</td><td> Passed</td>");
                                    }
                                    else
                                    {
                                        sb.Append("<td>Status :</td><td>Not Passed</td>");
                                    }
                                    sb.Append("<td>Assessment Passing Score :</td><td>" + dt.Rows[0]["AssessmentPassingScore"] + "</td>");
                                    sb.Append("</tr>");

                                }
                                if (MenuList.Count == MenuLists.intSequenceNo)
                                {
                                    if (Pledge.Count > 0)
                                    {
                                        sb.Append("<tr><td>Pledge :</td><td>Takken</td> </tr>");
                                        sb.Append("<tr><td>Certificate :</td><td>Downloaded</td> </tr>");
                                    }
                                    else
                                    {
                                        sb.Append("<tr><td>Pledge :</td><td>Not Takken</td> </tr>");
                                        sb.Append("<tr><td>Certificate :</td><td>Not Downloaded</td> </tr>");
                                    }
                                }
                            }

                            else
                            {
                                if (MenuLists.intId == 1)
                                {
                                    sb.Append("<tr>");
                                    sb.Append("<td>EmployeeName :</td><td>" + user.txtName + "</td> ");
                                    sb.Append("<td>First Access :</td><td>" + user.dtCreatedOn + "</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    sb.Append("<td>Employee ID :</td><td>" + user.intId + "</td>");
                                    DataTable EndLastOn = ModuleEndLastOn(user.intId.ToString());
                                    sb.Append("<td>Latest Access :</td><td>" + EndLastOn.Rows[0]["ModuleEndOn"] + "</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    sb.Append("<td>Department :</td><td>" + Department.Rows[0]["Department"] + "</td>");
                                    if (Pledge.Count > 0)
                                    {
                                        sb.Append("<td>Course Status :</td><td>Complete</td>");
                                    }
                                    else
                                    {
                                        sb.Append("<td>Course Status :</td><td>Pending</td>");
                                    }
                                    sb.Append("</tr>");
                                }
                                if (MenuLists.intId <= 3)
                                {
                                    sb.Append("<td>" + MenuLists.txtName + " </td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    sb.Append("<td>Started On :</td><td>" + "--" + "</td>");
                                    sb.Append("<td>No Of Attempts :</td><td>--</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    sb.Append("<td>Ended On :</td><td>" + "--" + "</td>");
                                    sb.Append("<td>Latest Assessment Score :</td><td>--</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    sb.Append("<td>Status :</td><td>Not Passed</td>");
                                    sb.Append("<td>Assessment Passing Score :</td><td>--</td>");
                                    sb.Append("</tr>");
                                }
                                else
                                {
                                    sb.Append("<td>" + MenuLists.txtName + " </td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    sb.Append("<td>Started On :</td><td>NA</td>");
                                    sb.Append("<td>No Of Attempts :</td><td>NA</td>");
                                    sb.Append("</tr>");
                                    sb.Append("<tr>");
                                    sb.Append("<td>Ended On :</td><td>NA</td>");
                                    sb.Append("<td>Latest Assessment Score :</td><td>NA</td>");
                                    sb.Append("</tr>");

                                    sb.Append("<tr>");
                                    sb.Append("<td>Status :</td><td>NA</td>");
                                    sb.Append("<td>Assessment Passing Score :</td><td>NA</td>");
                                    sb.Append("</tr>");

                                }
                                if (MenuList.Count == MenuLists.intSequenceNo)
                                {
                                    if (Pledge.Count > 0)
                                    {
                                        sb.Append("<tr><td>Pledge :</td><td>Takken</td> </tr>");
                                        sb.Append("<tr><td>Certificate :</td><td>Downloaded</td> </tr>");
                                    }
                                    else
                                    {
                                        sb.Append("<tr><td>Pledge :</td><td>Not Takken</td> </tr>");
                                        sb.Append("<tr><td>Certificate :</td><td>Not Downloaded</td> </tr>");
                                    }
                                }
                            }

                        }
                    }
                }

            }
            sb.Append("</table>");
            ltrReport.Text = sb.ToString();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("reports.aspx");
        }

        protected void btnExportoCSV_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand("sp_ExportToExcelUserDetails", con))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                       cmd.CommandType = CommandType.StoredProcedure;
            //        //    cmd.Parameters.AddWithValue("@userid", userid);
                       DataTable dt = new DataTable();
                            sda.Fill(dt);
                        string attachment = "attachment; filename=EmployeeDetails.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/vnd.ms-excel";
                        string tab = "";
                        foreach (DataColumn dc in dt.Columns)
                        {
                            Response.Write(tab + dc.ColumnName);
                            tab = "\t";
                        }
                        Response.Write("\n");
                        int i;
                        foreach (DataRow dr in dt.Rows)
                        {
                            tab = "";
                            for (i = 0; i < dt.Columns.Count; i++)
                            {
                                Response.Write(tab + dr[i].ToString());
                                tab = "\t";
                            }
                            Response.Write("\n");
                        }
                        Response.End();
     

                    }
                }  
            }


            //string EmployeeName = txtEmployeeName.Text;
            //string EmployeeId = txtEmployeeid.Text;
            //if (userListGrid.Rows.Count > 0)
            //{
            //    using (StringWriter sw = new StringWriter())
            //    {
            //        HtmlTextWriter hw = new HtmlTextWriter(sw);
            //        userListGrid.AllowPaging = false;
            //        if (EmployeeName == "" && EmployeeId == "")
            //        {
            //            lblMessage.Text = "";
            //            BindData();
            //        }
            //        else if (EmployeeName != "" && EmployeeId == "")
            //        {
            //            lblMessage.Text = "";
            //            searchByLoginName(EmployeeName);
            //        }
            //        else if (EmployeeName == "" && EmployeeId != "")
            //        {
            //            lblMessage.Text = "";
            //            SearchByEmployeeID(EmployeeId);
            //        }
            //        else if (EmployeeName != "" && EmployeeId != "")
            //        {
            //            lblMessage.Text = "";
            //            searchByNameAndyEmployeeID(EmployeeName, EmployeeId);
            //        }

            //        StringBuilder sb = new StringBuilder();
            //        foreach (TableCell cell in userListGrid.HeaderRow.Cells)
            //        {
            //            sb.Append(cell.Text + ",");
            //        }
            //        sb.Append("\r\n");
            //        foreach (GridViewRow row in userListGrid.Rows)
            //        {
            //            foreach (TableCell cell in row.Cells)
            //            {
            //                Label lbl = (Label)cell.FindControl("lblHasPassed");
            //                LinkButton lblink = (LinkButton)cell.FindControl("btnGenerate");
            //                if (cell.Controls.Contains(lbl))
            //                {
            //                    sb.Append(lbl.Text.ToString().Replace(",", ";").Replace("&nbsp;", "") + ',');
            //                }
            //                else if (cell.Controls.Contains(lblink))
            //                {
            //                    sb.Append(lblink.Text.ToString().Replace(",", ";").Replace("&nbsp;", "") + ',');
            //                }
            //                else
            //                {
            //                    sb.Append(cell.Text.ToString().Replace(",", ";").Replace("&nbsp;", "") + ',');
            //                }
            //            }
            //            sb.Append("\r\n");
            //        }
            //        Response.Clear();
            //        Response.Buffer = true;
            //        Response.AddHeader("content-disposition", "attachment;filename=CSV_Report.csv");
            //        Response.Charset = "";
            //        Response.ContentType = "application/text";
            //        Response.Output.Write(sb.ToString());
            //        Response.Flush();
            //        Response.End();
            //    }
            //}

        }
        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=HTML.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            Response.Output.Write(Request.Form[hfGridHtml.UniqueID]);
            Response.Flush();
            Response.End();
        }

        private void BindReports()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table cellspacing='0' cellpadding='2' style='border-collapse: collapse; border: 1px solid #ccc;font-size: 9pt;'>");
            sb.Append("<tr>");
            sb.Append("<th class='th'>Employee ID</th>");
            sb.Append(" <th class='th'>Employee Name</th>");
            sb.Append(" <th class='th'>Employee Email ID</th>");
            sb.Append(" <th class='th'>Company Name</th>");
            sb.Append("<th class='th'>First Access</th>");
            sb.Append(" <th class='th'>Last Access</th>");
            sb.Append("<th class='th'>Course Status</th>");
            sb.Append(" <th class='th'>Module 1 Status</th>");
            sb.Append("<th class='th'>Module 1 Start On</th>");
            sb.Append("<th class='th'>Module 1 Complete On</th>");
            sb.Append("<th class='th'>No of Assessmet Test Attempts in Module 1 </th>");
            sb.Append("<th class='th'>Module 1 Passing Score</th>");
            sb.Append("<th class='th'>Module 2 Status</th>");
            sb.Append("<th class='th'>Module 2 Start On</th>");
            sb.Append("<th class='th'>Module 2 Complete On</th>");
            sb.Append("<th class='th'>No of Assessmet Test Attempts in Module 2 </th>");
            sb.Append("<th class='th'>Module 2 Passing Score</th>");
            sb.Append("<th class='th'>Module 3 Status</th>");
            sb.Append("<th class='th'>Module 3 Start On</th>");
            sb.Append(" <th class='th'>Module 3 Complete On</th>");
            sb.Append(" <th class='th'>No of Assessmet Test Attempts in Module 3 </th>");
            sb.Append("<th class='th'>Module 3 Passing Score</th>");
            sb.Append("  <th class='th'>Module 4 Status</th>");
            sb.Append("<th class='th'>Module 4 Start On</th>");
            sb.Append("<th class='th'>Module 4 Complete On</th>");
            sb.Append(" <th class='th'>Module 4 Passing Score</th>");
            sb.Append("<th class='th'>Pledge</th>");
            sb.Append(" <th class='th'>Certificate Download Status</th>");
            sb.Append("<th class='th'>Certificate Download Date & Time</th>");
            sb.Append("</tr>");
            Dictionary<string, object> Filter = new Dictionary<string, object>();
            Filter.Add("Name", "ByUser");
            List<IPersistantObject> AllUser = Global.Store.Find("iSafeMstUser", Filter);
            List<iSafeMstUser> userall = new List<iSafeMstUser>();
            for (int i = 0; i < AllUser.Count; i++)
            {
                iSafeMstUser user = (iSafeMstUser)AllUser[i];
                DataTable Department = bindDepartment(user.intId.ToString());
                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter.Add("Name", "ByAll");
                List<IPersistantObject> MenuList = Global.Store.Find("iSafeMstModule", filter);
                List<iSafeMstModule> Model = new List<iSafeMstModule>();

                Dictionary<string, object> filter1 = new Dictionary<string, object>();
                filter1.Add("Name", "ByPledge");
                filter1.Add("UserId", user.intId);
                List<IPersistantObject> Pledge = Global.Store.Find("iSafeTrnPledge", filter1);
                sb.Append("<tr>");

                for (int cnt = 0; cnt < MenuList.Count; cnt++)
                {
                    iSafeMstModule MenuLists = (iSafeMstModule)MenuList[cnt];
                    DataTable StartOn = bindModuleStartOn(user.intId.ToString(), MenuLists.intId.ToString());
                    DataTable EndOn = bindModuleEndOn(user.intId.ToString(), MenuLists.intId.ToString());
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString()))
                    {
                        using (SqlCommand cmd = new SqlCommand("iSafeMstUser_Reports", con))
                        {
                            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@userid", user.intId);
                                cmd.Parameters.AddWithValue("@moduleid", MenuLists.intId);
                                DataTable dt = new DataTable();
                                sda.Fill(dt);

                                if (dt.Rows.Count > 0)
                                {
                                    // RptReport.DataSource = dt;
                                    //RptReport.DataBind();
                                    if (MenuLists.intId == 1)
                                    {

                                        sb.Append("<td class='td'>" + dt.Rows[0]["EmployeeId"] + "</td>");
                                        sb.Append("<td class='td'>" + dt.Rows[0]["EmployeeName"] + "</td>");
                                        sb.Append("<td class='td'>" + dt.Rows[0]["EmailId"] + "</td>");
                                        sb.Append("<td class='td'>" + dt.Rows[0]["Department"] + "</td>");
                                        sb.Append("<td class='td'>" + dt.Rows[0]["FirstAccess"] + "</td>");
                                        sb.Append("<td class='td'>" + dt.Rows[0]["LatestAccess"] + "</td>");
                                        if (Pledge.Count > 0)
                                        {
                                            sb.Append("<td class='td'>Complete</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td class='td'>Pending</td>");
                                        }
                                    }

                                    if (MenuLists.intId <= 3)
                                    {
                                        if (Convert.ToInt32(dt.Rows[0]["Result"]) > 69)
                                        {
                                            sb.Append("<td class='td'> Passed</td>");
                                        }
                                        else
                                        {

                                            sb.Append("<td class='td'>Not Passed</td>");

                                        }
                                        if (StartOn.Rows.Count > 0)
                                        {
                                            sb.Append("<td class='td'>" + StartOn.Rows[0]["ModuleStartOn"] + "</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td class='td'>--</td>");
                                        }

                                        if (EndOn.Rows.Count > 0)
                                        {
                                            sb.Append("<td class='td'>" + EndOn.Rows[0]["ModuleEndOn"] + "</td>");
                                        }
                                        else
                                        {
                                            // sb.Append("<td>Ended On :</td><td>" + "--" + "</td>");
                                            DataTable EndLastOn = ModuleEndLastOn(user.intId.ToString());
                                            if (EndLastOn.Rows.Count > 0)
                                            {
                                                if (MenuLists.intId == Convert.ToUInt32(EndLastOn.Rows[0]["Moduleid"]))
                                                {
                                                    sb.Append("<td class='td'>" + EndLastOn.Rows[0]["ModuleEndOn"] + "</td>");
                                                }
                                                else
                                                {
                                                    sb.Append("<td class='td'>--</td>");
                                                }
                                            }
                                            else
                                            {
                                                sb.Append("<td class='td'>Ended On :</td><td>--</td>");
                                            }
                                        }
                                        sb.Append("<td class='td'>" + dt.Rows[0]["NoOfAttempts"] + "</td>");
                                        sb.Append("<td class='td'>" + dt.Rows[0]["AssessmentPassingScore"] + "</td>");
                                    }
                                    else
                                    {
                                        // 4th module assessment
                                        if (Convert.ToInt32(dt.Rows[0]["Result"]) > 69)
                                        {
                                            sb.Append("<td class='td'> Passed</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td class='td'>Not Passed</td>");
                                        }
                                        if (StartOn.Rows.Count > 0)
                                        {
                                            DataTable EndLastOn = ModuleEndLastOn(user.intId.ToString());
                                            if (MenuLists.intId == Convert.ToUInt32(EndLastOn.Rows[0]["Moduleid"]))
                                            {
                                                sb.Append("<td class='td'>" + StartOn.Rows[0]["ModuleStartOn"] + "</td>");
                                            }
                                            else
                                            {
                                                sb.Append("<td class='td'>--</td>");
                                            }
                                        }
                                        else
                                        {
                                            sb.Append("<td class='td'>NA</td>");
                                        }
                                        if (EndOn.Rows.Count > 0)
                                        {
                                            DataTable EndLastOn = ModuleEndLastOn(user.intId.ToString());
                                            if (EndLastOn.Rows.Count > 0)
                                            {
                                                if (MenuLists.intId == Convert.ToUInt32(EndLastOn.Rows[0]["Moduleid"]))
                                                {
                                                    sb.Append("<td class='td'>" + EndLastOn.Rows[0]["ModuleEndOn"] + "</td>");
                                                }
                                                else
                                                {
                                                    sb.Append("<td class='td'>--</td>");
                                                }
                                            }
                                            else
                                            {
                                                sb.Append("<td class='td'>--</td>");
                                            }
                                        }
                                        else
                                        {
                                            sb.Append("<td class='td'>NA</td>");
                                        }
                                        sb.Append("<td>NA</td>");
                                        sb.Append("<td class='td'>" + dt.Rows[0]["AssessmentPassingScore"] + "</td>");
                                    }
                                    if (MenuList.Count == MenuLists.intSequenceNo)
                                    {
                                        if (Pledge.Count > 0)
                                        {
                                            sb.Append("<td class='td'>Takken</td>");
                                            sb.Append("<td class='td'>Downloaded</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td class='td'>Not Takken</td>");
                                            sb.Append("<td class='td'>Not Downloaded</td>");
                                        }
                                    }
                                }
                                else
                                {
                                    if (MenuLists.intId == 1)
                                    {
                                        sb.Append("<td class='td'>" + user.txtEmployeeId + "</td>");
                                        sb.Append("<td class='td'>" + user.txtName + "</td>");
                                        sb.Append("<td class='td'>" + user.txtEmailId + "</td>");
                                        sb.Append("<td class='td'>" + Department.Rows[0]["Department"] + "</td>");
                                        sb.Append("<td class='td'>" + user.dtCreatedOn + "</td>");
                                        DataTable EndLastOn = ModuleEndLastOn(user.intId.ToString());
                                       // sb.Append("<td class='td'>" + EndLastOn.Rows[0]["ModuleEndOn"] + "</td>");
                                        if (Pledge.Count > 0)
                                        {
                                            sb.Append("<td class='td'>Complete</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td class='td'>Pending</td>");
                                        }
                                    }
                                    if (MenuLists.intId <= 3)
                                    {
                                        sb.Append("<td class='td'>Not Passed</td>");
                                        sb.Append("<td class='td'>" + "--" + "</td>");
                                        sb.Append("<td class='td'>" + "--" + "</td>");
                                        sb.Append("<td class='td'>--</td>");
                                        sb.Append("<td class='td'>--</td>");
                                    }
                                    else
                                    {
                                        sb.Append("<td class='td'>NA</td>");
                                        sb.Append("<td class='td'>NA</td>");
                                        sb.Append("<td class='td'>NA</td>");
                                        sb.Append("<td class='td'>NA</td>");
                                        sb.Append("<td class='td'>NA</td>");

                                    }
                                    if (MenuList.Count == MenuLists.intSequenceNo)
                                    {
                                        if (Pledge.Count > 0)
                                        {
                                            sb.Append("<td class='td'>Takken</td>");
                                            sb.Append("<td class='td'>Downloaded</td>");
                                        }
                                        else
                                        {
                                            sb.Append("<td class='td'>Not Takken</td>");
                                            sb.Append("<td class='td'>Not Downloaded</td>");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            lblReportData.Text = sb.ToString();
        }

        public DataTable bindDepartment(string userid)
        {
            
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand("iSafeMstUser_Department", con))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@userid", userid);
                        DataTable dt = new DataTable();
                        try
                        {
                            sda.Fill(dt);
                        }
                        catch (Exception e)
                        {

                        }
                        return dt;
                    }
                }  
            }          
        }

        public DataTable bindModuleStartOn(string userid, string Moduleid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand("iSafeTrnSessionPage_FindByModuleStartOn", con))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@userid", Convert.ToInt32(userid));
                        cmd.Parameters.AddWithValue("@Moduleid", Convert.ToInt32(Moduleid));
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        return dt;
                    }
                }
            }
        }

        public DataTable bindModuleEndOn(string userid, string Moduleid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand("iSafeTrnSessionPage_FindByModuleEndOn", con))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@userid", Convert.ToInt32(userid));
                        cmd.Parameters.AddWithValue("@Moduleid", Convert.ToInt32(Moduleid));
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        return dt;
                    }
                }
            }
        }

        public DataTable ModuleEndLastOn(string userid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand("iSafeTrnSessionPage_FindByModuleEndLastOn", con))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@userid", Convert.ToInt32(userid));
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        return dt;
                    }
                }
            }
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            ViewState["FromPagging"] = Convert.ToInt32(ViewState["FromPagging"]) - 10;
            ViewState["ToPagging"] = Convert.ToInt32(ViewState["ToPagging"]) - 10;
            BindData();
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            ViewState["FromPagging"] = Convert.ToInt32(ViewState["FromPagging"]) + 10;
            ViewState["ToPagging"] = Convert.ToInt32(ViewState["ToPagging"]) + 10;
            BindData();
        }
    }
}