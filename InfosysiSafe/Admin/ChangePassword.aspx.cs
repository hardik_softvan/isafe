﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfosysiSafe.Domain;

namespace InfosysiSafe.Admin
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["AdminId"] != null)
                {
                    
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
           if(txtOldPass.Text== Session["Password"].ToString())
            {
                iSafeMstAdminUser iSafeMstAdminUser = new iSafeMstAdminUser();
                iSafeMstAdminUser.intId = Convert.ToInt32(Session["AdminId"]);
                iSafeMstAdminUser.txtLoginName = "";
                iSafeMstAdminUser.txtUserName = "";
                iSafeMstAdminUser.txtPassword = txtNewPassword.Text;
                iSafeMstAdminUser.txtRoleid =  "0";
                Global.Store.Update(iSafeMstAdminUser);
                lblMessage.Visible = true;
                txtOldPass.Text = "";
                lblMessage.Text = "password successfully changed";
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Old Password Does Not Match";
            }
        }
    }
}