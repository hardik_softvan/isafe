﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="InfosysiSafe.Admin.ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
             <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upnl" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
        <ContentTemplate>
            <!-- Page Content -->
            <div id="page-content-wrapper">
   <!-- InfosysiSafe Header -->
   <div class="header">
      <button type="button" class="hamburger is-closed" data-toggle="offcanvas"><span class="hamb-top"></span><span class="hamb-middle"></span><span class="hamb-bottom"></span></button>
   </div>
   <!-- InfosysiSafe Content -->
   <div class="content-wrapper">
      <h3 class="header-tabs">Change Password</h3>
      <!-- InfosysiSafe View Report -->
      <br />
      <br />
      <asp:Label ID="lblMessage" Style="" Visible="false" runat="server" ForeColor="Red" Text=""></asp:Label>
      <div id="AddAdmin" runat="server">
         <table>
            <tr>
               <td>Old Password</td>
               <td>
                  <asp:TextBox ID="txtOldPass" runat="server" required></asp:TextBox>
               </td>
            </tr>
            <tr>
               <td>New Password :</td>
               <td>
                  <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server" required></asp:TextBox>
               </td>
            </tr>
            <tr>
               <td>Re Enter Password :</td>
               <td>
                  <asp:TextBox ID="txtReEnterPass" TextMode="Password" runat="server" required ></asp:TextBox>
               </td>
               <asp:CompareValidator ID="CompareValidator1" runat="server" controltovalidate="txtReEnterPass" controltocompare="txtNewPassword"
                  ErrorMessage="new password does not match" ForeColor="Red" ValidationGroup="a"></asp:CompareValidator>
            </tr>
            <tr>
               <td></td>
               <td>
                  <asp:Button ID="btnSave" ValidationGroup="a" runat="server" Text="Save" OnClick="btnSave_Click" />
               </td>
            </tr>
         </table>
      </div>
   </div>
</div>
        </ContentTemplate>
    </asp:UpdatePanel>
<script>
$(document).ready(function () {
	$("#reports").addClass("active");
});
</script>
</asp:Content>
