﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="InfosysiSafe.Admin.Login" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<title>Infosys : iSAFE</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Infosys">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Custom CSS -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/infosys.css">
<!-- Favicon -->
<link rel="shortcut icon" href="../images/favicon.png">
<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">
</head>
<body class="infs-login">
<!-- Login Header -->

<!-- Login Section -->

 <form runat="server">

<%--	<div class="loginwrap">
		<div class="loginform">
			<div class="logincontent center">
				<p class="white welcometxt">Welcome to <i><img src="images/info-logo.png" alt="" title="" /></i></p>
				<img src="images/isafe.png" alt="" title="" />
			</div>
			<div class="loginformwrap">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                     <asp:TextBox ID="txtLoginName" runat="server" CssClass="form-control" placeholder="Username" ></asp:TextBox>
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="form-control" placeholder="Password"></asp:TextBox>                
				</div>
				<div class="submitbtn">
                    <asp:Button ID="btnLogin" CssClass="buttoncss" Width="250px" BackColor="#007dc3"  runat="server" Text="Log In"  OnClick="btnLogin_Click" />
                    <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
				</div>
                 <asp:Label ID="lblMessage" Style="" runat="server" Text="" ForeColor="Red"></asp:Label>
			</div>
		</div>
		<div class="clear"></div>
	</div>--%>

          <div class="infs-logo"><img src="../images/info-logo.png" alt=""></div>
  <div class="login-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6 offset-md-6">
          <div class="loginform">   
            <div class="logincontent">
              <h2>Welcome to</h2>
              <img src="../images/isafe.png" class="isafe-logo" alt="" title="infosys isafe logo" /> </div>
             <div class="loginformwrap">
              <div class="input-group mb-3">
                <div class="input-group-prepend"> <span class="input-group-text" id="basic-addon1"><i class="fa fa-user fa" aria-hidden="true"></i></span> </div>
                <asp:TextBox ID="txtLoginName" runat="server" CssClass="form-control form-control" placeholder="Username" ></asp:TextBox>
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend"> <span class="input-group-text" id="basic-addon1"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span> </div>
                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="form-control" placeholder="Password"></asp:TextBox>
              </div>
              <asp:Label ID="lblMessage" CssClass="login-error" runat="server" Text="" ForeColor="Red"></asp:Label>
              <div class="submitbtn">
                <asp:Button ID="btnLogin" class="login-btn" CssClass="btn-block infs-btn-primary" runat="server" Text="LOGIN TO YOUR ACCOUNT" OnClick="btnLogin_Click" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

 </form>
<!-- Custom JavaScript --> 
<script type="text/javascript" src="js/aos.js"></script>
<script>
    AOS.init({
    	easing: 'ease-in-out-sine',
    	duration: 1000,
    	delay: 6,
    	disable: function () {
		    var maxWidth = 1024;
		    return window.innerWidth < maxWidth;
		}
    });
</script>
</html>
