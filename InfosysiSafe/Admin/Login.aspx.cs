﻿using InfosysiSafe.API;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfosysiSafe.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string loginName = txtLoginName.Text;
            string password = txtPassword.Text;
            
#if DEBUG   

#endif
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            AdminUserLogin loginAPI = new AdminUserLogin(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("loginName", loginName);
            input.Add("password", password);
            Dictionary<string, object> output = loginAPI.Process(input);

            if (output.ContainsKey("error"))
            {
                lblMessage.Text = "Incorrect user id or password. Please try again.";
            }
            else
            {
                Session["AdminId"] = output["Id"];
                Session["AdminName"] = output["LoginName"];
                Session["Roles"] = output["Roles"];
                Session["RoleId"] = output["RoleId"];
                Response.Redirect("reports.aspx");
            }

        }

        
    }
}