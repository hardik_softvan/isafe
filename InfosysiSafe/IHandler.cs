﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe
{
    /// <summary>
    /// All services exposed as APIs will need to implement this interface.
    /// This can easily be linked to an external interface. (JSON/XML/REST)
    /// </summary>
    public interface IHandler
    {
        /// <summary>
        /// The processing of the services takes place here.
        /// </summary>
        /// <param name="input">Key value pairs</param>
        /// <returns>Key value pairs</returns>
        Dictionary<string, object> Process(Dictionary<string, object> input);
    }
}