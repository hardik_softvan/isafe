﻿using InfosysiSafe.API;
using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace InfosysiSafe.User
{


    public partial class Master : System.Web.UI.MasterPage
    {
        DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
        Dictionary<string, object> input = new Dictionary<string, object>();

        private string htmlfile = "";
        public string HTMLPageFile
        {
            get { return htmlfile; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userId"] != null && Session["RoleId"] != null)
            {
                if (!IsPostBack)
                {
                    Session["Scenario"] = 0;
                    Session["Pledge"] = 0;
                    Session["ScenarioName"] = "";
                    loadPage();
                    loadMenu();
                    CheckStatus();
                }
                Certificate();
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        public void loadPage()
        {
            if (Session["userId"].ToString() != null)
            {

                hdnPageSessionType.Value = "currentPage";
                // Session["hdnPageSessionType"] = "currentPage";
                Index IndexAPI = new Index(dbAuthenticator, Global.Store, Global.Logger);
                input.Add("intMstUserid", Convert.ToString(Session["userId"]));
                input.Add("Page", "0");
                input.Add("Pageid", "0");
                input.Add("CurrentPageid", "0");
                input.Add("Model", "0");
                Dictionary<string, object> output = IndexAPI.Process(input);
                int PageSequence = Convert.ToInt32(output["SequenceNo"]);
                //hdnPageSeq.Value = output["SequenceNo"].ToString();
                Session["hdnPageSeq"] = output["SequenceNo"].ToString();
                ViewState["Model"] = Convert.ToInt32(output["Model"]);
                decimal ModelCnt = Convert.ToDecimal(output["ModelCount"]);
                //hdnModelCnt.Value =Convert.ToString(ModelCnt);
                Session["hdnModelCnt"] = Convert.ToString(ModelCnt);
                Session["Pledge"] = output["Pledge"];
                ViewState["PrivacyStatus"] = Convert.ToInt32(output["PrivacyStatus"]);
                Session["PrivacyStatus"] = ViewState["PrivacyStatus"];
                IframePages(PageSequence, Convert.ToInt32(output["Model"]), output["PageName"].ToString(), Convert.ToInt32(Session["userId"]), ModelCnt, output["Topic"].ToString(), Convert.ToInt32(output["Pageid"]));
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void imgbtnCertificate_Click(object sender, EventArgs e)
        {
            DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            PledgeApi objPledgeApi = new PledgeApi(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("intMstUserId", Session["userId"].ToString());
            input.Add("username", Session["username"].ToString());
            Dictionary<string, object> output = objPledgeApi.Process(input);
            if (output.ContainsKey("error"))
            {
                //lblMessage.Text = "Incorrect user id or password. Please try again.";
            }
            else
            {
                DBAuthenticator dbAuthenticator2 = new DBAuthenticator(Global.Store);
                CertificateApi objCertificateApi = new CertificateApi(Global.Store, Global.Logger, System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/CertificateTemplate.png"), System.Web.Hosting.HostingEnvironment.MapPath("~/images/Certificates/"));
                Dictionary<string, object> input2 = new Dictionary<string, object>();
                input2.Add("intMstUserId", Session["userId"].ToString());
                input2.Add("username", Session["username"].ToString());
                Dictionary<string, object> output2 = objCertificateApi.Process(input2);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Button_Alert", "alert('Pledge To UnderTake');", true);
                string htmlpage = "/html/showcertificate.aspx";
                PlaceHolder PlaceHolder1 = (PlaceHolder)ContentPlaceHolder1.FindControl("iframeDiv");
                PlaceHolder1.Controls.Add(new LiteralControl("<iframe id='pageIframe' onload='access()' src=\"" + htmlpage + "\" width='100%' height='100%'></iframe><br />"));
            }
        }

        protected void BtnNext_Click(object sender, EventArgs e)
        {

            //if (Convert.ToInt32(Session["Pledge"]) != 1)
            //{
            Index IndexAPI = new Index(dbAuthenticator, Global.Store, Global.Logger);
            input.Add("intMstUserid", Convert.ToString(Session["userId"]));
            input.Add("Page", "1");
            input.Add("Pageid", Convert.ToString(Convert.ToInt32(Session["hdnPageSequence"]) + 1));
            input.Add("CurrentPageid", Convert.ToString(Convert.ToInt32(Session["hdnPageSequence"])));
            input.Add("Model", Session["hdnModel"].ToString());
            Dictionary<string, object> output = IndexAPI.Process(input);
            int PageSequence = Convert.ToInt32(output["SequenceNo"]);
            int PageSequenceid = Convert.ToInt32(output["Pageid"]);
            int lstpage = Convert.ToInt32(output["LastVisted"]);
            hdnPageSessionType.Value = "nextPage";
            Session["hdnPageSeq"] = output["SequenceNo"].ToString();
            //if (lstpage <= PageSequenceid)
            //{
            //    //Session["hdnPageSessionType"] = "nextPage";
            //    hdnPageSessionType.Value = "nextPage";
            //    Session["hdnPageSeq"] = output["SequenceNo"].ToString();
            //}
            //else
            //{
            //    hdnPageSessionType.Value = "prevPage";
            //    //Session["hdnPageSessionType"] = "prevPage";
            //}
            int currentpage = Convert.ToInt32(Session["hdnPageSequence"]) + 1;
            decimal ModelCnt = Convert.ToDecimal(output["ModelCount"]);
            Session["Pledge"] = output["Pledge"];
            string PageName = output["PageName"].ToString();
            if (Convert.ToInt32(Session["Scenario"]) == 3)
            {
                Session["Scenario"] = 0;
                IframePages(PageSequence, Convert.ToInt32(output["Model"]), PageName, Convert.ToInt32(Session["userId"]), ModelCnt, output["Topic"].ToString(), Convert.ToInt32(output["Pageid"]));
            }
            else if (Convert.ToInt32(Session["Scenario"]) == 0)
            {
                IframePages(PageSequence, Convert.ToInt32(output["Model"]), PageName, Convert.ToInt32(Session["userId"]), ModelCnt, output["Topic"].ToString(), Convert.ToInt32(output["Pageid"]));
            }
            else
            {
                if (Session["ScenarioName"].ToString() == "Ben")
                {
                    Session["TopicValue"] = "What should Ben do in this situation";
                    PageName = "Module01-2-3.aspx";
                }
                if (Session["ScenarioName"].ToString() == "Roy")
                {
                    Session["TopicValue"] = "What should Roy do in this situation";
                    PageName = "Module01-1-6.aspx";
                }
                PlaceHolder PlaceHolder1 = (PlaceHolder)ContentPlaceHolder1.FindControl("iframeDiv");
                PlaceHolder1.Controls.Add(new LiteralControl("<iframe id='pageIframe' onload='access()' src=\"/html/" + PageName + "\" width='100%' height='100%' frameborder='0'></iframe><br />"));

            }

            // IframePages(PageSequence, Convert.ToInt32(output["Model"]), PageName, Convert.ToInt32(Session["userId"]), ModelCnt, output["Topic"].ToString(), Convert.ToInt32(output["Pageid"]));
            //}
            //else
            //{
            //    lblModule.Text = "Pledge";
            //    lblTopic.Text = "";
            //    string htmlpage = "/html/pledge.aspx";
            //    PlaceHolder PlaceHolder1 = (PlaceHolder)ContentPlaceHolder1.FindControl("iframeDiv");
            //    PlaceHolder1.Controls.Add(new LiteralControl("<iframe id='pageIframe' onload='access()' src=\"" + htmlpage + "\" width='100%' height='100%'></iframe><br />"));
            //}
        }

        protected void BtnPrev_Click(object sender, EventArgs e)
        {
            Session["hdnPageSessionType"] = "prevPage";
            hdnPageSessionType.Value = "prevPage";
            Index IndexAPI = new Index(dbAuthenticator, Global.Store, Global.Logger);
            input.Add("intMstUserid", Convert.ToString(Session["userId"]));
            input.Add("Page", "2");
            input.Add("Pageid", Convert.ToString(Convert.ToInt32(Session["hdnPageSequence"]) - 1));
            input.Add("CurrentPageid", Convert.ToString(Convert.ToInt32(Session["hdnPageSequence"])));
            input.Add("Model", Session["hdnModel"].ToString());
            Dictionary<string, object> output = IndexAPI.Process(input);
            int PageSequence = Convert.ToInt32(output["SequenceNo"]);
            decimal ModelCnt = Convert.ToDecimal(output["ModelCount"]);
            string PageName = output["PageName"].ToString();
            Session["DirectionType"] = 2;

            if (Convert.ToInt32(Session["Scenario"]) == 1)
            {
                if (Session["ScenarioName"].ToString() == "Ben")
                {
                    Session["TopicValue"] = "What should Ben do in this situation";
                    PageName = "Module01-2-3.aspx";
                }
                if (Session["ScenarioName"].ToString() == "Roy")
                {
                    Session["TopicValue"] = "What should Roy do in this situation";
                    PageName = "Module01-1-6.aspx";
                }
                PlaceHolder PlaceHolder1 = (PlaceHolder)ContentPlaceHolder1.FindControl("iframeDiv");
                PlaceHolder1.Controls.Add(new LiteralControl("<iframe id='pageIframe' onload='access()' src=\"/html/" + PageName + "\" width='100%' height='100%' frameborder='0'></iframe><br />"));
            }
            else if (Convert.ToInt32(Session["Scenario"]) == 2)
            {
                if (Session["ScenarioName"].ToString() == "Ben")
                {
                    Session["TopicValue"] = "What should Ben do in this situation";
                    PageName = "Module01-2-3.aspx";
                }
                if (Session["ScenarioName"].ToString() == "Roy")
                {
                    Session["TopicValue"] = "What should Roy do in this situation";
                    PageName = "Module01-1-6.aspx";
                }
                PlaceHolder PlaceHolder1 = (PlaceHolder)ContentPlaceHolder1.FindControl("iframeDiv");
                PlaceHolder1.Controls.Add(new LiteralControl("<iframe id='pageIframe' onload='access()' src=\"/html/" + PageName + "\" width='100%' height='100%' frameborder='0'></iframe><br />"));
            }
            else if (Convert.ToInt32(Session["Scenario"]) == 3)
            {
                if (Session["ScenarioName"].ToString() == "Ben")
                {
                    Session["TopicValue"] = "What should Ben do in this situation";
                    PageName = "Module01-2-3.aspx";
                }
                if (Session["ScenarioName"].ToString() == "Roy")
                {
                    Session["TopicValue"] = "What should Roy do in this situation";
                    PageName = "Module01-1-6.aspx";
                }
                Session["Scenario"] = 0;
                PlaceHolder PlaceHolder1 = (PlaceHolder)ContentPlaceHolder1.FindControl("iframeDiv");
                PlaceHolder1.Controls.Add(new LiteralControl("<iframe id='pageIframe' onload='access()' src=\"/html/" + PageName + "\" width='100%' height='100%' frameborder='0'></iframe><br />"));
            }
            else
            {
                IframePages(PageSequence, Convert.ToInt32(output["Model"]), PageName, Convert.ToInt32(Session["userId"]), ModelCnt, output["Topic"].ToString(), Convert.ToInt32(output["Pageid"]));
            }

        }

        public void IframePages(int PageSequence, int Model, string PageName, int intMstUserid, decimal ModelCnt, string Topic, int Pageid)
        {

            ScriptManager.RegisterStartupScript(UpdatePanel3, this.GetType(), "MyAction", "Preloader();", true);
            int PageNextValue = Convert.ToInt32(PageSequence) + 1;
            int PagePrevValue = Convert.ToInt32(PageSequence) - 1;
            Session["Pageid"] = Pageid;
            Session["PageSequence"] = PageSequence;
            Session["hdnPageSequence"] = PageSequence.ToString();
            Session["hdnModel"] = Model.ToString();
            Session["ScenarioName"] = "";
            Session["hdnNextiFrameId"] = PageNextValue.ToString();
            Session["hdnEmpId"] = Session["userId"].ToString();
            Session["hdnNextiFrameId"] = PageName;
            Session["hdnPreviFrameId"] = PagePrevValue.ToString();
            lblModule.Text = "Module " + Model.ToString();
            lblTopic.Text = Topic;
            Session["TopicValue"] = Topic;
            lblUsername.Text = Session["username"].ToString();
            decimal TopicCnt = 100 / ModelCnt;
            int prgbar = Convert.ToInt32((Convert.ToInt32(PageSequence) * TopicCnt));
            int Progressbar = progressbar(prgbar);
            ltrPersentage.Text = "<div id='ltrPersentage'  class='progress-bar progress-bar-striped active' role='progressbar'  aria-valuenow='" + Progressbar + "' aria-valuemin='0' aria-valuemax='100' style='width:" + Progressbar + "%;'></div>";
            LtrlPersentageCnt.Text = "<span id='LtrlPersentageCnt'>" + Progressbar + "% </span>";
            BtnNext.Attributes.Add("class", "hide");
            string htmlpage = "/html/" + PageName + "";
            PlaceHolder PlaceHolder1 = (PlaceHolder)ContentPlaceHolder1.FindControl("iframeDiv");
            PlaceHolder1.Controls.Add(new LiteralControl("<iframe id='pageIframe' onload='access()' src=\"" + htmlpage + "\" width='100%' height='100%' frameborder='0'></iframe><br />"));
            loadMenu();
            ScriptManager.RegisterStartupScript(UpdatePanel3, this.GetType(), "Action", "Proloader();", true);
            if (Convert.ToInt32(ViewState["PrivacyStatus"]) != 0)
            {

                ScriptManager.RegisterStartupScript(UpdatePanel3, this.GetType(), "Popuphide", "popuphide();", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(UpdatePanel3, this.GetType(), "PopupShow", "popupshow();", true);
            }

        }
        public int progressbar(int percentage)
        {
            if(percentage > 100)
            {
                percentage = 100;
            }

                return percentage;
        }

        public void loadMenu()
        {
            MenuList IndexAPI = new MenuList(dbAuthenticator, Global.Store, Global.Logger);
            Dictionary<string, object> output = IndexAPI.Process(input);
            ltrlMenu.Text = output["MenuList"].ToString();
            ltrlSubmenu.Text = output["Submenu"].ToString();

        }

        public void Certificate()
        {
            Dictionary<string, object> filter2 = new Dictionary<string, object>();
            filter2.Add("Name", "ByPledge");
            filter2.Add("UserId", Convert.ToInt32(Session["userId"]));
            List<IPersistantObject> Pledge = Global.Store.Find("iSafeTrnPledge", filter2);
            if (Pledge.Count > 0)
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "showPledge()", true);
                ScriptManager.RegisterStartupScript(UpdatePanel6, this.GetType(), "PopupShow", "Certificate();", true);
              
                

            }
            //else
            //{
            //    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "hidePledge()", true);
            //    ScriptManager.RegisterStartupScript(UpdatePanel6, this.GetType(), "PopupShow", "popupshow();", true);
            //    imgbtnCertificate.Visible = false;
            //}
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session["userId"] = null;
            Session.Clear();
            Session.Abandon();
            Response.Redirect("Login.aspx");
        }

        public void CheckStatus()
        {
            if (Convert.ToInt32(Session["RoleId"]) == 1)
            {
                ancAdmin.Visible = true;
            }
            else
            {
                ancAdmin.Visible = false;
            }

        }


        protected void PrivacyStatus_Click(object sender, EventArgs e)
        {
            iSafeMstUser iSafeMstUserPrivacyStatus = new iSafeMstUser();
            iSafeMstUserPrivacyStatus.intPrivacyStatus = 1;
            iSafeMstUserPrivacyStatus.intId = Convert.ToInt32(Session["userId"]);
            iSafeMstUserPrivacyStatus.intMstCompanyId = 0;
            iSafeMstUserPrivacyStatus.intMstEmployeeTypeId = 0;
            iSafeMstUserPrivacyStatus.intRoleId = 0;
            iSafeMstUserPrivacyStatus.txtEmailId = "";
            iSafeMstUserPrivacyStatus.dtCreatedOn = DateTime.Now;
            iSafeMstUserPrivacyStatus.txtEmployeeId = "";
            iSafeMstUserPrivacyStatus.txtName = "";
            Global.Store.Update(iSafeMstUserPrivacyStatus);
            Response.Redirect("~/User/index.aspx");

        }
        protected void Pledge_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> filter2 = new Dictionary<string, object>();
            filter2.Add("Name", "ByPledge");
            filter2.Add("UserId", Convert.ToInt32(Session["userId"]));
            List<IPersistantObject> Pledge = Global.Store.Find("iSafeTrnPledge", filter2);
            if (Pledge.Count > 0)
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "showPledge()", true);
                ScriptManager.RegisterStartupScript(UpdatePanel6, this.GetType(), "PopupShow", "Certificate();", true);

            }
            Session["TopicValue"] = "Conclusion";
            PlaceHolder PlaceHolder1 = (PlaceHolder)ContentPlaceHolder1.FindControl("iframeDiv");
            PlaceHolder1.Controls.Add(new LiteralControl("<iframe id='pageIframe' onload='access()' src=\"" + "../html/Conclusion.aspx" + "\" width='100%' height='100%' frameborder='0'></iframe><br />"));
        }

    }

}