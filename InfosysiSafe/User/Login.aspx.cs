﻿using InfosysiSafe.API;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace InfosysiSafe.Admin
{
    public partial class login : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            //userlogin("isafeuser10989", 1);  //Pass value Dynamic
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            userlogin(txtLoginName.Text, 1);
            //string loginName = txtLoginName.Text;
            //int RoleId = 1;

            //if (loginName !="") { 
            //DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
            //UserLogin loginAPI = new UserLogin(dbAuthenticator, Global.Store, Global.Logger);
            //Dictionary<string, object> input = new Dictionary<string, object>();
            //input.Add("loginName", loginName);
            //input.Add("RoleId", RoleId);
            //Dictionary<string, object> output = loginAPI.Process(input);
            //if (output.ContainsKey("error"))
            //{
            //        Exception ex = new Exception(output.ContainsKey("error").ToString());
            //        LogError(ex);
            //        lblMessage.Text = "Incorrect user id or password. Please try again.";
            //}
            //else
            //{
            //    Session["userId"] = output["intUserId"];
            //    Session["username"] = output["txtName"];
            //    Session["EmployeeId"] = output["EmployeeId"];
            //    Session["RoleId"] = output["RoleId"];
            //    Response.Redirect("Index.aspx");
            //}
            //}
            //else
            //{
            //    lblMessage.Text = " User id and password required";
            //}
        }

        private void LogError(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = Server.MapPath("~/ErrorLog/ErrorLog.txt");
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }

        public void userlogin(string loginName, int RoleId)
        {
            if (loginName != "")
            {
                DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
                UserLogin loginAPI = new UserLogin(dbAuthenticator, Global.Store, Global.Logger);
                Dictionary<string, object> input = new Dictionary<string, object>();
                input.Add("loginName", loginName);
                input.Add("RoleId", RoleId);
                Dictionary<string, object> output = loginAPI.Process(input);
                if (output.ContainsKey("error"))
                {
                    Exception ex = new Exception(output.ContainsKey("error").ToString());
                    LogError(ex);

                }
                else
                {
                    Session["userId"] = output["intUserId"];
                    Session["username"] = output["txtName"];
                    Session["EmployeeId"] = output["EmployeeId"];
                    Session["RoleId"] = output["RoleId"];
                    Response.Redirect("Index.aspx");
                }
            }
            else
            {
                //lblMessage.Text = " User id and password required";
            }
        }
        }
    }