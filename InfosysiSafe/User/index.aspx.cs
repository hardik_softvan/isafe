﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InfosysiSafe;
using InfosysiSafe.API;
using InfosysiSafe.Domain;
using Newtonsoft.Json;

public partial class index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
  
    }

    [System.Web.Services.WebMethod]
    public static string Menu(int hdnPageSequence, int hdnPreviFrameId, int hdnNextiFrameId, int hdnModel, int hdnModelCnt, string title)
    {
        HttpContext.Current.Session["hdnPageSequence"] = hdnPageSequence;
        HttpContext.Current.Session["hdnPreviFrameId"] = hdnPreviFrameId;
        HttpContext.Current.Session["hdnNextiFrameId"] = hdnNextiFrameId;
        HttpContext.Current.Session["hdnModel"] = hdnModel;
        HttpContext.Current.Session["PageSequence"] = hdnPageSequence;
        HttpContext.Current.Session["hdnModelCnt"] = hdnModelCnt;
        HttpContext.Current.Session["TopicValue"] = title;
        return JsonConvert.SerializeObject("succeed");
    }
    [System.Web.Services.WebMethod]
    public static string Topic(int hdnPageSequence, int hdnPreviFrameId, int hdnNextiFrameId, int hdnModel)
    {
        HttpContext.Current.Session["hdnPageSequence"] = hdnPageSequence;
        HttpContext.Current.Session["hdnPreviFrameId"] = hdnPreviFrameId;
        HttpContext.Current.Session["hdnNextiFrameId"] = hdnNextiFrameId;
        HttpContext.Current.Session["hdnModel"] = hdnModel;
        HttpContext.Current.Session["PageSequence"] = hdnPageSequence;
        return JsonConvert.SerializeObject("succeed");
    }

    [System.Web.Services.WebMethod]
    public static string Header()
    {
        int Model = Convert.ToInt32(HttpContext.Current.Session["hdnModel"]);
        string PageName = HttpContext.Current.Session["TopicValue"].ToString();
        string PageSequence = HttpContext.Current.Session["PageSequence"].ToString();
        string hdnModelCnt = HttpContext.Current.Session["hdnModelCnt"].ToString();
        return JsonConvert.SerializeObject("'{\"Model\":\"" + Model + "\",\"PageName\":\"" + PageName + "\",\"PageSequence\":\"" + PageSequence + "\",\"hdnModelCnt\":\"" + hdnModelCnt + "\"}'");
    }


    public void userlogin(string loginName,int RoleId)
        {
           
            if (loginName != "")
            {
                DBAuthenticator dbAuthenticator = new DBAuthenticator(Global.Store);
                UserLogin loginAPI = new UserLogin(dbAuthenticator, Global.Store, Global.Logger);
                Dictionary<string, object> input = new Dictionary<string, object>();
                input.Add("loginName", loginName);
                input.Add("RoleId", RoleId);
                Dictionary<string, object> output = loginAPI.Process(input);
                if (output.ContainsKey("error"))
                {
                    Exception ex = new Exception(output.ContainsKey("error").ToString());
                    LogError(ex);
                  
                }
                else
                {
                    Session["userId"] = output["intUserId"];
                    Session["username"] = output["txtName"];
                    Session["EmployeeId"] = output["EmployeeId"];
                    Session["RoleId"] = output["RoleId"];
            }
            }
            else
            {
                //lblMessage.Text = " User id and password required";
            }

        }

        private void LogError(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = Server.MapPath("~/ErrorLog/ErrorLog.txt");
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
}