﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe
{
    public class DBAuthenticator : IAuthenticator
    {
        IPersistantStore Store { get; }

        public DBAuthenticator(IPersistantStore store)
        {
            Store = store;
        }

        public Dictionary<string, object> GetUserDetails(string loginName, string password)
        {
            Dictionary<string, object> details = new Dictionary<string, object>();
            return details;
        }

        public bool IsValid(string loginName, string password)
        {
            Dictionary<string, object> filter = new Dictionary<string, object>();
            filter.Add("Name", "ByLoginName_Password");
            filter.Add("LoginName", loginName);
            filter.Add("Password", password);
            List<IPersistantObject> users = Store.Find("iSafeMstUser", filter);
            return users.Count == 1;
        }
    }
}