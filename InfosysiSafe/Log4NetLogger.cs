﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe
{
    public class Log4NetLogger : ILogger
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Log4NetLogger()
        {
            log4net.Config.XmlConfigurator.Configure();
            SetContext("");
        }

        public void SetContext(string context)
        {
            log4net.LogicalThreadContext.Properties["Context"] = context;
        }

        public void Debug(object message) { log.Debug(message); }
        public void Debug(object message, Exception ex) { log.Debug(message, ex); }

        public void Info(object message) { log.Info(message); }
        public void Info(object message, Exception ex) { log.Info(message, ex); }

        public void Warn(object message) { log.Warn(message); }
        public void Warn(object message, Exception ex) { log.Warn(message, ex); }

        public void Error(object message) { log.Error(message); }
        public void Error(object message, Exception ex) { log.Error(message, ex); }

        public void Fatal(object message) { log.Fatal(message); }
        public void Fatal(object message, Exception ex) { log.Fatal(message, ex); }
    }
}