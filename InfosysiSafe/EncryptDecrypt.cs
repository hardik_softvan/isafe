﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace InfosysiSafe
{
    public class EncryptDecrypt
    {
        private const string CHARS = "?*^&%$#@!~";

        public static string Encrypt(int integer)
        {
            StringBuilder sb = new StringBuilder();
            Random rnd = new Random();
            char[] integerChar = (((integer < 10) ? "00" : ((integer < 100) ? "0" : "")) + integer.ToString()).ToCharArray();
            sb.Append(rnd.Next(1, 9));
            int temp = rnd.Next(1, 9);
            sb.Append(temp);
            sb.Append(Pad0(temp + (int)Char.GetNumericValue(integerChar[0])));
            temp = rnd.Next(1, 9);
            sb.Append(temp);
            sb.Append(Pad0(temp + (int)Char.GetNumericValue(integerChar[1])));
            temp = rnd.Next(1, 9);
            sb.Append(temp);
            sb.Append(Pad0(temp + (int)Char.GetNumericValue(integerChar[2])));
            temp = rnd.Next(1, 9);
            sb.Append(temp);
            return ToStr(sb.ToString());
        }

        private static string ToStr(string num)
        {
            StringBuilder sb = new StringBuilder();
            char[] numArr = num.ToCharArray();
            for (int i = 0; i < numArr.Length; i++)
            {
                sb.Append(CHARS.Substring((int)Char.GetNumericValue(numArr[i]), 1));
            }
            return sb.ToString();
        }

        private static string ToNum(string num)
        {
            StringBuilder sb = new StringBuilder();
            char[] numArr = num.ToCharArray();
            for (int i = 0; i < numArr.Length; i++)
            {
                sb.Append(CHARS.IndexOf(numArr[i]));
            }
            return sb.ToString();
        }

        private static string Pad0(int integer)
        {
            return (integer < 10 ? "0" : "") + integer.ToString();
        }

        public static int Decrypt(string encrypted)
        {
            char[] integerChar = ToNum(encrypted).ToCharArray();
            int result = 0;
            int temp = (int)Char.GetNumericValue(integerChar[1]);
            int total = Convert.ToInt16("" + integerChar[2] + integerChar[3]);
            result += 100 * (total - temp);
            temp = (int)Char.GetNumericValue(integerChar[4]);
            total = Convert.ToInt16("" + integerChar[5] + integerChar[6]);
            result += 10 * (total - temp);
            temp = (int)Char.GetNumericValue(integerChar[7]);
            total = Convert.ToInt16("" + integerChar[8] + integerChar[9]);
            result += (total - temp);
            return result;
        }
    }
}