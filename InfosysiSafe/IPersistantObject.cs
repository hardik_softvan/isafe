﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfosysiSafe
{
    /// <summary>
    /// Persistant objects that are stored in a storage (database etc.)
    /// will need to implement this interface
    /// </summary>
    public interface IPersistantObject
    {
        /// <summary>
        /// Primary key 
        /// </summary>
        long intId { get; set; }
        
        /// <summary>
        /// Version number, this is incremented each time an object gets updated
        /// </summary>
      //  int Version { get; set; }
    }
}
