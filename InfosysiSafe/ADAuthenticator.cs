﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.DirectoryServices;

namespace InfosysiSafe
{
    public class ADAuthenticator : IAuthenticator
    {
        private string Path;

        public ADAuthenticator(string path)
        {
            Path = path;
        }
        public bool IsValid(string loginName, string password)
        {
#if DEBUG
            return true;
#else
            DirectoryEntry de = new DirectoryEntry(Path, loginName, password);
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(sAMAccountName=" + loginName + ")";
            SearchResultCollection results = deSearch.FindAll();
            if(results.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }            
#endif
        }
        public Dictionary<string, object> GetUserDetails(string loginName, string password)
        {
            Dictionary<string, object> details = new Dictionary<string, object>();
#if DEBUG
            details.Add("userName", "iSafe");
            details.Add("department", "");
#else
            DirectoryEntry de = new DirectoryEntry(Path, loginName, password);
            DirectorySearcher deSearch = new DirectorySearcher(de);
            deSearch.Filter = "(sAMAccountName=" + loginName + ")";
            SearchResultCollection results = deSearch.FindAll();
            if (results.Count == 0)
            {
                throw new Exception("Invalid User");
            }
            else
            {
                var userEntry = results[0].GetDirectoryEntry();
                de.Close();
                string name = userEntry.Properties["Name"].Value.ToString().Replace("'", "''");
                string department = userEntry.Properties["department"].Value.ToString().Replace("'", "''") == "" ? "Not Available" : userEntry.Properties["department"].Value.ToString().Replace("'", "''");
                details.Add("userName", name);
                details.Add("department", department);
            }
#endif
            return details;
        }
    }
}