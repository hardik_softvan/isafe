﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfosysiSafe
{
    /// <summary>
    /// Persistant stores like MSSQL server DB, MySQL server DB will need to implement this interface.
    /// This class provides CRUD functionaltity for the store.
    /// </summary>
    public interface IPersistantStore
    {
        /// <summary>
        /// Creates an object of the specified type.
        /// </summary>
        /// <param name="className">Class name, in case of DB, this is the table name</param>
        /// <param name="newObject">The instance of the object that needs to be persisted</param>
        void Create(string className, IPersistantObject newObject);

        /// <summary>
        /// Returns a single object from the store.
        /// </summary>
        /// <param name="className">Class name / table name</param>
        /// <param name="propertyName">Used for filtering, in case of DB, this is the column name</param>
        /// <param name="value">The value of the specified property</param>
        /// <returns>Persistant object or null</returns>
        IPersistantObject Get(string className, string propertyName, object value);

        /// <summary>
        /// Persists the given object into the store. Existing object will be overwitten.
        /// Class name is inferred from the given object.
        /// </summary>
        /// <param name="updatedObject">Persistant object that needs to be saved</param>
        void Update(IPersistantObject updatedObject);

        /// <summary>
        /// Returns zero, one or more instances of persistant objects from the store based on the 
        /// given criteria.
        /// </summary>
        /// <param name="className">Class name</param>
        /// <param name="filter">Filter criteria</param>
        /// <returns>List of persistant objects</returns>
        List<IPersistantObject> Find(string className, Dictionary<string, object> filter);

        /// <summary>
        /// Returns count of persistant objects from the store based on the 
        /// given criteria.
        /// </summary>
        /// <param name="className">Class name</param>
        /// <param name="filter">Filter criteria</param>
        /// <returns>Count</returns>
        long Count(string className, Dictionary<string, object> filter);
    }
}
