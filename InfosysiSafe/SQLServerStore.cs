﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using InfosysiSafe.Domain;

namespace InfosysiSafe
{
    public class SQLServerStore : IPersistantStore
    {
        private string ConnectionString { get; }
        public SQLServerStore(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public void Create(string className, IPersistantObject newObject)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(className + "_Create", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param;
                foreach (PropertyInfo propertyInfo in newObject.GetType().GetProperties())
                {
                    if (!propertyInfo.Name.Equals("Id"))
                    {
                        string datatype = GetDataType(propertyInfo);
                        switch (datatype)
                        {
                            case "Int32":
                                param = new SqlParameter(propertyInfo.Name, SqlDbType.Int);
                                param.Value = propertyInfo.GetValue(newObject);
                                break;
                            case "Int64":
                                param = new SqlParameter(propertyInfo.Name, SqlDbType.BigInt);
                                param.Value = propertyInfo.GetValue(newObject);
                                break;
                            case "Decimal":
                                param = new SqlParameter(propertyInfo.Name, SqlDbType.Decimal);
                                param.SourceColumn = propertyInfo.Name;
                                param.Value = propertyInfo.GetValue(newObject);
                                param.Precision = 8;
                                param.Scale = 2;                                
                                break;
                            case "String":
                                param = new SqlParameter(propertyInfo.Name, SqlDbType.VarChar);
                                param.Value = propertyInfo.GetValue(newObject);
                                break;
                            case "Single":
                                param = new SqlParameter(propertyInfo.Name, SqlDbType.Float);
                                param.Value = propertyInfo.GetValue(newObject);
                                break;
                            case "Boolean":
                                param = new SqlParameter(propertyInfo.Name, SqlDbType.Bit);
                                param.Value = propertyInfo.GetValue(newObject);
                                break;
                            case "DateTime":
                                param = new SqlParameter(propertyInfo.Name, SqlDbType.DateTime);
                                param.Value = propertyInfo.GetValue(newObject);
                                break;
                            default:
                                throw new NotImplementedException("Create -> Data type(" + datatype + "): " + propertyInfo.PropertyType.Name);
                        }
                        cmd.Parameters.Add(param);
                    }
                }
                SqlDataReader rdr = cmd.ExecuteReader();
            }
        }
        public IPersistantObject Get(string className, string propertyName, object value)
        {
            IPersistantObject obj = null;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(className + "_Get", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("columnName", SqlDbType.VarChar);
                param.Value = propertyName;
                cmd.Parameters.Add(param);
                param = new SqlParameter("columnValue", SqlDbType.Variant);
                param.Value = value;
                cmd.Parameters.Add(param);
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Type type = Type.GetType("InfosysiSafe.Domain." + className, true);
                    obj = (IPersistantObject)Activator.CreateInstance(type);
                    foreach (PropertyInfo propertyInfo in type.GetProperties())
                    {
                        string datatype = GetDataType(propertyInfo);
                        switch (datatype)
                        {
                            case "Int32":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToInt32(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "Int64":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToInt64(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "String":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToString(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "Single":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToSingle(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "Boolean":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToBoolean(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "DateTime":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToDateTime(rdr[propertyInfo.Name]));
                                }
                                break;
                            default:
                                throw new NotImplementedException("Get -> Data type: " + propertyInfo.PropertyType.Name);
                        }
                    }
                }
            }
            return obj;
        }
        public void Update(IPersistantObject updatedObject)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                string className = updatedObject.GetType().Name;
                connection.Open();
                SqlCommand cmd = new SqlCommand(className + "_Update", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param;
                foreach (PropertyInfo propertyInfo in updatedObject.GetType().GetProperties())
                {
                    string datatype = GetDataType(propertyInfo);
                    switch (datatype)
                    {
                        case "Int32":
                            param = new SqlParameter(propertyInfo.Name, SqlDbType.Int);
                            param.Value = propertyInfo.GetValue(updatedObject);
                            break;
                        case "Int64":
                            param = new SqlParameter(propertyInfo.Name, SqlDbType.BigInt);
                            param.Value = propertyInfo.GetValue(updatedObject);
                            break;
                        case "String":
                            param = new SqlParameter(propertyInfo.Name, SqlDbType.VarChar);
                            param.Value = propertyInfo.GetValue(updatedObject);
                            break;
                        case "Single":
                            param = new SqlParameter(propertyInfo.Name, SqlDbType.Float);
                            param.Value = propertyInfo.GetValue(updatedObject);
                            break;
                        case "Boolean":
                            param = new SqlParameter(propertyInfo.Name, SqlDbType.Bit);
                            param.Value = propertyInfo.GetValue(updatedObject);
                            break;
                        case "DateTime":
                            param = new SqlParameter(propertyInfo.Name, SqlDbType.DateTime);
                            param.Value = propertyInfo.GetValue(updatedObject);
                            break;
                        default:
                            throw new NotImplementedException("Update -> Data type(" + datatype + "): " + propertyInfo.PropertyType.Name);
                    }
                    cmd.Parameters.Add(param);
                }
                SqlDataReader rdr = cmd.ExecuteReader();
            }
        }
        private string GetDataType(PropertyInfo propertyInfo)
        {
            if (Nullable.GetUnderlyingType(propertyInfo.PropertyType) != null)
                return Nullable.GetUnderlyingType(propertyInfo.PropertyType).Name;
            else if (propertyInfo.PropertyType.IsEnum)
                return "Int32";
            else
                return propertyInfo.PropertyType.Name;
        }
        public List<IPersistantObject> Find(string className, Dictionary<string, object> filter)
        {
            List<IPersistantObject> output = new List<IPersistantObject>();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(className + "_Find" + filter["Name"], connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param;
                foreach (string key in filter.Keys)
                {
                    if (!key.Equals("Name"))
                    {
                        param = new SqlParameter(key, SqlDbType.Variant);
                        param.Value = filter[key];
                        cmd.Parameters.Add(param);
                    }
                }
                SqlDataReader rdr = cmd.ExecuteReader();
                IPersistantObject obj = null;
                while (rdr.Read())
                {
                    Type type = Type.GetType("InfosysiSafe.Domain." + className, true);
                    obj = (IPersistantObject)Activator.CreateInstance(type);
                    foreach (PropertyInfo propertyInfo in type.GetProperties())
                    {
                        string datatype = GetDataType(propertyInfo);
                        switch (datatype)
                        {
                            case "Int32":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToInt32(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "Int64":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToInt64(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "Decimal":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToDecimal(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "String":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToString(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "Single":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToSingle(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "Boolean":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToBoolean(rdr[propertyInfo.Name]));
                                }
                                break;
                            case "DateTime":
                                if (rdr[propertyInfo.Name] != DBNull.Value)
                                {
                                    propertyInfo.SetValue(obj, Convert.ToDateTime(rdr[propertyInfo.Name]));
                                }
                                break;
                            default:
                                throw new NotImplementedException("Find -> Data type: " + propertyInfo.PropertyType.Name);
                        }
                    }
                    output.Add(obj);
                }
            }
            return output;
        }
        public long Count(string className, Dictionary<string, object> filter)
        {
            long count = 0L;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand(className + "_Count" + filter["Name"], connection);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param;
                foreach (string key in filter.Keys)
                {
                    if (!key.Equals("Name"))
                    {
                        param = new SqlParameter(key, SqlDbType.Variant);
                        param.Value = filter[key];
                        cmd.Parameters.Add(param);
                    }
                }
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    count = Convert.ToInt64(rdr[0]);
                }
            }
            return count;
        }
    }
}