﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe
{
    public class DevelopmentAuthenticator : IAuthenticator
    {
        public DevelopmentAuthenticator()
        {
#if DEBUG
#else
            throw new InvalidOperationException("Can use DevelopmentAuthenticator only in DEBUG configuration");
#endif
        }
        public bool IsValid(string loginName, string password)
        {
#if DEBUG
            return true;
#else
            throw new InvalidOperationException("Can use DevelopmentAuthenticator only in DEBUG configuration");
#endif
        }
        public Dictionary<string, object> GetUserDetails(string loginName, string password)
        {
#if DEBUG
            Dictionary<string, object> details = new Dictionary<string, object>();
            details.Add("userName", "Unknown User");
            details.Add("department", "Some Department");
            return details;
#else
            throw new InvalidOperationException("Can use DevelopmentAuthenticator only in DEBUG configuration");
#endif
        }
    }
}