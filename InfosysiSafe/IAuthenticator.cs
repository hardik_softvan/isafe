﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe
{
    /// <summary>
    /// All authenticators will need to implement this interface
    /// </summary>
    public interface IAuthenticator
    {
        /// <summary>
        /// Authenticates the given credentials and returns true if valid, else returns false
        /// </summary>
        /// <param name="loginName">LoginName</param>
        /// <param name="password">Password</param>
        /// <returns>true if credentials are valid, else returns false</returns>
        bool IsValid(string loginName, string password);

        /// <summary>
        /// Gets the details of the specified user
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="password">Password</param>
        /// <returns>Key value pairs of user properties</returns>
        Dictionary<string, object> GetUserDetails(string loginName, string password);
    }
}