﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeMstTopic : IPersistantObject
    {
        public long intId { get; set; }
        public int intMstModuleId { get; set; }
        public string txtName { get; set; }
        public int intSequenceNo { get; set; }
        public DateTime dtCreatedOn { get; set; }

        public iSafeMstTopic()
        {
            dtCreatedOn = DateTime.Now;
        }

        public override string ToString()
        {
            return "iSafeMstTopic: " + intId + ", " + txtName + ", " + intMstModuleId + ", " + intSequenceNo + "," + dtCreatedOn;
        }
    }
}