﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeMstModule : IPersistantObject
    {
        public long intId { get; set; }
        public string txtName { get; set; }
        public int intSequenceNo { get; set; }
        public DateTime dtCreatedOn { get; set; }
        
        public iSafeMstModule()
        {
            dtCreatedOn = DateTime.Now;
        }

        public override string ToString()
        {
            return "iSafeMstModule: " + intId + ", " + txtName +  ", " + intSequenceNo + "," + dtCreatedOn;
        }
    }
}