﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeTrnAdminSessionPage : IPersistantObject
    {
        public long intId { get; set; }
        // public int count { get; set; }
        public int intMstAdminId { get; set; }
        public string txtTrnSessionId { get; set; }
        public DateTime dtVisitedAt { get; set; }


        public iSafeTrnAdminSessionPage()
        {
            dtVisitedAt = DateTime.Now;
        }

        public override string ToString()
        {
            return "iSafeTrnAdminSessionPage: " + intId + "," + intMstAdminId + "," + txtTrnSessionId + "," + txtTrnSessionId + "," + dtVisitedAt;
        }
    }
}
