﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public enum SessionState
    {
        Active = 0,
        Closed = 1,
        TimedOut = 2
    }
}