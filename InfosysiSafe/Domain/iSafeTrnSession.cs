﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeTrnSession : IPersistantObject
    {
        public long intId { get; set; }
        public string txtSessionId { get; set; }
        public int intMstUserid { get; set; }
        public SessionState intState { get; set; }
        public DateTime dtStart { get; set; }
        public DateTime dtEnd { get; set; }
        
        public iSafeTrnSession()
        {
            dtStart = DateTime.Now;
            intState = SessionState.Active;
            dtEnd = DateTime.Now; 
        }

        public override string ToString()
        {
            return "iSafeTrnSession: "+intId+","+txtSessionId+","+intMstUserid+","+dtStart+","+dtEnd+","+ intState;
        }
    }
}