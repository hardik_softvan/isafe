﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeTrnAssessmentQuestion : IPersistantObject
    {
        public long intId { get; set; }
        public int intTrnAssessmentId { get; set; }
        public int intMstModuleQuestionId { get; set; }
        public int intUserOption { get; set; }

        public iSafeTrnAssessmentQuestion()
        {

        }

        public override string ToString()
        {
            return "[{\"id\":\"" + intId + "\",\"intTrnAssessmentId\":\"" + intTrnAssessmentId + "\", \"intMstModuleQuestionId\":\"" + intMstModuleQuestionId + "\", \"intUserOption\":\"" + intUserOption + "\" }]";
        }

    }
}