﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeTrnCertificate : IPersistantObject
    {
        public long intId { get; set; }
        public int intMstUserid { get; set; }
        public string txtCertificateUrl { get; set;}
        public DateTime dtCreatedOn { get; set; }
       

        public iSafeTrnCertificate()
        {
            dtCreatedOn = DateTime.Now;
        }

        public override string ToString()
        {
            return "iSafeTrnCertificate: " + intId + ", " + intMstUserid + ", " + txtCertificateUrl + ", " + dtCreatedOn;
        }
    }
}