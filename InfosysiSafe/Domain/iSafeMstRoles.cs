﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeMstRoles : IPersistantObject
    {
        public long intId { get; set; }
        public string Roles { get; set; }
       
        public iSafeMstRoles()
        {
          
        }

        public override string ToString()
        {
            return "iSafeMstRoles: " + intId + ", " + Roles;
        }
    }
}