﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeMstUser : IPersistantObject
    {
        public long intId { get; set; }
        public string txtEmployeeId { get; set; }
        public int intMstEmployeeTypeId { get; set; }
        public int intMstCompanyId { get; set; }
        public DateTime dtCreatedOn { get; set; }
        public string txtName { get; set; }
        public string txtEmailId { get; set; }
        public int intRoleId { get; set; }
        public int intPrivacyStatus { get; set; }
        public iSafeMstUser()
        {
            txtEmailId = Guid.NewGuid().ToString();
            dtCreatedOn = DateTime.Now;
        }

        public override string ToString()
        {
            return "iSafeMstUser: " + intId + ", " + txtName + ", " + txtEmployeeId + ", " + txtEmailId + ", " + intMstEmployeeTypeId + ", " + intRoleId + ", " + intPrivacyStatus;
        }
    }
}