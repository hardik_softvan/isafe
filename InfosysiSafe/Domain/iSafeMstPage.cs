﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeMstPage : IPersistantObject
    {
        public long intId { get; set; }
        public int intMstTopicid { get; set; }
        public string txtType { get; set; }
        public string txtName { get; set; }
        public int intSequenceNo { get; set; }
        public DateTime dtCreatedOn { get; set; }

        public iSafeMstPage()
        {
            dtCreatedOn = DateTime.Now;
        }

        public override string ToString()
        {
            return "iSafeMstPage: " + intId + ", " + txtName + ", " + intMstTopicid + ", " + txtType + ", " + intSequenceNo+","+ dtCreatedOn;
        }
    }
}