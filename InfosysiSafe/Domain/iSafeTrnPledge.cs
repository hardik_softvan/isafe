﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeTrnPledge : IPersistantObject
    {
        public long intId { get; set; }
        public int intMstUserId { get; set; }
        public DateTime dtPledgeTakenOn { get; set; }
      

        public iSafeTrnPledge()
        {
            dtPledgeTakenOn = DateTime.Now;
        }

        public override string ToString()
        {
            return "iSafeMstPage: " + intId + ", " + intMstUserId + ", " + dtPledgeTakenOn ;
        }
    }
}