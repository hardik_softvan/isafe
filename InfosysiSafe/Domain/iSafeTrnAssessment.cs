﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeTrnAssessment : IPersistantObject
    {
        public long intId { get; set; }
        public int intMstModuleId { get; set; }
        public int intMstUserId { get; set; }
        public int intScore { get; set; }
        public bool flgResult { get; set; } 
        public DateTime? dtAssessmentStartAt { get; set; }
        public DateTime? dtAssessmentCompletedAt { get; set; }

        public iSafeTrnAssessment()
        {
        }

        public override string ToString()
        {
            return "[{\"id\":\"" + intId + "\",\"intMstModuleId\":\"" + intMstModuleId + "\", \"intMstUserId\":\"" + intMstUserId + "\", \"intScore\":\"" + intScore + "\", \"flgResult\":\"" + flgResult + "\" }]";
        }

    }
}