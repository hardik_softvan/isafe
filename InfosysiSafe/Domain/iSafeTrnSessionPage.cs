﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace InfosysiSafe.Domain
{
    public class iSafeTrnSessionPage : IPersistantObject
    {
        public long intId { get; set; }
       // public int count { get; set; }
        public int intMstUserid { get; set; }
        public int intMstPageId { get; set; }
        public string txtTrnSessionId { get; set; }
        public int intCorrectResponsePageId { get; set; }
        public int intMaxScore { get; set; }
        public int intUserScore { get; set; }
        public DateTime dtVisitedAt { get; set; }

        public iSafeTrnSessionPage()
        { 
          dtVisitedAt = DateTime.Now;
        }

        public override string ToString()
        {
            return "iSafeTrnSessionPage: " + intId + "," + intMstPageId + "," + txtTrnSessionId + "," + intCorrectResponsePageId + "," + intMaxScore + "," + intUserScore+","+ dtVisitedAt+","+ intMstUserid ;
        }
    }
}