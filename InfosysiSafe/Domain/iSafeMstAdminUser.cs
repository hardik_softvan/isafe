﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeMstAdminUser : IPersistantObject
    {
        public long intId { get; set; }
        public string txtLoginName { get; set; }
        public string txtUserName { get; set; }
        public string txtPassword { get; set; }
        public string txtRoleid { get; set; }
        public DateTime  dtCreatedOn { get; set; }
        
        public iSafeMstAdminUser()
        {
            dtCreatedOn = DateTime.Now;
        }

        public override string ToString()
        {
            return "iSafeMstAdminUser: " + intId + ", " + txtLoginName + ", " + txtUserName + ", " + txtPassword+","+ txtRoleid+","+ dtCreatedOn;
        }
    }
}