﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.Domain
{
    public class iSafeMstQuestion : IPersistantObject
    {
        public long intId { get; set; }
        public int intMstModuleId { get; set; }
        public string txtQuestion { get; set; }
        public string txtOpt1 { get; set; }
        public string txtOpt2 { get; set; }
        public string txtOpt3 { get; set; }
        public string txtOpt4 { get; set; }
        public int intCorrectOptNo { get; set; }
        public DateTime dtCreatedOn { get; set; }
        
        public iSafeMstQuestion()
        {
            dtCreatedOn = DateTime.Now;
            // txtEmailId = Guid.NewGuid().ToString();
        }

        public override string ToString()
        {
            return "[{\"id\":\"" + intId + "\",\"intMstModuleId\":\"" + intMstModuleId + "\", \"txtQuestion\":\"" + txtQuestion + "\", \"txtOpt1\":\"" + txtOpt1 + "\", \"txtOpt2\":\"" + txtOpt2 + "\",\"txtOpt3\":\"" + txtOpt3 + "\",\"txtOpt4\":\"" + txtOpt4 + "\",\"intCorrectOptNo\":\"" + intCorrectOptNo + "\", \"dtCreatedOn\":\"" + dtCreatedOn + "\" }]";
        }
    }
}