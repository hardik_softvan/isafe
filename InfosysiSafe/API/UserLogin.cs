﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.API
{
    public class UserLogin : IHandler
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public UserLogin(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("AdminLogin API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                string loginName = (string)input["loginName"];
                int RoleId = (int)input["RoleId"];
                Logger.Info("AdminLogin Request,loginName => " + loginName);
                    // Get user
                    iSafeMstUser user = (iSafeMstUser)Store.Get("iSafeMstUser", "loginName", loginName);
                if (user != null)
                {
                    iSafeTrnSession session = new iSafeTrnSession();
                    session.txtSessionId = Guid.NewGuid().ToString("N");
                    session.intMstUserid = Convert.ToInt32(user.intId);
                    session.intState = SessionState.Active;
                    Store.Create("iSafeTrnSession", session);
                    iSafeTrnSessionPage userPageDetails = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", user.intId);
                    if (userPageDetails == null)
                    {
                        iSafeTrnSessionPage userPageCreate = new iSafeTrnSessionPage();
                        userPageCreate.txtTrnSessionId = Guid.NewGuid().ToString("N");
                        userPageCreate.intMstPageId = 1;
                        userPageCreate.intMstUserid = Convert.ToInt32(user.intId);
                        Store.Create("iSafeTrnSessionPage", userPageCreate);
                        output.Add("intUserId", user.intId);
                        output.Add("txtName", user.txtName);
                        output.Add("EmployeeId", user.txtEmployeeId);
                        output.Add("RoleId", user.intRoleId);
                    }
                    else
                    {
                        output.Add("intUserId", user.intId);
                        output.Add("txtName", user.txtName);
                        output.Add("EmployeeId", user.txtEmployeeId);
                        output.Add("RoleId", user.intRoleId);
                    }
                }
                else
                {
                    iSafeMstUser MstUser = new iSafeMstUser();
                    MstUser.txtName = loginName;
                    MstUser.intRoleId = RoleId;
                    MstUser.txtEmailId = "";//Email Id 
                    MstUser.txtEmployeeId = "1";
                    MstUser.intMstEmployeeTypeId = 1;
                    MstUser.intMstCompanyId = 1;
                    MstUser.intPrivacyStatus = 0;
                    Store.Create("iSafeMstUser", MstUser);
                    iSafeMstUser userlogin = (iSafeMstUser)Store.Get("iSafeMstUser", "loginName", loginName);
                    output.Add("intUserId", userlogin.intId);
                    output.Add("txtName", userlogin.txtName);
                    output.Add("EmployeeId", userlogin.txtEmployeeId);
                    output.Add("RoleId", userlogin.intRoleId);
                    iSafeTrnSession session = new iSafeTrnSession();
                    session.txtSessionId = Guid.NewGuid().ToString("N");
                    session.intMstUserid = Convert.ToInt32(userlogin.intId);
                    session.intState = SessionState.Active;
                    Store.Create("iSafeTrnSession", session);
                    iSafeTrnSessionPage userPageCreate = new iSafeTrnSessionPage();
                    userPageCreate.txtTrnSessionId = Guid.NewGuid().ToString("N");
                    userPageCreate.intMstPageId = 1;
                    userPageCreate.intMstUserid = Convert.ToInt32(userlogin.intId);
                    Store.Create("iSafeTrnSessionPage", userPageCreate);
                }
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AdminLogin Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("UserLogin API End");
            return output;
        }
    }
}