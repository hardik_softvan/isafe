﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace InfosysiSafe.API
{
    public class AdminUserLogin : IHandler
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public AdminUserLogin(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("AdminLogin API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                string loginName = (string)input["loginName"];
                Logger.Info("AdminLogin Request,loginName => " + loginName);
                string password = (string)input["password"];
                // Get user
                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter.Add("Name", "ByAdminLogin");
                filter.Add("LoginName ", loginName);
                filter.Add("Password ", password);
                List<IPersistantObject> Admin = Global.Store.Find("iSafeMstAdminUser", filter);
                List<iSafeMstAdminUser> AdimnUser = new List<iSafeMstAdminUser>();
                iSafeMstAdminUser user = (iSafeMstAdminUser)Admin[0];
                // Prepare output
                output.Add("Id", user.intId);
                output.Add("LoginName", user.txtUserName);
                output.Add("RoleId", user.txtRoleid);
                output.Add("Roles", user.txtRoleid);
                output.Add("Password", user.txtPassword);
                //Start a session
                iSafeTrnAdminSessionPage session = new iSafeTrnAdminSessionPage();
                session.txtTrnSessionId = Guid.NewGuid().ToString("N");
                session.intMstAdminId = Convert.ToInt32(user.intId);
                Store.Create("iSafeTrnAdminSessionPage", session);
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AdminLogin Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("AdminUserLogin API End");
            return output;
        }

    }
}