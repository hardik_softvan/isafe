﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.API
{
    public class AdminRightsControl : IHandler
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public AdminRightsControl(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("AdminLogin API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                string Name = (string)input["Name"];
                string Password = (string)input["Password"];
                string Emailid = (string)input["Emailid"];
                string Roles = (string)input["Roles"];

                iSafeMstAdminUser Admin = new iSafeMstAdminUser();
                Admin.txtLoginName = Emailid;
                Admin.txtUserName = Name;
                Admin.txtRoleid = Roles;
                Admin.txtPassword = Password;
                Store.Create("iSafeMstAdminUser", Admin);
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AdminLogin Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("AdminRightsControl API End");
            return output;
        }
    }
}