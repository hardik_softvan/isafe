﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;

namespace InfosysiSafe.API
{
    public class CertificateApi
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }


        string Template;
        string CertificatePath;

        public CertificateApi(IPersistantStore store, ILogger logger, string template, string certificatePath)
        {
            Store = store;
            Logger = logger;

            Template = template;
            CertificatePath = certificatePath;
        }
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("CertificateApi API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();

            try
            {
                string userid = (string)input["intMstUserId"];
                string username = (string)input["username"];
                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter.Add("Name", "");
                filter.Add("intMstUserid", Convert.ToInt32(userid));
                List<IPersistantObject> certificateRequests = Store.Find("iSafeTrnCertificate", filter);
                if (certificateRequests.Count > 0)
                {
                    iSafeTrnCertificate certificateRequest = new iSafeTrnCertificate();
                    iSafeMstUser user = (iSafeMstUser)Store.Get("iSafeMstUser", "getEMPid", userid);
                    Bitmap bitmap = (Bitmap)Image.FromFile(Template);
                    // Fill in the details
                    Font fontName = new Font("Arial", 13);
                    Font fontOthers = new Font("Arial", 12);
                    Graphics graphicsObj = Graphics.FromImage(bitmap);
                    RectangleF nameRectangle = new RectangleF(370, 150, 0, 0); ;
                    StringFormat stringFormat = new StringFormat();
                    stringFormat.Alignment = StringAlignment.Near;
                    graphicsObj.DrawString(username, fontName, Brushes.Black, nameRectangle, stringFormat);
                    RectangleF dateRectangle = new RectangleF(370, 300, 0, 0);
                    graphicsObj.DrawString(String.Format("{0:dd/MM/yyyy}", DateTime.Now), fontOthers, Brushes.Black, dateRectangle, stringFormat);
                    RectangleF datePercentage = new RectangleF(100, 200, 0, 0);
                    string certificateFile = CertificatePath + "\\" + userid + ".png";
                    if (File.Exists(certificateFile)) File.Delete(certificateFile);
                    bitmap.Save(certificateFile, System.Drawing.Imaging.ImageFormat.Png);
                    // Store with version name for administrators to download
                    string certificateVersionFile = CertificatePath + "\\" + userid + ".png";
                    File.Copy(certificateFile, certificateVersionFile, true);
                }
                else
                {
                    iSafeTrnCertificate certificateRequest = new iSafeTrnCertificate();
                    iSafeMstUser user = (iSafeMstUser)Store.Get("iSafeMstUser", "getEMPid", userid);
                    iSafeTrnCertificate iSafeTrnCertificateDomain = new iSafeTrnCertificate();
                    iSafeTrnCertificateDomain.intMstUserid = Convert.ToInt32(userid);
                    iSafeTrnCertificateDomain.txtCertificateUrl = userid + ".png";
                    Store.Create("iSafeTrnCertificate", iSafeTrnCertificateDomain);
                    Bitmap bitmap = (Bitmap)Image.FromFile(Template);
                    Font fontName = new Font("Arial", 13);
                    Font fontOthers = new Font("Arial", 12);
                    Graphics graphicsObj = Graphics.FromImage(bitmap);
                    RectangleF nameRectangle = new RectangleF(370, 150, 0, 0);
                    StringFormat stringFormat = new StringFormat();
                    stringFormat.Alignment = StringAlignment.Near;
                    graphicsObj.DrawString(username, fontName, Brushes.Black, nameRectangle, stringFormat);
                    RectangleF dateRectangle = new RectangleF(370, 300, 0, 0);
                    graphicsObj.DrawString(String.Format("{0:dd/MM/yyyy}", DateTime.Now), fontOthers, Brushes.Black, dateRectangle, stringFormat);
                    RectangleF datePercentage = new RectangleF(100, 200, 0, 0);
                    string certificateFile = CertificatePath + "\\" + userid + ".png";
                    if (File.Exists(certificateFile)) File.Delete(certificateFile);
                    bitmap.Save(certificateFile, System.Drawing.Imaging.ImageFormat.Png);
                    string certificateVersionFile = CertificatePath + "\\" + userid + ".png";
                    File.Copy(certificateFile, certificateVersionFile, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("Certificate Request", ex);
            }

            Logger.Debug("CertificateApi API End");
            return output;
        }
    }
}