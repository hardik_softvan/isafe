﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.API
{
    public class AssessmentApi
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public AssessmentApi(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("AssessmentApi API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                // Start a session
                var flgresult = (string)input["flgresult"];
                int intScore = (int)input["intScore"] ; 
                string userid = (string)input["userid"];
                iSafeTrnAssessment iSafeTrnAssessmentDomain = new iSafeTrnAssessment();
                iSafeTrnAssessmentDomain.intMstModuleId = 1;
                iSafeTrnAssessmentDomain.intMstUserId = Convert.ToInt32(userid);
                iSafeTrnAssessmentDomain.intScore = Convert.ToInt32(intScore.ToString());
                iSafeTrnAssessmentDomain.flgResult = Convert.ToBoolean(flgresult);
                iSafeTrnAssessmentDomain.dtAssessmentStartAt = DateTime.Now;
                iSafeTrnAssessmentDomain.dtAssessmentCompletedAt = DateTime.Now;
                Store.Create("iSafeTrnAssessment", iSafeTrnAssessmentDomain);
                iSafeTrnAssessment user = (iSafeTrnAssessment)Store.Get("iSafeTrnAssessment", "loginName", iSafeTrnAssessmentDomain.intMstUserId);
                output.Add("userid", user.intId);
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AdminLogin Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("AssessmentApi API End");
            return output;
        }
    }
}