﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace InfosysiSafe.API
{
    public class Topic : IHandler
    {

        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public Topic(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
        StringBuilder sb = new StringBuilder();

        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("Topic API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                int intMstUserid = Convert.ToInt32((string)input["intMstUserid"]);
                //int Pageid = Convert.ToInt32((string)input["Pageid"]);
                int Moduleid = Convert.ToInt32((string)input["Topicid"]);
                Dictionary<string, object> filter2 = new Dictionary<string, object>();
                filter2.Add("Name", "ByPledge");
                filter2.Add("UserId", Convert.ToInt32(intMstUserid));
                List<IPersistantObject> Pledge = Global.Store.Find("iSafeTrnPledge", filter2);
                if (Pledge.Count > 0)
                {
                    //***************After Pledge Completion Start********************//
                    iSafeMstTopic userPageSequence = (iSafeMstTopic)Store.Get("iSafeMstTopic", "Topicid", Moduleid);
                    Dictionary<string, object> dt = new Dictionary<string, object>();
                    dt.Add("Name", "ByAll");
                    dt.Add("@Model", Moduleid);
                    string Complete = "<i class='fa fa-check check1'></i>";
                    List<IPersistantObject> Topic = Global.Store.Find("iSafeMstTopic", dt);
                    iSafeTrnSessionPage userPageDetails = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", intMstUserid);
                    iSafeMstPage userPage = (iSafeMstPage)Store.Get("iSafeMstPage", "lastPageid", userPageDetails.intMstPageId);

                    for (int c = 0; c < Topic.Count; c++)
                    {
                        iSafeMstTopic Topics = (iSafeMstTopic)Topic[c];
                        Dictionary<string, object> FirstTopic = new Dictionary<string, object>();
                        FirstTopic.Add("Name", "ByFirstTopic");
                        FirstTopic.Add("Topicid", Topics.intId);
                        List<IPersistantObject> FirstTopicid = Global.Store.Find("iSafeMstPage", FirstTopic);
                        List<iSafeMstPage> FirstTop = new List<iSafeMstPage>();
                        iSafeMstPage Topid = (iSafeMstPage)FirstTopicid[0];
                        //*******************Module 1 Start***********************//
                        if (Moduleid == 1)
                        {
                            if (Topics.intSequenceNo == 1)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                      Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Introduction.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 2)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{

                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                   Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "passwordpolicy.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "passwordpolicy.png");
                                //    }

                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "passwordpolicy.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 3)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "privileged_access.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "privileged_access.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "privileged_access.png");
                                //}
                            }


                        }
                        //*******************Module 1 End***********************//
                        //*******************Module 2 Start*********************//
                        else if (Moduleid == 2)
                        {
                            if (Topics.intSequenceNo == 1)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Introduction.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 2)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "CIA.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "CIA.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "CIA.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 3)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "lifecycle_management .png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "lifecycle_management .png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "lifecycle_management .png");
                                //}
                            }
                            if (Topics.intSequenceNo == 4)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Infomation_leakage.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Infomation_leakage.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Infomation_leakage.png");
                                //}
                            }
                        }
                        //*******************Module 2 End***********************//
                        //*******************Module 3 Start*********************//
                        else if (Moduleid == 3)
                        {
                            if (Topics.intSequenceNo == 1)
                            {
                                //    if (Topics.intId >= userPage.intMstTopicid)
                                //    {
                                //        if (Topics.intId == userPage.intMstTopicid)
                                //        {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Introduction.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 2)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{

                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Internet_usage_topic.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Internet_usage_topic.png");
                                //    }

                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Internet_usage_topic.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 3)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Email_usage.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Email_usage.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Email_usage.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 4)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Desktop_laptop.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Desktop_laptop.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Desktop_laptop.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 5)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "protection_information.png");
                                //        }
                                //        else
                                //        {
                                //            Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "protection_information.png");
                                //        }
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "protection_information.png");
                                //    }
                                //}
                            }
                            //*******************Module 3 End***********************//
                           
                        }
                        //*******************Module 4 Start*********************//
                        else if (Moduleid == 4)
                        {
                            if (Topics.intSequenceNo == 1)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Introduction.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 2)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Infomation_security_incident.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Infomation_security_incident.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Infomation_security_incident.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 3)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Infomation_security_Policy.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Infomation_security_Policy.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Infomation_security_Policy.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 4)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "SLDC_Framework.png");
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "SLDC_Framework.png");
                                //    }
                                //}
                                //else
                                //{
                                //    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "SLDC_Framework.png");
                                //}
                            }
                            if (Topics.intSequenceNo == 5)
                            {
                                //if (Topics.intId >= userPage.intMstTopicid)
                                //{
                                //    if (Topics.intId == userPage.intMstTopicid)
                                //    {
                                Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Rules_resposibility.png");
                                //        }
                                //        else
                                //        {
                                //            Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Rules_resposibility.png");
                                //        }
                                //    }
                                //    else
                                //    {
                                //        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Rules_resposibility.png");
                                //    }
                            }
                        }
                        //*******************Module 4 End***********************//
                       
                        //***************After Pledge Completion End********************//


                    }
                    output.Add("Topic", sb.ToString());
                }
                else
                {
                    //***************Before Pledge Completion Start********************//
                    iSafeMstTopic userPageSequence = (iSafeMstTopic)Store.Get("iSafeMstTopic", "Topicid", Moduleid);
                    Dictionary<string, object> dt = new Dictionary<string, object>();
                    dt.Add("Name", "ByAll");
                    dt.Add("@Model", Moduleid);
                    string Complete = "<i class='fa fa-check check1'></i>";
                    List<IPersistantObject> Topic = Global.Store.Find("iSafeMstTopic", dt);
                    iSafeTrnSessionPage userPageDetails = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", intMstUserid);
                    iSafeMstPage userPage = (iSafeMstPage)Store.Get("iSafeMstPage", "lastPageid", userPageDetails.intMstPageId);

                    for (int c = 0; c < Topic.Count; c++)
                    {
                        iSafeMstTopic Topics = (iSafeMstTopic)Topic[c];
                        Dictionary<string, object> FirstTopic = new Dictionary<string, object>();
                        FirstTopic.Add("Name", "ByFirstTopic");
                        FirstTopic.Add("Topicid", Topics.intId);
                        List<IPersistantObject> FirstTopicid = Global.Store.Find("iSafeMstPage", FirstTopic);
                        List<iSafeMstPage> FirstTop = new List<iSafeMstPage>();
                        iSafeMstPage Topid = (iSafeMstPage)FirstTopicid[0];
                        //*******************Module 1 Start***********************//
                        if (Moduleid == 1)
                        {
                            if (Topics.intSequenceNo == 1)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Introduction.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Introduction.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                }
                            }
                            if (Topics.intSequenceNo == 2)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {

                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "passwordpolicy.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "passwordpolicy.png");
                                    }

                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "passwordpolicy.png");
                                }
                            }
                            if (Topics.intSequenceNo == 3)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "privileged_access.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "privileged_access.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "privileged_access.png");
                                }
                            }


                        }
                        //*******************Module 1 End***********************//
                        //*******************Module 2 Start*********************//
                        else if (Moduleid == 2)
                        {
                            if (Topics.intSequenceNo == 1)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Introduction.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Introduction.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                }
                            }
                            if (Topics.intSequenceNo == 2)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "CIA.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "CIA.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "CIA.png");
                                }
                            }
                            if (Topics.intSequenceNo == 3)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "lifecycle_management .png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "lifecycle_management .png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "lifecycle_management .png");
                                }
                            }
                            if (Topics.intSequenceNo == 4)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Infomation_leakage.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Infomation_leakage.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Infomation_leakage.png");
                                }
                            }
                        }
                        //*******************Module 2 End***********************//
                        //*******************Module 3 Start*********************//
                        else if (Moduleid == 3)
                        {
                            if (Topics.intSequenceNo == 1)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Introduction.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Introduction.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                }
                            }
                            if (Topics.intSequenceNo == 2)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {

                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Internet_usage_topic.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Internet_usage_topic.png");
                                    }

                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Internet_usage_topic.png");
                                }
                            }
                            if (Topics.intSequenceNo == 3)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Email_usage.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Email_usage.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Email_usage.png");
                                }
                            }
                            if (Topics.intSequenceNo == 4)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Desktop_laptop.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Desktop_laptop.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Desktop_laptop.png");
                                }
                            }
                            if (Topics.intSequenceNo == 5)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "protection_information.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "protection_information.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "protection_information.png");
                                }
                            }
                        }
                        //*******************Module 3 End***********************//
                        //*******************Module 4 Start*********************//
                        else if (Moduleid == 4)
                        {
                            if (Topics.intSequenceNo == 1)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Introduction.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Introduction.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Introduction.png");
                                }
                            }
                            if (Topics.intSequenceNo == 2)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Infomation_security_incident.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Infomation_security_incident.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Infomation_security_incident.png");
                                }
                            }
                            if (Topics.intSequenceNo == 3)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Infomation_security_Policy.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Infomation_security_Policy.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Infomation_security_Policy.png");
                                }
                            }
                            if (Topics.intSequenceNo == 4)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "SLDC_Framework.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "SLDC_Framework.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "SLDC_Framework.png");
                                }
                            }
                            if (Topics.intSequenceNo == 5)
                            {
                                if (Topics.intId >= userPage.intMstTopicid)
                                {
                                    if (Topics.intId == userPage.intMstTopicid)
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "", "flash-button", c, "Rules_resposibility.png");
                                    }
                                    else
                                    {
                                        Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, "", "disabled", "", c, "Rules_resposibility.png");
                                    }
                                }
                                else
                                {
                                    Module(Topics.txtName, Convert.ToInt32(Topid.intSequenceNo), Moduleid, Complete, "", "", c, "Rules_resposibility.png");
                                }
                            }
                        }
                        //*******************Module 4 End***********************//
                    }
                    output.Add("Topic", sb.ToString());
                    //***************Before Pledge Completion End********************//
                }
            }

            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AdminLogin Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("Topic API End");
            return output;
        }
        public string Module(string TopicName,int Topicid,int Moduleid,string Process,string Access,string Animation,int id,string img)
        {
            int cnt = id+1;
            //if (Moduleid == 3 || Moduleid == 4 || Moduleid == 2)
            //{
                if(cnt==1 || cnt==3 || cnt == 5)
                { 
                sb.Append("<div class='col-sm-2'></div><div class='col-sm-4 " + Access + "'>");
                sb.Append("<a  href='javascript:void(0)' id='Antopic" + cnt + "' class='topic-selection " + Animation + "' onclick='topicSelection(" + Topicid + "," + Moduleid + "," + cnt + ")'> <img src = '../images/" + img + "' alt=''>" + Process + "</a>");
                sb.Append(" <p class='mt-4 topics'>" + TopicName + "</p>");
                sb.Append("</div>");
                }
                if (cnt == 2 || cnt == 4)
                {
                    sb.Append("<div class='col-sm-4 " + Access + "'>");
                    sb.Append("<a  href='javascript:void(0)' id='Antopic" + cnt + "' class='topic-selection " + Animation + "' onclick='topicSelection(" + Topicid + "," + Moduleid + "," + cnt + ")'> <img src = '../images/" + img + "' alt=''>" + Process + "</a>");
                    sb.Append(" <p class='mt-4 topics'>" + TopicName + "</p>");
                    sb.Append("</div><div class='col-sm-2'></div>");
                }
            //}
            //else
            //{
            //    sb.Append("<div class='col-sm-4 " + Access + "'>");
            //    sb.Append("<a  href='javascript:void(0)' id='Antopic" + cnt + "' class='topic-selection " + Animation + "' onclick='topicSelection(" + Topicid + "," + Moduleid + "," + cnt + ")'> <img src = '../images/" + img + "' alt=''>" + Process + "</a>");
            //    sb.Append(" <p class='mt-4 topics'>" + TopicName + "</p>");
            //    sb.Append("</div>");
            //}
           
            return sb.ToString();
        }

    }
}