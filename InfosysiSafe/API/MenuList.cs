﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace InfosysiSafe.API
{
    public class MenuList : IHandler
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public MenuList(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("AdminLogin API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                int intMstUserid = Convert.ToInt32((string)input["intMstUserid"]);
                Dictionary<string, object> filter2 = new Dictionary<string, object>();
                filter2.Add("Name", "ByPledge");
                filter2.Add("UserId", Convert.ToInt32(intMstUserid));
                List<IPersistantObject> Pledge = Global.Store.Find("iSafeTrnPledge", filter2);
                if (Pledge.Count > 0)
                {
                    //***************After Pledge Completion Start********************//
                    Dictionary<string, object> filter = new Dictionary<string, object>();
                    filter.Add("Name", "ByAll");
                    List<IPersistantObject> MenuList = Global.Store.Find("iSafeMstModule", filter);
                    List<iSafeMstModule> Model = new List<iSafeMstModule>();
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < MenuList.Count; i++)
                    {
                        iSafeMstModule MenuLists = (iSafeMstModule)MenuList[i];
                        iSafeTrnSessionPage userPageDetails = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", intMstUserid);
                        iSafeMstPage userPageSequence = (iSafeMstPage)Store.Get("iSafeMstPage", "lastPageid", userPageDetails.intMstPageId);
                        Dictionary<string, object> Topicfilter = new Dictionary<string, object>();
                        Topicfilter.Add("Name", "ByUserTopic");
                        Topicfilter.Add("Topicid", userPageSequence.intId);
                        List<IPersistantObject> Module1 = Global.Store.Find("iSafeMstModule", Topicfilter);
                        List<iSafeMstModule> TopicList = new List<iSafeMstModule>();
                        iSafeMstModule Topic = (iSafeMstModule)Module1[0];
                        iSafeMstPage Pagelastid = (iSafeMstPage)Store.Get("iSafeMstPage", "Pagelastid", MenuLists.intId);
                        iSafeMstPage Pagetopfirstid = (iSafeMstPage)Store.Get("iSafeMstPage", "Pagetopfirstid", MenuLists.intId);
                       // if (Topic.intId == MenuLists.intId)
                      //  {
                        // sb.Append("<li id=menuid" + MenuLists.intId + " class='module resp-tab-item'  onclick='menu(\"" + MenuLists.intId + "\")'><div class='module-ico'><img src='../images/module-icon.png'></div><div class='module-text'><h4>" + MenuLists.txtName + "</h4><span class='completed'>Current</span></div></li>");
                           sb.Append("<li id=menuid" + MenuLists.intId + " class='module resp-tab-item'  onclick='menu(\"" + MenuLists.intId + "\")'><div class='module-ico'><img src='../images/module-icon.png'></div><div class='module-text'><h4>" + MenuLists.txtName + "</h4><span class='completed'>Completed</span></div></li>");
                        //}
                        //else
                        //{
                        //    if (Pagetopfirstid != null || Pagelastid != null)
                        //    {
                        //        if (Topic.intId > MenuLists.intId)
                        //        {
                        //            sb.Append("<li id=menuid" + MenuLists.intId + " class='module resp-tab-item'  onclick='menu(\"" + MenuLists.intId + "\")'><div class='module-ico'><img src='../images/module-icon.png'></div><div class='module-text'><h4>" + MenuLists.txtName + "</h4><span class='completed'>Completed</span></div></li>");
                        //        }
                        //        else
                        //        {
                        //            sb.Append("<li id=menuid" + MenuLists.intId + " class='module resp-tab-item disabled'  onclick='menu(\"" + MenuLists.intId + "\")'><div class='module-ico'><img src='../images/module-icon.png'></div><div class='module-text'><h4>" + MenuLists.txtName + "</h4><span class='pending'>Pending</span></div></li>");
                        //        }
                        //    }
                        //}
                        Model.Add(MenuLists);
                    }
                    output.Add("MenuList", sb);
                    StringBuilder sb1 = new StringBuilder();
                    for (int c = 0; c < MenuList.Count; c++)
                    {
                        iSafeMstModule Menu = (iSafeMstModule)MenuList[c];
                        iSafeTrnSessionPage userPageDetails = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", intMstUserid);
                        iSafeMstPage userPage = (iSafeMstPage)Store.Get("iSafeMstPage", "lastPageid", userPageDetails.intMstPageId);
                        iSafeMstPage Pagelastid = (iSafeMstPage)Store.Get("iSafeMstPage", "Pagelastid", Menu.intId);
                        iSafeMstPage TotalLastPage = (iSafeMstPage)Store.Get("iSafeMstPage", "TotalLastPage", Menu.intId);
                        Dictionary<string, object> dt = new Dictionary<string, object>();
                        dt.Add("Name", "ByAll");
                        dt.Add("@Model", Menu.intId);
                        List<IPersistantObject> user = Global.Store.Find("iSafeMstTopic", dt);
                        List<iSafeMstTopic> userPageSequences = new List<iSafeMstTopic>();
                        Dictionary<string, object> CurrentPageidfilter = new Dictionary<string, object>();
                        CurrentPageidfilter.Add("Name", "ByCurrentTopic");
                        CurrentPageidfilter.Add("intMstPageId", userPageDetails.intMstPageId);
                        List<IPersistantObject> userpage = Global.Store.Find("iSafeMstPage", CurrentPageidfilter);
                        List<iSafeMstPage> userpages = new List<iSafeMstPage>();
                        iSafeMstPage UserCurrentpage = (iSafeMstPage)userpage[0];
                        Dictionary<string, object> TopicCnt = new Dictionary<string, object>();
                        TopicCnt.Add("Name", "ModelCount");
                        TopicCnt.Add("Modelid", Menu.intId);
                        List<IPersistantObject> Topic = Global.Store.Find("iSafeMstPage", TopicCnt);
                        sb1.Append("<div class='infs-module-list taskwrap'  id='task-" + Menu.intId + "' >");
                        sb1.Append("<ul>");
                        for (int i = 0; i < user.Count; i++)
                        {
                            iSafeMstTopic userPageSequence = (iSafeMstTopic)user[i];
                            Dictionary<string, object> FirstTopic = new Dictionary<string, object>();
                            FirstTopic.Add("Name", "ByFirstTopic");
                            FirstTopic.Add("Topicid", userPageSequence.intId);
                            List<IPersistantObject> FirstTopicid = Global.Store.Find("iSafeMstPage", FirstTopic);
                            List<iSafeMstPage> FirstTop = new List<iSafeMstPage>();
                            iSafeMstPage Topid = (iSafeMstPage)FirstTopicid[0];
                           // if (UserCurrentpage.intMstTopicid > userPageSequence.intId)
                           // {
                                sb1.Append("<li><a href='javascript:void(0)' onclick='topic(\"" + Topid.txtType + "\"," + Topid.intSequenceNo + "," + Menu.intId + ",\"" + Topid.txtName + "\",\"" + Menu.txtName + "\",\"" + Topic.Count + "\");closeNav()' ><div class='row'><div class='col-7'><div class='module-ico'><img src = '../images/complete-module.png' alt='' title=''></div><h4>" + userPageSequence.txtName + "</h4></div><div class='col-5'>");
                            //}
                            //else
                            //{
                            //    if (userPage.intId == TotalLastPage.intId)
                            //    {
                            //        sb1.Append("<li><a href='javascript:void(0)' onclick='topic(\"" + Topid.txtType + "\"," + Topid.intSequenceNo + "," + Menu.intId + ",\"" + Topid.txtName + "\",\"" + Menu.txtName + "\",\"" + Topic.Count + "\");closeNav()' ><div class='row'><div class='col-7'><div class='module-ico'><img src = '../images/complete-module.png' alt='' title=''></div><h4>" + userPageSequence.txtName + "</h4></div><div class='col-5'>");
                            //    }
                            //    else
                            //    {
                            //        if (UserCurrentpage.intMstTopicid == userPageSequence.intId)
                            //        {
                            //            sb1.Append("<li><a href='javascript:void(0)'   onclick='topic(\"" + Topid.txtType + "\"," + Topid.intSequenceNo + "," + Menu.intId + ",\"" + Topid.txtName + "\",\"" + Menu.txtName + "\",\"" + Topic.Count + "\");closeNav()' ><div class='row'><div class='col-7'><div class='module-ico'><img src = '../images/complete-module.png' alt='' title=''></div><h4>" + userPageSequence.txtName + "</h4></div><div class='col-5'>");
                            //        }
                            //        else
                            //        {
                            //            sb1.Append("<li  class='disabled'><a href='javascript:void(0)'  onclick='topic(\"" + Topid.txtType + "\"," + Topid.intSequenceNo + "," + Menu.intId + ",\"" + Topid.txtName + "\",\"" + Menu.txtName + "\",\"" + Topic.Count + "\");closeNav()' ><div class='row'><div class='col-7'><div class='module-ico'><img src = '../images/complete-module.png' alt='' title=''></div><h4>" + userPageSequence.txtName + "</h4></div><div class='col-5'>");
                            //        }

                            //    }
                            //}
                          // if (UserCurrentpage.intMstTopicid > userPageSequence.intId)
                            //{
                                sb1.Append("<span class='completed'>Completed</span></div></div></a></li>");
                            //}
                            //else
                            //{
                            //    if (userPage.intId == TotalLastPage.intId)
                            //    {
                            //        sb1.Append("<span class='completed'>Completed</span></div></div></a></li>");
                            //    }
                            //    else
                            //    {
                            //        if (UserCurrentpage.intMstTopicid == userPageSequence.intId)
                            //        {
                            //            sb1.Append("<span class='pending'>Current</span></div></div></a></li>");
                            //        }
                            //        else
                            //        {
                            //            sb1.Append("<span class='pending'>Pending</span></div></div></a></li>");
                            //        }


                            //    }

                            //}
                            userPageSequences.Add(userPageSequence);
                        }
                        sb1.Append("</ul>");
                        sb1.Append("</div>");
                    }
                    output.Add("Submenu", sb1);
                    //***************After Pledge Completion End********************//



                }
                else
                {
                    //***************Before Pledge Completion Start********************//
                    Dictionary<string, object> filter = new Dictionary<string, object>();
                    filter.Add("Name", "ByAll");
                    List<IPersistantObject> MenuList = Global.Store.Find("iSafeMstModule", filter);
                    List<iSafeMstModule> Model = new List<iSafeMstModule>();
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < MenuList.Count; i++)
                    {
                        iSafeMstModule MenuLists = (iSafeMstModule)MenuList[i];
                        iSafeTrnSessionPage userPageDetails = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", intMstUserid);
                        iSafeMstPage userPageSequence = (iSafeMstPage)Store.Get("iSafeMstPage", "lastPageid", userPageDetails.intMstPageId);
                        Dictionary<string, object> Topicfilter = new Dictionary<string, object>();
                        Topicfilter.Add("Name", "ByUserTopic");
                        Topicfilter.Add("Topicid", userPageSequence.intId);
                        List<IPersistantObject> Module1 = Global.Store.Find("iSafeMstModule", Topicfilter);
                        List<iSafeMstModule> TopicList = new List<iSafeMstModule>();
                        iSafeMstModule Topic = (iSafeMstModule)Module1[0];
                        iSafeMstPage Pagelastid = (iSafeMstPage)Store.Get("iSafeMstPage", "Pagelastid", MenuLists.intId);
                        iSafeMstPage Pagetopfirstid = (iSafeMstPage)Store.Get("iSafeMstPage", "Pagetopfirstid", MenuLists.intId);
                        if (Topic.intId == MenuLists.intId)
                        {
                            sb.Append("<li id=menuid" + MenuLists.intId + " class='module resp-tab-item'  onclick='menu(\"" + MenuLists.intId + "\")'><div class='module-ico'><img src='../images/module-icon.png'></div><div class='module-text'><h4>" + MenuLists.txtName + "</h4><span class='completed'>Current</span></div></li>");
                        }
                        else
                        {
                            if (Pagetopfirstid != null || Pagelastid != null)
                            {
                                if (Topic.intId > MenuLists.intId)
                                {
                                    sb.Append("<li id=menuid" + MenuLists.intId + " class='module resp-tab-item'  onclick='menu(\"" + MenuLists.intId + "\")'><div class='module-ico'><img src='../images/module-icon.png'></div><div class='module-text'><h4>" + MenuLists.txtName + "</h4><span class='completed'>Completed</span></div></li>");
                                }
                                else
                                {
                                    sb.Append("<li id=menuid" + MenuLists.intId + " class='module resp-tab-item disabled'  onclick='menu(\"" + MenuLists.intId + "\")'><div class='module-ico'><img src='../images/module-icon.png'></div><div class='module-text'><h4>" + MenuLists.txtName + "</h4><span class='pending'>Pending</span></div></li>");
                                }
                            }
                        }
                        Model.Add(MenuLists);
                    }
                    output.Add("MenuList", sb);
                    StringBuilder sb1 = new StringBuilder();
                    for (int c = 0; c < MenuList.Count; c++)
                    {
                        iSafeMstModule Menu = (iSafeMstModule)MenuList[c];
                        iSafeTrnSessionPage userPageDetails = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", intMstUserid);
                        iSafeMstPage userPage = (iSafeMstPage)Store.Get("iSafeMstPage", "lastPageid", userPageDetails.intMstPageId);
                        iSafeMstPage Pagelastid = (iSafeMstPage)Store.Get("iSafeMstPage", "Pagelastid", Menu.intId);
                        iSafeMstPage TotalLastPage = (iSafeMstPage)Store.Get("iSafeMstPage", "TotalLastPage", Menu.intId);
                        Dictionary<string, object> dt = new Dictionary<string, object>();
                        dt.Add("Name", "ByAll");
                        dt.Add("@Model", Menu.intId);
                        List<IPersistantObject> user = Global.Store.Find("iSafeMstTopic", dt);
                        List<iSafeMstTopic> userPageSequences = new List<iSafeMstTopic>();
                        Dictionary<string, object> CurrentPageidfilter = new Dictionary<string, object>();
                        CurrentPageidfilter.Add("Name", "ByCurrentTopic");
                        CurrentPageidfilter.Add("intMstPageId", userPageDetails.intMstPageId);
                        List<IPersistantObject> userpage = Global.Store.Find("iSafeMstPage", CurrentPageidfilter);
                        List<iSafeMstPage> userpages = new List<iSafeMstPage>();
                        iSafeMstPage UserCurrentpage = (iSafeMstPage)userpage[0];
                        Dictionary<string, object> TopicCnt = new Dictionary<string, object>();
                        TopicCnt.Add("Name", "ModelCount");
                        TopicCnt.Add("Modelid", Menu.intId);
                        List<IPersistantObject> Topic = Global.Store.Find("iSafeMstPage", TopicCnt);
                        sb1.Append("<div class='infs-module-list taskwrap'  id='task-" + Menu.intId + "' >");
                        sb1.Append("<ul>");
                        for (int i = 0; i < user.Count; i++)
                        {
                            iSafeMstTopic userPageSequence = (iSafeMstTopic)user[i];
                            Dictionary<string, object> FirstTopic = new Dictionary<string, object>();
                            FirstTopic.Add("Name", "ByFirstTopic");
                            FirstTopic.Add("Topicid", userPageSequence.intId);
                            List<IPersistantObject> FirstTopicid = Global.Store.Find("iSafeMstPage", FirstTopic);
                            List<iSafeMstPage> FirstTop = new List<iSafeMstPage>();
                            iSafeMstPage Topid = (iSafeMstPage)FirstTopicid[0];
                            if (UserCurrentpage.intMstTopicid > userPageSequence.intId)
                            {
                                sb1.Append("<li><a href='javascript:void(0)' onclick='topic(\"" + Topid.txtType + "\"," + Topid.intSequenceNo + "," + Menu.intId + ",\"" + Topid.txtName + "\",\"" + Menu.txtName + "\",\"" + Topic.Count + "\");closeNav()' ><div class='row'><div class='col-7'><div class='module-ico'><img src = '../images/complete-module.png' alt='' title=''></div><h4>" + userPageSequence.txtName + "</h4></div><div class='col-5'>");
                            }
                            else
                            {
                                if (userPage.intId == TotalLastPage.intId)
                                {
                                    sb1.Append("<li><a href='javascript:void(0)' onclick='topic(\"" + Topid.txtType + "\"," + Topid.intSequenceNo + "," + Menu.intId + ",\"" + Topid.txtName + "\",\"" + Menu.txtName + "\",\"" + Topic.Count + "\");closeNav()' ><div class='row'><div class='col-7'><div class='module-ico'><img src = '../images/complete-module.png' alt='' title=''></div><h4>" + userPageSequence.txtName + "</h4></div><div class='col-5'>");
                                }
                                else
                                {
                                    if (UserCurrentpage.intMstTopicid == userPageSequence.intId)
                                    {
                                        sb1.Append("<li><a href='javascript:void(0)'   onclick='topic(\"" + Topid.txtType + "\"," + Topid.intSequenceNo + "," + Menu.intId + ",\"" + Topid.txtName + "\",\"" + Menu.txtName + "\",\"" + Topic.Count + "\");closeNav()' ><div class='row'><div class='col-7'><div class='module-ico'><img src = '../images/complete-module.png' alt='' title=''></div><h4>" + userPageSequence.txtName + "</h4></div><div class='col-5'>");
                                    }
                                    else
                                    {
                                        sb1.Append("<li  class='disabled'><a href='javascript:void(0)'  onclick='topic(\"" + Topid.txtType + "\"," + Topid.intSequenceNo + "," + Menu.intId + ",\"" + Topid.txtName + "\",\"" + Menu.txtName + "\",\"" + Topic.Count + "\");closeNav()' ><div class='row'><div class='col-7'><div class='module-ico'><img src = '../images/complete-module.png' alt='' title=''></div><h4>" + userPageSequence.txtName + "</h4></div><div class='col-5'>");
                                    }

                                }
                            }
                            if (UserCurrentpage.intMstTopicid > userPageSequence.intId)
                            {
                                sb1.Append("<span class='completed'>Completed</span></div></div></a></li>");
                            }
                            else
                            {
                                if (userPage.intId == TotalLastPage.intId)
                                {
                                    sb1.Append("<span class='completed'>Completed</span></div></div></a></li>");
                                }
                                else
                                {
                                    if (UserCurrentpage.intMstTopicid == userPageSequence.intId)
                                    {
                                        sb1.Append("<span class='pending'>Current</span></div></div></a></li>");
                                    }
                                    else
                                    {
                                        sb1.Append("<span class='pending'>Pending</span></div></div></a></li>");
                                    }


                                }

                            }
                            userPageSequences.Add(userPageSequence);
                        }
                        sb1.Append("</ul>");
                        sb1.Append("</div>");
                    }
                    output.Add("Submenu", sb1);
                    //***************Before Pledge Completion End********************//
                }


                
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AdminLogin Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("MenuList API End");
            return output;
        }
    }
}