﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.API
{
    public class Activity : IHandler
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public Activity(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("AdminLogin API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                int intMstUserid = Convert.ToInt32((string)input["intMstUserid"]);
                int Pageid = Convert.ToInt32((string)input["Pageid"]);
                int intUserScore = Convert.ToInt32((string)input["intUserScore"]);

                iSafeTrnSessionPage iSafeTrnSessionPageActivity = new iSafeTrnSessionPage();
                iSafeTrnSessionPageActivity.intMstPageId = Pageid;
                iSafeTrnSessionPageActivity.intMstUserid = intMstUserid;
                iSafeTrnSessionPageActivity.intMaxScore = 30;
                iSafeTrnSessionPageActivity.intUserScore = intUserScore;
                iSafeTrnSessionPageActivity.txtTrnSessionId = "1";
                Global.Store.Update(iSafeTrnSessionPageActivity);
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AdminLogin Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("Activity API End");
            return output;
        }
    }
}