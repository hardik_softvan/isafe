﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.API
{
    public class Index : IHandler
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }

        public Index(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
 
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("AdminLogin API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                int intMstUserid = Convert.ToInt32((string)input["intMstUserid"]);
                int Page = Convert.ToInt32((string)input["Page"]);
                int Pageid = Convert.ToInt32((string)input["Pageid"]);
                int CurrentPageid = Convert.ToInt32((string)input["CurrentPageid"]);
                int Model = Convert.ToInt32((string)input["Model"]);
               //-----------------------Topic Count Start----------------------//
                iSafeTrnSessionPage userPage = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", intMstUserid);
                iSafeMstPage userPageCnt = (iSafeMstPage)Store.Get("iSafeMstPage", "lastPageid", userPage.intMstPageId);
                Dictionary<string, object> PagesCountfilter = new Dictionary<string, object>();
                PagesCountfilter.Add("Name", "ByUserTopic");
                PagesCountfilter.Add("Topicid", userPageCnt.intId);
                List<IPersistantObject> PagesCount = Global.Store.Find("iSafeMstModule", PagesCountfilter);
                List<iSafeMstModule> PagesCnt = new List<iSafeMstModule>();
                iSafeMstModule PageCnt = (iSafeMstModule)PagesCount[0];
                Dictionary<string, object> TopicCnt = new Dictionary<string, object>();
                TopicCnt.Add("Name", "ModelCount");
                // TopicCnt.Add("Modelid", PageCnt.intId);
                if (Page == 0)
                {
                    TopicCnt.Add("Modelid", PageCnt.intId);
                }
                else
                {
                    TopicCnt.Add("Modelid", Model);
                }
                List<IPersistantObject> Topic = Global.Store.Find("iSafeMstPage", TopicCnt);
                output.Add("ModelCount", Topic.Count);
                //-----------------------Topic Count End----------------------//
                //-----------------------Page Load Start----------------------//
                if (Page == 0)
                {
                    iSafeMstUser userdetails = (iSafeMstUser)Store.Get("iSafeMstUser", "Id", intMstUserid);
                    iSafeTrnSessionPage userPageDetails = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", intMstUserid);
                    iSafeMstPage userPageSequence = (iSafeMstPage)Store.Get("iSafeMstPage", "lastPageid", userPageDetails.intMstPageId);
                    Dictionary<string, object> Topicfilter = new Dictionary<string, object>();
                    Topicfilter.Add("Name", "ByUserTopic");
                    Topicfilter.Add("Topicid", userPageSequence.intId);
                    List<IPersistantObject> Module1 = Global.Store.Find("iSafeMstModule", Topicfilter);
                    List<iSafeMstModule> ModuleList = new List<iSafeMstModule>();
                    iSafeMstModule Moduleid = (iSafeMstModule)Module1[0];
                    Dictionary<string, object> filter1 = new Dictionary<string, object>();
                    filter1.Add("Name", "ByLastModule");
                    List<IPersistantObject> userpage = Global.Store.Find("iSafeMstPage", filter1);
                    List<iSafeMstPage> userpages = new List<iSafeMstPage>();
                    iSafeMstPage lastPage = (iSafeMstPage)userpage[0];
                    if (userPageSequence.intId == lastPage.intId)
                    {
                        output.Add("SequenceNo", userPageSequence.intSequenceNo);
                        output.Add("PageName", userPageSequence.txtType);
                        output.Add("Model", Moduleid.intId);
                        output.Add("Topic", userPageSequence.txtName);
                        output.Add("Pageid", userPageSequence.intId);
                        output.Add("Pledge", 1);
                        output.Add("PrivacyStatus", userdetails.intPrivacyStatus);
                    }
                    else
                    {
                        output.Add("SequenceNo", userPageSequence.intSequenceNo);
                        output.Add("PageName", userPageSequence.txtType);
                        output.Add("Model", Moduleid.intId);
                        output.Add("Topic", userPageSequence.txtName);
                        output.Add("Pageid", userPageSequence.intId);
                        output.Add("Pledge", 0);
                        output.Add("PrivacyStatus", userdetails.intPrivacyStatus);
                    }
                }
                //-----------------------Page Load End------------------------//
                //-----------------------Next Page Start----------------------//
                if (Page == 1)
                {

                    iSafeTrnSessionPage userPageDetails = (iSafeTrnSessionPage)Store.Get("iSafeTrnSessionPage", "intMstUserid", intMstUserid);
                    if (CurrentPageid < Topic.Count)
                    {
                       
                        Dictionary<string, object> filter = new Dictionary<string, object>();
                        filter.Add("Name", "ByCurrentPage");
                        filter.Add("intSequenceNo", Pageid);
                        filter.Add("Model", Model);
                        List<IPersistantObject> user = Global.Store.Find("iSafeMstPage", filter);
                        List<iSafeMstPage> userPageSequences= new List<iSafeMstPage>();
                        iSafeMstPage userPageSequence = (iSafeMstPage)user[0];
                        Dictionary<string, object> filter1 = new Dictionary<string, object>();
                        filter1.Add("Name", "ByLastModule");
                        //filter1.Add("Moduelid", Model);
                        List<IPersistantObject> userpage = Global.Store.Find("iSafeMstPage", filter1);
                        List<iSafeMstPage> userpages = new List<iSafeMstPage>();
                        iSafeMstPage lastPage = (iSafeMstPage)userpage[0];
                        if (userPageSequence.intId > userPageDetails.intMstPageId)
                        {
                            if (userPageSequence.intId == lastPage.intId)
                            {
                                iSafeTrnSessionPage userPageCreate = new iSafeTrnSessionPage();
                                userPageCreate.txtTrnSessionId = Guid.NewGuid().ToString("N");
                                userPageCreate.intMstPageId = Convert.ToInt32(userPageSequence.intId);
                                userPageCreate.intMstUserid = Convert.ToInt32(intMstUserid);
                                Store.Create("iSafeTrnSessionPage", userPageCreate);
                                output.Add("SequenceNo", userPageSequence.intSequenceNo);
                                output.Add("PageName", userPageSequence.txtType);
                                output.Add("Model", userPageSequence.intMstTopicid);
                                output.Add("Pageid", userPageSequence.intId);
                                output.Add("Topic", userPageSequence.txtName);
                                output.Add("LastVisted", userPageDetails.intMstPageId);
                                output.Add("Pledge", 1);
                            }
                            else
                            {
                                iSafeTrnSessionPage userPageCreate = new iSafeTrnSessionPage();
                                userPageCreate.txtTrnSessionId = Guid.NewGuid().ToString("N");
                                userPageCreate.intMstPageId = Convert.ToInt32(userPageSequence.intId);
                                userPageCreate.intMstUserid = Convert.ToInt32(intMstUserid);
                                Store.Create("iSafeTrnSessionPage", userPageCreate);
                                output.Add("SequenceNo", userPageSequence.intSequenceNo);
                                output.Add("PageName", userPageSequence.txtType);
                                output.Add("Model", userPageSequence.intMstTopicid);
                                output.Add("Pageid", userPageSequence.intId);
                                output.Add("Topic", userPageSequence.txtName);
                                output.Add("LastVisted", userPageDetails.intMstPageId);
                                output.Add("Pledge", 0);
                            }
                        }
                        else
                        {
                            output.Add("SequenceNo", userPageSequence.intSequenceNo);
                            output.Add("PageName", userPageSequence.txtType);
                            output.Add("Model", userPageSequence.intMstTopicid);
                            output.Add("Pageid", userPageSequence.intId);
                            output.Add("Topic", userPageSequence.txtName);
                            output.Add("LastVisted", userPageDetails.intMstPageId);
                            output.Add("Pledge", 0);
                        }
                    }
                    else
                    {
                            Dictionary<string, object> filter = new Dictionary<string, object>();
                            filter.Add("Name", "ByCurrentPage");
                            filter.Add("intSequenceNo", 1);
                            filter.Add("Model", Model + 1);
                            List<IPersistantObject> user = Global.Store.Find("iSafeMstPage", filter);
                            List<iSafeMstPage> userPageSequences = new List<iSafeMstPage>();
                            iSafeMstPage userPageSequence = (iSafeMstPage)user[0];
                            iSafeTrnSessionPage userPageCreate = new iSafeTrnSessionPage();
                            userPageCreate.txtTrnSessionId = Guid.NewGuid().ToString("N");
                            userPageCreate.intMstPageId = Convert.ToInt32(userPageSequence.intId);
                            userPageCreate.intMstUserid = Convert.ToInt32(intMstUserid);
                            Store.Create("iSafeTrnSessionPage", userPageCreate);
                            output.Add("SequenceNo", userPageSequence.intSequenceNo);
                            output.Add("PageName", userPageSequence.txtType);
                            output.Add("Model", userPageSequence.intMstTopicid);
                            output.Add("Pageid", userPageSequence.intId);
                            output.Add("Topic", userPageSequence.txtName);
                            output.Add("LastVisted", userPageDetails.intMstPageId);
                            output.Add("Pledge", 0);
                    }
                }
                //-----------------------Next Page End----------------------//
                //-----------------------Prievious Page Start----------------------//
                if (Page == 2)
                {
                    if (CurrentPageid > 1)
                    {
                        Dictionary<string, object> filter = new Dictionary<string, object>();
                        filter.Add("Name", "ByCurrentPage");
                        filter.Add("intSequenceNo", Pageid);
                        filter.Add("Model", Model);
                        List<IPersistantObject> user = Global.Store.Find("iSafeMstPage", filter);
                        List<iSafeMstPage> userPageSequences = new List<iSafeMstPage>();
                        iSafeMstPage userPageSequence = (iSafeMstPage)user[0];
                        output.Add("SequenceNo", userPageSequence.intSequenceNo);
                        output.Add("PageName", userPageSequence.txtType);
                        output.Add("Model", userPageSequence.intMstTopicid);
                        output.Add("Pageid", userPageSequence.intId);
                        output.Add("Topic", userPageSequence.txtName);
                    }
                    else
                    {
                        Dictionary<string, object> filter = new Dictionary<string, object>();
                        filter.Add("Name", "ByCurrentPage");
                        filter.Add("intSequenceNo", Pageid+1);
                        filter.Add("Model", Model);
                        List<IPersistantObject> user = Global.Store.Find("iSafeMstPage", filter);
                        List<iSafeMstPage> userPageSequences = new List<iSafeMstPage>();
                        iSafeMstPage userPageSequence = (iSafeMstPage)user[0];
                        output.Add("SequenceNo", userPageSequence.intSequenceNo);
                        output.Add("PageName", userPageSequence.txtType);
                        output.Add("Model", userPageSequence.intMstTopicid);
                        output.Add("Pageid", userPageSequence.intId);
                        output.Add("Topic", userPageSequence.txtName);
                    }
                }
                //-----------------------Next Page End----------------------//
                //-----------------------Module Complete--------------------//
                if(Page == 4)
                {
                    Dictionary<string, object> filter = new Dictionary<string, object>();
                    filter.Add("Name", "ByCurrentPage");
                    filter.Add("intSequenceNo", 1);
                    filter.Add("Model", Model + 1);
                    List<IPersistantObject> user = Global.Store.Find("iSafeMstPage", filter);
                    List<iSafeMstPage> userPageSequences = new List<iSafeMstPage>();
                    iSafeMstPage userPageSequence = (iSafeMstPage)user[0];
                    if (Model == PageCnt.intId)
                    {
                        iSafeTrnSessionPage userPageCreate = new iSafeTrnSessionPage();
                        userPageCreate.txtTrnSessionId = Guid.NewGuid().ToString("N");
                        userPageCreate.intMstPageId = Convert.ToInt32(userPageSequence.intId);
                        userPageCreate.intMstUserid = Convert.ToInt32(intMstUserid);
                        Store.Create("iSafeTrnSessionPage", userPageCreate);
                        output.Add("SequenceNo", userPageSequence.intSequenceNo);
                        output.Add("PageName", userPageSequence.txtType);
                        output.Add("Model", userPageSequence.intMstTopicid);
                        output.Add("Pageid", userPageSequence.intId);
                        output.Add("Topic", userPageSequence.txtName);
                        output.Add("Pledge", 0);
                    }
                    else
                    {

                        output.Add("SequenceNo", userPageSequence.intSequenceNo);
                        output.Add("PageName", userPageSequence.txtType);
                        output.Add("Model", userPageSequence.intMstTopicid);
                        output.Add("Pageid", userPageSequence.intId);
                        output.Add("Topic", userPageSequence.txtName);
                        output.Add("Pledge", 0);
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AdminLogin Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("Index API End");
            return output;
        }
    }
}