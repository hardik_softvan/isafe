﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.API
{
    public class Scenario : IHandler
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public Scenario(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("AdminLogin API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                int intMstUserid = Convert.ToInt32((string)input["intMstUserid"]);
                int Pageid = Convert.ToInt32((string)input["Pageid"]);
                int Scenario = Convert.ToInt32((string)input["Scenario"]);
                Dictionary<string, object> filter = new Dictionary<string, object>();
                filter.Add("Name", "Scenario");
                filter.Add("intSequenceNo", Pageid);
                filter.Add("intMstUserid", intMstUserid);
                List<IPersistantObject> user = Global.Store.Find("iSafeTrnSessionPage", filter);
                List<iSafeTrnSessionPage> userPageSequences = new List<iSafeTrnSessionPage>();
                iSafeTrnSessionPage userPageSequence = (iSafeTrnSessionPage)user[0];
                if (userPageSequence.intCorrectResponsePageId == 0)
                { 
                iSafeTrnSessionPage iSafeTrnSessionPageActivity = new iSafeTrnSessionPage();
                iSafeTrnSessionPageActivity.intMstPageId = Pageid;
                iSafeTrnSessionPageActivity.intMstUserid = intMstUserid;
                iSafeTrnSessionPageActivity.intCorrectResponsePageId = Scenario;
                iSafeTrnSessionPageActivity.txtTrnSessionId = "2";
                Global.Store.Update(iSafeTrnSessionPageActivity);
                }
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AdminLogin Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("Scenario API End");
            return output;
        }
    }
}