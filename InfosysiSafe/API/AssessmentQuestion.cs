﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.API
{
    public class AssessmentQuestion : IHandler
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public AssessmentQuestion(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }
        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("AssessmentQuestion API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                for (int i = 1; i < 6; i++)
                {
                string intMstModuleId = (string)input["intMstModuleId"];
                    string Question1 = (string)input["Question"+i];
                    string Answer1 = (string)input["Answer"+i];
                    iSafeTrnAssessmentQuestion AssessmentQuestionDomain = new iSafeTrnAssessmentQuestion();
                    AssessmentQuestionDomain.intTrnAssessmentId = Convert.ToInt32(intMstModuleId);
                    AssessmentQuestionDomain.intMstModuleQuestionId = Convert.ToInt32(Question1);
                    AssessmentQuestionDomain.intUserOption = Convert.ToInt32(Answer1);
                    Store.Create("iSafeTrnAssessmentQuestion", AssessmentQuestionDomain);
                }
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("AssessmentQuestion Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("AssessmentQuestion API End");
            return output;
        }
    }
}