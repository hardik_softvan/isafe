﻿using InfosysiSafe.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfosysiSafe.API
{
    public class PledgeApi : IHandler
    {
        ILogger Logger;
        IAuthenticator Authenticator { get; }
        IPersistantStore Store { get; }
        public PledgeApi(IAuthenticator authenticator, IPersistantStore store, ILogger logger)
        {
            Authenticator = authenticator;
            Store = store;
            Logger = logger;
        }

        public Dictionary<string, object> Process(Dictionary<string, object> input)
        {
            Logger.Debug("PledgeApi API Start");
            Dictionary<string, object> output = new Dictionary<string, object>();
            try
            {
                string intMstUserId = (string)input["intMstUserId"];
                // Start a session
                iSafeTrnPledge iSafeTrnPledgeDomain = new iSafeTrnPledge();
                iSafeTrnPledgeDomain.intMstUserId = Convert.ToInt32(intMstUserId);
                iSafeTrnPledgeDomain.dtPledgeTakenOn = DateTime.Now;
                Store.Create("iSafeTrnPledge", iSafeTrnPledgeDomain);

                iSafeTrnSessionPage userPageCreate = new iSafeTrnSessionPage();
                userPageCreate.txtTrnSessionId = Guid.NewGuid().ToString("N");
                userPageCreate.intMstPageId = 1;
                userPageCreate.intMstUserid = Convert.ToInt32(intMstUserId);
                Store.Create("iSafeTrnSessionPage", userPageCreate);
            }
            catch (Exception ex)
            {
                ErrorLog log = new ErrorLog();
                log.LogError(ex);
                output.Add("error", ex.Message);
                Logger.Error("PledgeApi Request, loginName => " + (string)input["loginName"], ex);
            }
            Logger.Debug("PledgeApi API End");
            return output;
        }
    }
}