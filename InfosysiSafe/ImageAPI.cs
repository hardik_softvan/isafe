﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace InfosysiSafe
{
    public class ImageAPI : IHttpHandler, IRouteHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            Logger.Debug("ImageAPI Start");
            Dictionary<string, object> input = new Dictionary<string, object>();
            string sessionId = context.Request.QueryString["sessionId"];
            input.Add("sessionId", sessionId);
            Logger.SetContext(context.Request.Url.AbsoluteUri + " -> " + sessionId);
            Dictionary<string, object> output = Handler.Process(input);
            if (output.ContainsKey("error"))
            {
                context.Response.ContentType = "text/html";
                context.Response.WriteFile(ErrorPage);
            }
            else
            {
                context.Response.ContentType = (string)output["MIME"];
                context.Response.BinaryWrite((byte[])output["file"]);
            }
            Logger.Debug("ImageAPI End");
        }

        #endregion

        #region IRouteHandler Members

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return this;
        }

        #endregion

        ILogger Logger;
        public IHandler Handler { get; protected set; }
        string ErrorPage;

        public ImageAPI(IHandler handler, string errorPage, ILogger logger)
        {
            Handler = handler;
            ErrorPage = errorPage;
            Logger = logger;
        }

        public static Route GetRoute(string route, IHandler handler, string errorPage, ILogger logger)
        {
            return new Route("image/" + route, new ImageAPI(handler, errorPage, logger));
        }
    }
}