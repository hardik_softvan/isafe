﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using InfosysiSafe.API;
using System.Xml;
using System.Web.Caching;

namespace InfosysiSafe
{
    public class Global : System.Web.HttpApplication
    {
        // Ref: https://stackoverflow.blog/2008/07/18/easy-background-tasks-in-aspnet/

        private static CacheItemRemovedCallback OnCacheRemove = null;
        public static ILogger Logger;
        public static IPersistantStore Store;
        public static CertificateApi Generator = null;

        protected void Application_Start(object sender, EventArgs e)
        {
            AddTask("CertificateGenerator", 5);            
            Logger = new Log4NetLogger();
            Logger.Info("Application Start");
            string configurationFile = "";
            configurationFile = HttpContext.Current.Server.MapPath("~/App_Data/Configuration.xml");
            XmlDocument configuration = new XmlDocument();
            configuration.LoadXml(System.IO.File.ReadAllText(configurationFile));
            AddCompTask("CompliantGenerator", Convert.ToInt32(configuration.GetElementsByTagName("CompliantGeneratorCount")[0].InnerText));
            RegisterRoutes(RouteTable.Routes, configuration);
            Generator = new CertificateApi(Store, Logger, System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/CertificateTemplate.png"), System.Web.Hosting.HostingEnvironment.MapPath("~/images/Certificates/"));

        }

        private void AddTask(string name, int seconds)
        {
            OnCacheRemove = new CacheItemRemovedCallback(CacheItemRemoved);
            HttpRuntime.Cache.Insert(name, seconds, null,
                DateTime.Now.AddSeconds(seconds), Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, OnCacheRemove);
        }

        public void CacheItemRemoved(string k, object v, CacheItemRemovedReason r)
        {
            try
            {
              
            }
            catch (Exception ex)
            {
                Logger.Error("Certificate Generation Exception", ex);
            }
            AddTask(k, Convert.ToInt32(v));
        }

        private void AddCompTask(string name, int seconds)
        {
            OnCacheRemove = new CacheItemRemovedCallback(GenerateCompliantReport);
            HttpRuntime.Cache.Insert(name, seconds, null,
                DateTime.Now.AddSeconds(seconds), Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, OnCacheRemove);
        }

        public void GenerateCompliantReport(string k, object v, CacheItemRemovedReason r)
        {
            try
            {
             
            }
            catch (Exception ex)
            {
                Logger.Error("Compliant Generation Exception", ex);
            }
            AddCompTask(k, Convert.ToInt32(v));
        }

        public void RegisterRoutes(RouteCollection routes, XmlDocument configuration)
        {
            string connectionString = configuration.GetElementsByTagName("SQLServerConnectionString")[0].InnerText;
            SQLServerStore sqlStore = new SQLServerStore(connectionString);
#if DEBUG
            IAuthenticator authenticator = new DevelopmentAuthenticator();
#else        
            string adPath = configuration.GetElementsByTagName("ADPath")[0].InnerText;
            IAuthenticator authenticator = new ADAuthenticator(adPath);
#endif
            int maximumConcurrentUsers = Convert.ToInt32(configuration.GetElementsByTagName("MaximumConcurrentUsers")[0].InnerText);
              Store = sqlStore;
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpRequest request = HttpContext.Current.Request;
            Logger.SetContext(request.Url.AbsoluteUri);
            Logger.Debug("Begin Request");
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Logger.Error("Application Error: " + sender + ", " + e);
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            Logger.Info("Application End");
        }
    }
}